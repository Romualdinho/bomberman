package bomberman.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.plaf.basic.BasicArrowButton;

import bomberman.model.util.grid.Grid;

public class GridSelector extends JComponent {

	private static final long serialVersionUID = 1L;
	
	private List<Grid> grids;
	private Grid selectedGrid;
	private int index;
	
	private GameSurfaceView gridThumbnail;
	private BasicArrowButton left;
	private BasicArrowButton right;
	
	public GridSelector() {
		super();
		
		setBorder(BorderFactory.createLineBorder(Color.BLACK, 3));
		
		createView();
		placeComponenents();
		createController();
	}
	
	private void createView() {
		Font font = Game.BOMBERMAN_FONT.deriveFont(Font.BOLD, Game.getWidthRelativeToScreen(50));
		
		left = new BasicArrowButton(BasicArrowButton.WEST, Color.LIGHT_GRAY, Color.GRAY, Color.BLACK, Color.BLACK); {
			left.setEnabled(false);
			left.setFont(font);
			left.setFocusPainted(false);
			left.setCursor(new Cursor(Cursor.HAND_CURSOR));
		}
		
		gridThumbnail = new GameSurfaceView(false); {
			gridThumbnail.setPreferredSize(new Dimension(Game.getWidthRelativeToScreen(500),Game.getHeightRelativeToScreen(300)));
		}
		
		right = new BasicArrowButton(BasicArrowButton.EAST, Color.LIGHT_GRAY, Color.GRAY, Color.BLACK, Color.BLACK); {
			right.setEnabled(false);
			right.setFont(font);
			right.setFocusPainted(false);
			right.setCursor(new Cursor(Cursor.HAND_CURSOR));
		}
	}
	
	private void placeComponenents() {
		setLayout(new BoxLayout(this, BoxLayout.X_AXIS));

		JPanel p = new JPanel(new BorderLayout()); {
			p.setPreferredSize(new Dimension(Game.getWidthRelativeToScreen(50),0));
			p.add(left);
		}
		add(p);
		
		add(gridThumbnail);
		
		p = new JPanel(new BorderLayout()); {
			p.setPreferredSize(new Dimension(Game.getWidthRelativeToScreen(50),0));
			p.add(right);
		}
		add(p);
	}
	
	private void createController() {
		left.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SoundManager.playSoundEffect("button_pressed.wav");
				index--;
				
				selectedGrid = grids.get(index);
				gridThumbnail.setGrid(selectedGrid);
				
				right.setEnabled(true);
				if (index == 0) {
					left.setEnabled(false);
				}
			}
		});
		
		right.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SoundManager.playSoundEffect("button_pressed.wav");
				
				index++;
				
				selectedGrid = grids.get(index);
				gridThumbnail.setGrid(selectedGrid);
				
				left.setEnabled(true);
				if (index >= grids.size() - 1) {
					right.setEnabled(false);
				}
			}
		});
	}
	
	public Grid getSelectedGrid() {
		return selectedGrid;
	}
	
	public void setGrids(List<Grid> grids) {
		this.grids = grids;
		index = 0;

		selectedGrid = grids.size() > 0 ? grids.get(index) : null;
		gridThumbnail.setGrid(selectedGrid);
		
		left.setEnabled(false);
		right.setEnabled(grids.size() > 1 ? true : false);
	}
}
