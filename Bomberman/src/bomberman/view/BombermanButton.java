package bomberman.view;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;


public class BombermanButton extends JButton implements ActionListener {

	private static final long serialVersionUID = 1L;

	public BombermanButton(String text) {
		super(text);
		init();
	}
	
	public BombermanButton(ImageIcon icon) {
		super(icon);
		init();
	}
	
	private void init() {
		setFont(Game.BOMBERMAN_FONT.deriveFont(Font.BOLD, Game.getWidthRelativeToScreen(50)));
		setForeground(Color.BLACK);
		setBackground(Color.LIGHT_GRAY);
		setBorder(BorderFactory.createLineBorder(Color.BLACK, 3));
		setFocusPainted(false);
		
		addActionListener(this);
		addMouseListener(new MouseAdapter() {
			public void mouseEntered(MouseEvent e) {
				if (isEnabled()) {
					setBackground(Color.GRAY);
				}
			}
			public void mouseExited(MouseEvent e) {
				if (isEnabled()) {
					setBackground(Color.LIGHT_GRAY);
				}
			}
		});
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		SoundManager.playSoundEffect("button_pressed.wav");
	}
}
