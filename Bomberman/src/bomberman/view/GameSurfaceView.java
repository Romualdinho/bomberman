package bomberman.view;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JComponent;
import javax.swing.Timer;

import bomberman.model.TexturesManager;
import bomberman.model.entity.Entity;
import bomberman.model.entity.player.AI;
import bomberman.model.entity.player.Player;
import bomberman.model.util.Coordinate;
import bomberman.model.util.grid.Grid;


public class GameSurfaceView extends JComponent implements ActionListener {

	private static final long serialVersionUID = 1L;
	
	private final static int FPS = 60;
	
	private final static Font PLAYER_NAME_FONT = new Font("Arial Black", Font.PLAIN, Game.getWidthRelativeToScreen(13));
	
	private final static Color OVERLAY = new Color(0,0,0,150);
	private final static Color PLAYER_COLOR = new Color(4,153,180);
	private final static Color ENNEMY_COLOR = new Color(255,76,76);
	
	private Coordinate[][] coords;
	private boolean overlay;
	private Timer autoRefresh;
	private Grid grid;
	
	private int tileSize;
	private int widthShift;
	private int heightShift;
	private int tilesToAdd1;
	private int tilesToAdd2;
	private int xStart;
	private int yStart;
	private int widthGrid;
	private int heightGrid;
	
	public GameSurfaceView(boolean refresh) {
		autoRefresh = new Timer((int)(1000/FPS), this);
		addComponentListener(new ComponentListener() {
			public void componentHidden(ComponentEvent e) {
				updateScales();
			}
			public void componentMoved(ComponentEvent e) {
				updateScales();
			}
			public void componentResized(ComponentEvent e) {
				updateScales();
			}
			public void componentShown(ComponentEvent e) {
				updateScales();
			}
		});
		autoRefresh.start();
	}
	
	public void setGrid(Grid grid) {
		this.grid = grid;
		if (grid != null) {
			coords = new Coordinate[grid.getWidth()][grid.getHeight()];
			for (int i = 0; i < grid.getWidth(); i++) {
				for (int j = 0; j < grid.getHeight(); j++) {
					coords[i][j] = new Coordinate(i, j);
				}
			}
			updateScales();
		}
		repaint();
	}
	
	public void setOverlay(boolean overlay) {
		this.overlay = overlay;
		repaint();
	}
	
	public void paintComponent(Graphics g) {
			super.paintComponent(g);
			Graphics2D g2 = (Graphics2D) g;
			g2.setColor(Color.BLACK);
	    	g2.fillRect(0, 0, getWidth(), getHeight());

	        if (grid != null) {
		        g2.setFont(PLAYER_NAME_FONT); 
		        
		        for (int i = 0; i < widthGrid + tilesToAdd1; i++) {
		        	for (int j = 0; j < heightGrid + tilesToAdd2; j++) {
		        		g2.drawImage(TexturesManager.getResource("wall.png"), xStart + i * tileSize, yStart + j * tileSize, tileSize, tileSize, null);
		        	}
		        }
		        
		        for (int i = 0; i < widthGrid; i++) {
		        	for (int j = 0; j < heightGrid; j++) {
		        		g2.drawImage(TexturesManager.getResource("empty.png"), i * tileSize + widthShift, j * tileSize + heightShift, tileSize, tileSize, null);
		        		
		        		Coordinate coord = new Coordinate(i, j);
		        		if (grid.getGrid().get(coord).size() > 0) {
		        			List<Entity> entityList = new ArrayList<Entity>(grid.getGrid().get(coord));
		        			for (Entity entity : entityList) {
		        				if (!(entity instanceof Player)) {
		        					Image image = TexturesManager.getResource(entity.pathImage());
		        					g2.drawImage(image, i * tileSize + widthShift, j * tileSize + heightShift, tileSize, tileSize, null);
		        				}
		        			}
		        		}
		        	}
		        }
		        if (grid.getPlayers() != null) {
					for(Player p : grid.getPlayers()) {
						if (p.isAlive()) {	
							Coordinate coordinate = p.getTemporaryPosition();
							int i = coordinate.getX();
							int j = coordinate.getY();
							
							String playerName = p.getName();
							int sWidth = g2.getFontMetrics().stringWidth(playerName);
							if (p == grid.getPlayers().get(0) && !(p instanceof AI)) {
								g2.setColor(PLAYER_COLOR);
							} else {
								g2.setColor(ENNEMY_COLOR);
							}
							Coordinate destination = p.getDestination();
							if (destination != null) {		
								int x = destination.getX();
								int y = destination.getY();
								
								int moveX = 0, moveY = 0;
								
								if (i != x) {
									if (i < x) {
										moveX = (int)(tileSize * p.getMovementProgression());
									} else {
										moveX = -(int)(tileSize * p.getMovementProgression());
									}
								} else {
									if (j < y) {
										moveY = (int)(tileSize * p.getMovementProgression());
									} else {
										moveY = -(int)(tileSize * p.getMovementProgression());
									}
								}
								Image image = TexturesManager.getResource(p.pathImage());
								g2.drawImage(image, i * tileSize + moveX + widthShift, j * tileSize + moveY + heightShift, tileSize, tileSize, null);
								g2.drawString(playerName, i * tileSize + (int)((tileSize - sWidth) / 2.0) + moveX + widthShift, j * tileSize + tileSize/5 + moveY + heightShift);
							} else {
								Image image = TexturesManager.getResource(p.pathImage());
								g2.drawImage(image, i * tileSize + widthShift, j * tileSize + heightShift, tileSize, tileSize, null);
								g2.drawString(playerName, i * tileSize + (int)((tileSize - sWidth) / 2.0) + widthShift, j * tileSize + tileSize/5 + heightShift);
							}
						}
					}
				}
	        }
		    if (overlay) {
		    	g2.setColor(OVERLAY);
				g2.fillRect(0, 0, getWidth(), getHeight());
		    }
	}

	public void actionPerformed(ActionEvent e) {
		repaint();
	}
	
	private void updateScales() {
		if (grid == null) {
			return;
		}
		
		widthGrid = grid.getWidth();
		heightGrid = grid.getHeight();
		tileSize = Math.min(getHeight()/heightGrid, getWidth()/widthGrid);
	    widthShift = (int)((getWidth() - (widthGrid * tileSize)) / 2.0);
	    heightShift = (int)((getHeight() - (heightGrid * tileSize)) / 2.0);
        tilesToAdd1 = (int)Math.ceil((double)widthShift / tileSize) * 2;
        tilesToAdd2 = (int)Math.ceil((double)heightShift / tileSize) * 2;
        xStart = (int)((getWidth() - (widthGrid + tilesToAdd1) * tileSize) * 0.5);
        yStart = (int)((getHeight() - (heightGrid + tilesToAdd2) * tileSize) * 0.5);
	}
}