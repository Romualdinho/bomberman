package bomberman.view.binding;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.KeyStroke;

import net.java.games.input.Component.Identifier;
import net.java.games.input.Controller;
import net.java.games.input.ControllerEnvironment;
import net.java.games.input.Event;

import bomberman.model.GameModel;
import bomberman.model.util.Direction;
import bomberman.view.binding.actions.BindingAction;
import bomberman.view.binding.actions.BombBindingAction;
import bomberman.view.binding.actions.MoveBindingAction;
import bomberman.view.binding.actions.PauseBindingAction;
import bomberman.view.binding.actions.ScoreBindingAction;
import bomberman.view.binding.util.Bindings;
import bomberman.view.binding.util.PressKeyAction;
import bomberman.view.binding.util.ReleaseKeyAction;
import bomberman.view.binding.event.GamepadListener;

public class BindingManager {

	private Map<String, BindingAction> binds;
	private GamepadController gamepad;
	
	public BindingManager(JComponent comp, GameModel model) {
		
		binds = new HashMap<String, BindingAction>();
		
		//Initialisation des binds pour le déplacement du joueur
		List<BindingAction> moveBinds = new ArrayList<BindingAction>();
		BindingAction moveUp = new MoveBindingAction(model, "MOVE_UP", Bindings.MOVE_UP, Direction.UP);
		BindingAction moveLeft = new MoveBindingAction(model, "MOVE_LEFT", Bindings.MOVE_LEFT, Direction.LEFT);
		BindingAction moveDown = new MoveBindingAction(model, "MOVE_DOWN", Bindings.MOVE_DOWN, Direction.DOWN);
		BindingAction moveRight = new MoveBindingAction(model, "MOVE_RIGHT", Bindings.MOVE_RIGHT, Direction.RIGHT);
		
		moveBinds.add(moveUp);
		moveBinds.add(moveLeft);
		moveBinds.add(moveDown);
		moveBinds.add(moveRight);
		
		for (BindingAction moveBind : moveBinds) {
			binds.put(moveBind.getId(), moveBind);
		}
		binds.put("DROP_BOMB", new BombBindingAction(model, "DROP_BOMB", Bindings.DROP_BOMB));
		binds.put("PAUSE", new PauseBindingAction(model, "PAUSE", Bindings.PAUSE));
		binds.put("SHOW_SCORE", new ScoreBindingAction(model, "SHOW_SCORE", Bindings.SHOW_SCORE));
		
		//Affectation des binds au composant
		InputMap im = comp.getInputMap(JPanel.WHEN_IN_FOCUSED_WINDOW);
		ActionMap am = comp.getActionMap();
		for (Entry<String, BindingAction> entry : binds.entrySet()) {
			
			BindingAction bind = entry.getValue();
			
			String pressedId = bind.getId() + "_PRESSED";
			String releasedId = bind.getId() + "_RELEASED";
			
			for (int keyCode : bind.getBindings().getKeyCodes() ) {
				im.put(KeyStroke.getKeyStroke(keyCode, 0), pressedId);
				im.put(KeyStroke.getKeyStroke(keyCode, 0, true), releasedId);
				
				if (bind instanceof MoveBindingAction) {
					List<BindingAction> incompatibleBindings = new ArrayList<BindingAction>(moveBinds);
					incompatibleBindings.remove(bind);
				
					am.put(pressedId, new PressKeyAction(bind, incompatibleBindings));
					am.put(releasedId, new ReleaseKeyAction(bind));
				} else {
					am.put(pressedId, new PressKeyAction(bind));
					am.put(releasedId, new ReleaseKeyAction(bind));
				}
			}
		}
		
		Controller[] controllers = ControllerEnvironment.getDefaultEnvironment().getControllers();
		for (Controller controller : controllers) {
			if (controller.getType() == Controller.Type.GAMEPAD) {
				gamepad = new GamepadController(controller);
				break;
			}
		}
		
		if (gamepad != null) {
			initGamepad();
		}
	}
	
	public boolean gamepadConnected() {
		return gamepad != null;
	}
	
	private void initGamepad() {
		gamepad.addGamepadListener(new GamepadListener() {
			public void buttonPressed(Event event) {
				Identifier identifier = event.getComponent().getIdentifier();
				
				System.out.println(identifier);
				
				for (Entry<String, BindingAction> entry : binds.entrySet()) {
					BindingAction bind = entry.getValue();
					
					if (bind.getBindings().contains(identifier)) {
						bind.press();
					}
				}
			}
			public void buttonReleased(Event event) {
				Identifier identifier = event.getComponent().getIdentifier();
				for (Entry<String, BindingAction> entry : binds.entrySet()) {
					BindingAction bind = entry.getValue();
					
					if (bind.getBindings().contains(identifier)) {
						bind.release();
					}
				}
			}
			public void onStopMovement() {
				binds.get("MOVE_LEFT").release();
				binds.get("MOVE_UP").release();
				binds.get("MOVE_RIGHT").release();
				binds.get("MOVE_DOWN").release();
			}
			public void onLeftMovement() {
				binds.get("MOVE_UP").release();
				binds.get("MOVE_RIGHT").release();
				binds.get("MOVE_DOWN").release();
				binds.get("MOVE_LEFT").press();
			}
			public void onUpMovement() {
				binds.get("MOVE_LEFT").release();
				binds.get("MOVE_RIGHT").release();
				binds.get("MOVE_DOWN").release();
				binds.get("MOVE_UP").press();
			}
			public void onRightMovement() {
				binds.get("MOVE_LEFT").release();
				binds.get("MOVE_UP").release();
				binds.get("MOVE_DOWN").release();
				binds.get("MOVE_RIGHT").press();
			}
			public void onDownMovement() {
				binds.get("MOVE_LEFT").release();
				binds.get("MOVE_UP").release();
				binds.get("MOVE_RIGHT").release();
				binds.get("MOVE_DOWN").press();
			}
			public void onControllerDisconnected() {
				System.out.println("Manette deconnectée.");
			}
		});
	}
}