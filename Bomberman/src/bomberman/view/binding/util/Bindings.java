package bomberman.view.binding.util;

import java.awt.event.KeyEvent;

import net.java.games.input.Component.Identifier;

public enum Bindings {

	MOVE_LEFT (new int[]{KeyEvent.VK_Q, KeyEvent.VK_LEFT},
			   new Identifier[]{}),
	MOVE_UP   (new int[]{KeyEvent.VK_Z, KeyEvent.VK_UP},
			   new Identifier[]{}),
	MOVE_RIGHT(new int[]{KeyEvent.VK_D, KeyEvent.VK_RIGHT},
			   new Identifier[]{}),
	MOVE_DOWN (new int[]{KeyEvent.VK_S, KeyEvent.VK_DOWN},
			   new Identifier[]{}),
	DROP_BOMB (new int[]{KeyEvent.VK_SPACE},
			   new Identifier[]{Identifier.Button._0, Identifier.Button._1, Identifier.Button._2,
							    Identifier.Button._3, Identifier.Button.A, Identifier.Button.B,
							    Identifier.Button.X, Identifier.Button.Y}),
	PAUSE	  (new int[]{KeyEvent.VK_ESCAPE, KeyEvent.VK_P}, 
			   new Identifier[]{Identifier.Button._7, Identifier.Button.START}),
	SHOW_SCORE(new int[]{KeyEvent.VK_TAB, KeyEvent.VK_A},
			   new Identifier[]{Identifier.Button._6, Identifier.Button.SELECT});
	
	private int[] keyCodes;
	private Identifier[] gamepadIdentifiers;
	Bindings(int[] keyCodes, Identifier[] gamepadIdentifiers) {
		this.keyCodes = keyCodes;
		this.gamepadIdentifiers = gamepadIdentifiers;
	}
	
	public int[] getKeyCodes() {
		return keyCodes;
	}
	
	public Identifier[] getGamepadIdentifiers() {
		return gamepadIdentifiers;
	}
	
	public boolean contains(Identifier identifier) {
		for (Identifier gamepadIdentifier : gamepadIdentifiers) {
			if (gamepadIdentifier == identifier) {
				return true;
			}
		}
		return false;
	}
}