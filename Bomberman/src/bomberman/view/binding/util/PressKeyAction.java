package bomberman.view.binding.util;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractAction;

import bomberman.view.binding.actions.BindingAction;

public class PressKeyAction extends AbstractAction {

	private static final long serialVersionUID = 1L;
	
	private BindingAction bindingAction;
	private List<BindingAction> incompatibleBindings;
	
	public PressKeyAction(BindingAction bindingAction) {
		this(bindingAction, new ArrayList<BindingAction>());
	}
	
	public PressKeyAction(BindingAction bindingAction, List<BindingAction> incompatibleBindings) {
		this.bindingAction = bindingAction;
		this.incompatibleBindings = incompatibleBindings;
	}
	
	public void actionPerformed(ActionEvent e) {
		for (BindingAction incompatibleBinding : incompatibleBindings) {
			incompatibleBinding.release();
		}
		bindingAction.press();
	}
}
