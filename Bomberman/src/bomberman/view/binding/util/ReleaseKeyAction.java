package bomberman.view.binding.util;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import bomberman.view.binding.actions.BindingAction;

public class ReleaseKeyAction extends AbstractAction {

	private static final long serialVersionUID = 1L;
	
	private BindingAction bindingAction;
	
	public ReleaseKeyAction(BindingAction bindingAction) {
		this.bindingAction = bindingAction;
	}
	
	public void actionPerformed(ActionEvent e) {
		bindingAction.release();
	}
}
