package bomberman.view.binding.actions;

import java.awt.event.ActionEvent;

import bomberman.model.GameModel;
import bomberman.view.binding.util.Bindings;

public class BombBindingAction extends BindingAction {

	public BombBindingAction(GameModel model, String id, Bindings bindings) {
		super(model, id, bindings);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (model.getPlayers().size() == 0) {
			return;
		}
		
		if (!model.isPaused()) {
			model.dropBomb(model.getPlayers().get(0));
		}
	}
}