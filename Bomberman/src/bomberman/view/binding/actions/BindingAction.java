package bomberman.view.binding.actions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Timer;

import bomberman.model.GameModel;
import bomberman.view.binding.util.Bindings;

public abstract class BindingAction implements ActionListener {

	protected GameModel model;
	private String id;
	private Bindings bindings;
	protected Timer action;
	
	public BindingAction(GameModel model, String id, Bindings bindings) {
		this.model = model;
		this.id = id;
		this.bindings = bindings;
		
		action = new Timer(5, this);
	}

	public void press() {
		if (model.inGame() && !action.isRunning()) {
			action.start();
		}
	}
	
	public void release() {
		if (model.inGame() && action.isRunning()) {
			action.stop();
		}
	}
	
	public String getId() {
		return id;
	}
	
	public Bindings getBindings() {
		return bindings;
	}
	
	public abstract void actionPerformed(ActionEvent e);

}
