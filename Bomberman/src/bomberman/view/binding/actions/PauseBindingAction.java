package bomberman.view.binding.actions;

import java.awt.event.ActionEvent;

import bomberman.model.GameModel;
import bomberman.view.binding.util.Bindings;

public class PauseBindingAction extends BindingAction {

	private boolean pressed;
	
	public PauseBindingAction(GameModel model, String id, Bindings bindings) {
		super(model, id, bindings);
	}
	
	@Override
	public void press() {
		if (!pressed) {
			if (model.isPaused()) {
				model.resume();
			} else {
				model.pause();
			}
			pressed = true;
		}
	}
	
	@Override
	public void release() {
		pressed = false;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		//rien
	}
}