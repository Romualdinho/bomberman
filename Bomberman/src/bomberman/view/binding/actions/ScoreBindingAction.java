package bomberman.view.binding.actions;

import java.awt.event.ActionEvent;

import bomberman.model.GameModel;
import bomberman.view.binding.util.Bindings;

public class ScoreBindingAction extends BindingAction {

	// CONSTANTES
	
	public ScoreBindingAction(GameModel model, String id, Bindings bindings) {
		super(model, id, bindings);
	}

	// COMMANDES
	
	public void press() {
		try {
			model.showScore();
		} catch (Exception e) {
			// RIEN
		}
	}
	
	public void release() {
		try {
			model.hideScore();
		} catch (Exception e) {
			// RIEN
		}
	}
	
	public void actionPerformed(ActionEvent e) {
		//rien
	}

}
