package bomberman.view.binding.actions;

import java.awt.event.ActionEvent;

import bomberman.model.GameModel;
import bomberman.model.entity.player.Player;
import bomberman.model.util.Direction;
import bomberman.view.binding.util.Bindings;

public class MoveBindingAction extends BindingAction {
	
	private Direction direction;
	
	public MoveBindingAction(GameModel model, String id, Bindings bindings, Direction direction) {
		super(model, id, bindings);
		this.direction = direction;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (model.getPlayers().size() == 0) {
			return;
		}
		
		Player player = model.getPlayers().get(0);
		if (!model.isPaused() && player.getDestination() == null) {
			model.movePlayer(player, direction);
		}
	}
}
