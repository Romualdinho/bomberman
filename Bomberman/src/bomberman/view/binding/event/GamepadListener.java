package bomberman.view.binding.event;

import java.util.EventListener;

import net.java.games.input.Event;

public interface GamepadListener extends EventListener {
	void buttonPressed(Event event);
	void buttonReleased(Event event);
	void onStopMovement();
	void onLeftMovement();
	void onUpMovement();
	void onRightMovement();
	void onDownMovement();
	void onControllerDisconnected();
}
