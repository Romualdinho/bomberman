package bomberman.view.binding;

import javax.swing.event.EventListenerList;

import net.java.games.input.Component;
import net.java.games.input.Controller;
import net.java.games.input.Event;
import net.java.games.input.EventQueue;
import net.java.games.input.Component.Identifier;
import bomberman.view.binding.event.GamepadListener;

public class GamepadController {

	private EventListenerList listeners;

	public GamepadController(final Controller controller) {
		
		if (controller.getType() != Controller.Type.GAMEPAD) {
			throw new AssertionError("le controller n'est pas un gamepad");
		}
		
		listeners = new EventListenerList();

		new Thread(new Runnable() {
			public void run() {
				
	            int xAxisPercentage = 50;
	            int yAxisPercentage = 50;
				
				while (true) {

					boolean leftStickMoved = false;
					
					if (!controller.poll()) {
						onControllerDisconnected();
						break;
					}

					EventQueue queue = controller.getEventQueue();
					Event event = new Event();

					while (queue.getNextEvent(event)) {
						Component comp = event.getComponent();
						Identifier identifier = comp.getIdentifier();
						
						if (identifier.getName().matches("^[0-9]*$")) {
							if (event.getValue() == 1.0) {
								buttonPressed(event);
							} else {
								buttonReleased(event);
							}
						} else if (identifier == Identifier.Axis.POV) {
							if (event.getValue() == 0.0) {
								onStopMovement();
							} else if (event.getValue() == 0.25) {
								onUpMovement();
							} else if (event.getValue() == 0.50) {
								onRightMovement();
							} else if (event.getValue() == 0.75) {
								onDownMovement();
							} else if (event.getValue() == 1.0) {
								onLeftMovement();
							}
						} else if (comp.isAnalog()) {
							leftStickMoved = true;
							
							float axisValue = event.getValue();
							int axisValueInPercentage = getAxisValueInPercentage(axisValue);
							
							if (identifier == Identifier.Axis.X) {
								xAxisPercentage = axisValueInPercentage;
							}

							if (identifier == Identifier.Axis.Y) {
								yAxisPercentage = axisValueInPercentage;
							}
						}
					}
					
					if (leftStickMoved) {
						int xRangeFromCenter = Math.abs(xAxisPercentage - 50);
						int yRangeFromCenter = Math.abs(yAxisPercentage - 50);
						int farRangerFromCenter = Math.max(xRangeFromCenter, yRangeFromCenter);
						
						if (farRangerFromCenter < 10) {
							onStopMovement();
						} else if (farRangerFromCenter == xRangeFromCenter) {
							if (xAxisPercentage < 50) {
								onLeftMovement();
							} else {
								onRightMovement();
							}
						} else {
							if ( yAxisPercentage < 50) {
								onUpMovement();
							} else {
								onDownMovement();
							}
						}
					}
				}
				try { Thread.sleep(50); } catch (InterruptedException ie) {}
			}
		}).start();
	}
	
    private int getAxisValueInPercentage(float axisValue) {
        return (int)(((2 - (1 - axisValue)) * 100) / 2);
    }

	public void addGamepadListener(GamepadListener listener) {
		listeners.add(GamepadListener.class, listener);
	}

	public void removeGamepadListener(GamepadListener listener) {
		listeners.remove(GamepadListener.class, listener);
	}

	private void buttonPressed(Event event) {
		for (GamepadListener listener : listeners.getListeners(GamepadListener.class)) {
			listener.buttonPressed(event);
		}
	}

	private void buttonReleased(Event event) {
		for (GamepadListener listener : listeners.getListeners(GamepadListener.class)) {
			listener.buttonReleased(event);
		}
	}

	private void onLeftMovement() {
		for (GamepadListener listener : listeners.getListeners(GamepadListener.class)) {
			listener.onLeftMovement();
		}
	}

	private void onUpMovement() {
		for (GamepadListener listener : listeners.getListeners(GamepadListener.class)) {
			listener.onUpMovement();
		}
	}

	private void onRightMovement() {
		for (GamepadListener listener : listeners.getListeners(GamepadListener.class)) {
			listener.onRightMovement();
		}
	}

	private void onDownMovement() {
		for (GamepadListener listener : listeners.getListeners(GamepadListener.class)) {
			listener.onDownMovement();
		}
	}
	
	private void onStopMovement() {
		for (GamepadListener listener : listeners.getListeners(GamepadListener.class)) {
			listener.onStopMovement();
		}
	}

	private void onControllerDisconnected() {
		for (GamepadListener listener : listeners.getListeners(GamepadListener.class)) {
			listener.onControllerDisconnected();
		}
	}
}