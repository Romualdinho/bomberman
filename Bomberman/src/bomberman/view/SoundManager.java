package bomberman.view;

import java.io.InputStream;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

import bomberman.model.Settings;

import javazoom.jl.decoder.JavaLayerException;
import javazoom.jl.player.advanced.AdvancedPlayer;

public class SoundManager {
	
	public static synchronized void playSoundEffect(final String fileName) {
		if (!Settings.getBooleanValue("sounds")) {
			return;
		}
		
		new Thread(new Runnable() {
			public void run() {
				try {
					Clip clip = AudioSystem.getClip();
					AudioInputStream inputStream = AudioSystem.getAudioInputStream(getClass().getClassLoader().getResource("sounds/" + fileName));
					clip.open(inputStream);
					clip.start(); 
				} catch (Exception e) {
					// rien
				}
			}
		}).start();
	}
	
	public static synchronized void playBackgroundMusic(final String fileName) {
		if (!Settings.getBooleanValue("music")) {
			return;
		}
		
		new Thread(new Runnable() {
			public void run() {
				try {
					while (true) {
						InputStream stream = getClass().getClassLoader().getResourceAsStream("sounds/" + fileName);
						AdvancedPlayer playMP3 = new AdvancedPlayer(stream);
					    playMP3.play();
					}
				} catch (JavaLayerException e) {
					e.printStackTrace();
				}
			}
		}).start();
	}
}
