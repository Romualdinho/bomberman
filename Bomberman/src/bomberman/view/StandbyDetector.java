package bomberman.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Timer;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.EventListenerList;

public class StandbyDetector implements ActionListener {

	private static final int STANDBY_TIME = 15000;
	
	private Timer detector;
	private EventListenerList listeners;
	private boolean standby;
	
	public StandbyDetector() {
		detector = new Timer(STANDBY_TIME, this);
		detector.setRepeats(false);
		
		listeners = new EventListenerList();
	}
	
	public void userDidSomething() {
		if (standby) {
			standby = false;
			fireStateChanged();
		}

		if (detector.isRunning()) {
			detector.stop();
		}
		detector.start();
	}
	
	public boolean isStandBy() {
		return standby;
	}

	public void actionPerformed(ActionEvent arg0) {
		standby = true;
		fireStateChanged();
	}
	
	public void addChangeListener(ChangeListener lst) {
		if (lst == null) {
			return;
		}
		listeners.add(ChangeListener.class, lst);
	}
	
	public void removeChangeListener(ChangeListener lst) {
		if (lst == null) {
			return;
		}
		listeners.remove(ChangeListener.class, lst);
	}
	
	public void fireStateChanged() {
		ChangeEvent e = new ChangeEvent(this);
		for (ChangeListener listener : listeners.getListeners(ChangeListener.class)) {
			listener.stateChanged(e);
		}
	}
}