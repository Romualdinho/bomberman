package bomberman.view;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.concurrent.TimeUnit;

import javax.swing.JLabel;
import javax.swing.Timer;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.EventListenerList;

public class Countdown extends JLabel implements ActionListener {

	private static final long serialVersionUID = 1L;
	
	public static final int SHORT_COUNTDOWN = 1;
	public static final int LONG_COUNTDOWN = 2;
	
	private long duration;
	private long secondsRemaining;
	private Timer countdown;
	private int style;
	private boolean sound;
	private EventListenerList listeners;
	
	public Countdown(long duration, int style, int size, boolean sound) {
		super();
		this.duration = duration;
		this.style = style;
		
		secondsRemaining = duration;
		
		setAlignmentX(Component.CENTER_ALIGNMENT);
		setForeground(Color.LIGHT_GRAY);
		setFont(Game.BOMBERMAN_FONT.deriveFont(Font.PLAIN, Game.getWidthRelativeToScreen(size)));
		
		long minutes = TimeUnit.SECONDS.toMinutes(duration);
		long seconds = duration - minutes * 60;
		
		if (style == SHORT_COUNTDOWN) {
			setText(String.valueOf(seconds));
		}
		else {
			if (seconds < 10) {
				setText(minutes + ":0" + seconds);
			} else {
				setText(minutes + ":" + seconds);
			}
		}
		
		listeners = new EventListenerList();
		
		countdown = new Timer(1000, this);
		
		this.sound = sound;
	}
	
	public void pause() {
		if (countdown.isRunning()) {
			countdown.stop();
		}
	}
	
	public void resume() {
		if (!countdown.isRunning()) {
			countdown.start();
		}
	}
	
	public void restart() {
		if (countdown.isRunning()) {
			countdown.stop();
		}
	
		long minutes = TimeUnit.SECONDS.toMinutes(duration);
		long seconds = duration - minutes * 60;
		if (style == SHORT_COUNTDOWN) {
			setText(String.valueOf(seconds));
		}
		else {
			if (seconds < 10) {
				setText(minutes + ":0" + seconds);
			} else {
				setText(minutes + ":" + seconds);
			}
		}
		
		secondsRemaining = duration;
		countdown.start();
	}
	
	public void addChangeListener(ChangeListener listener) {
		listeners.add(ChangeListener.class, listener);
	}
	
	public void removeChangeListener(ChangeListener listener) {
		listeners.remove(ChangeListener.class, listener);
	}
	
	protected void fireCountdownFinished() {
		for (ChangeListener listener : listeners.getListeners(ChangeListener.class)) {
			listener.stateChanged(new ChangeEvent(this));
		}
	}

	public void actionPerformed(ActionEvent e) {
		if (secondsRemaining > 0) {
			secondsRemaining--;
			long minutes = TimeUnit.SECONDS.toMinutes(secondsRemaining);
			long seconds = secondsRemaining - minutes * 60;
			
			if (style == SHORT_COUNTDOWN) {
				setText(String.valueOf(seconds));
			}
			else {
				if (seconds < 10) {
					setText(minutes + ":0" + seconds);
				} else {
					setText(minutes + ":" + seconds);
				}
			}
			if (sound) {
				SoundManager.playSoundEffect("beep.wav");
			}
		}
		if (secondsRemaining == 0) {
			countdown.stop();
			fireCountdownFinished();
		}
	}
}
