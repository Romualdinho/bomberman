package bomberman.view;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

public class BombermanTableRenderer implements TableCellRenderer {

	private DefaultTableCellRenderer delegate;
	
	public BombermanTableRenderer() {
		delegate = new DefaultTableCellRenderer();
	}
	
	public Component getTableCellRendererComponent(JTable table, Object value,
			boolean isSelected, boolean hasFocus, int row, int column) {
		
		JLabel result = (JLabel)delegate.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
		
		result.setBackground(new Color(0,0,0,130));
		result.setForeground(Color.LIGHT_GRAY);
		result.setFont(Game.BOMBERMAN_FONT.deriveFont(Font.PLAIN, Game.getWidthRelativeToScreen(30)));
		result.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));
		result.setHorizontalAlignment(SwingConstants.CENTER);
		
		if (value instanceof ImageIcon) {
			result.setText("");
			result.setIcon((Icon)value);
		}
		
		return result;
	}
}