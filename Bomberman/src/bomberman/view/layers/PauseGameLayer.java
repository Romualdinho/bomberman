package bomberman.view.layers;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import bomberman.model.GameModel;
import bomberman.view.BombermanButton;
import bomberman.view.Game;

public class PauseGameLayer extends AbstractBombermanLayer {

	private static final long serialVersionUID = 1L;

	private JButton resume;
	private JButton quit;
	
	public PauseGameLayer(GameModel model) {
		super(model);
	}

	@Override
	protected void createView() {
		resume = new BombermanButton("REPRENDRE"); {
			resume.setPreferredSize(new Dimension(Game.getWidthRelativeToScreen(500), resume.getPreferredSize().height));
		}
		quit = new BombermanButton("QUITTER"); {
			quit.setPreferredSize(new Dimension(Game.getWidthRelativeToScreen(500), quit.getPreferredSize().height));
		}
	}

	@Override
	protected void placeComponents() {
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		
		add(Box.createRigidArea(new Dimension(0,Game.getHeightRelativeToScreen(200))));
		
		JPanel p = new JPanel(); {
			p.setOpaque(false);
			JLabel title = new JLabel("PAUSE"); {
				title.setForeground(Color.LIGHT_GRAY);
				title.setFont(Game.BOMBERMAN_FONT.deriveFont(Font.BOLD, Game.getWidthRelativeToScreen(120)));
			}
			p.add(title);
		}
		add(p);
		
		p = new JPanel(); {
			p.setOpaque(false);
			p.add(resume);
		}
		add(p);
		
		p = new JPanel(); {
			p.setOpaque(false);
			p.add(quit);
		}
		add(p);
		
		add(Box.createRigidArea(new Dimension(0,Game.getHeightRelativeToScreen(300))));
	}

	@Override
	protected void createController() {
		resume.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				model.resume();
			}
			
		});
		
		quit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				model.exit();
			}
		});
	}
}