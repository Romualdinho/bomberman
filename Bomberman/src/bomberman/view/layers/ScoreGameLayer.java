package bomberman.view.layers;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.TableColumnModel;

import bomberman.model.GameModel;
import bomberman.model.entity.player.Player;
import bomberman.model.event.GameAdapter;
import bomberman.model.gui.DefaultScoreTableModel;
import bomberman.view.Game;
import bomberman.view.BombermanTableRenderer;

public class ScoreGameLayer extends AbstractBombermanLayer {

	private static final long serialVersionUID = 1L;
	
	private DefaultScoreTableModel table;
	private JTable scores;
	private JScrollPane jsp;
	
	public ScoreGameLayer(GameModel model) {
		super(model);
	}

	protected void createView() {
		scores = new JTable(); {
			scores.getTableHeader().setDefaultRenderer(new BombermanTableRenderer());
			scores.setPreferredSize(new Dimension(Game.getWidthRelativeToScreen(900), Game.getHeightRelativeToScreen(600)));
			scores.setOpaque(false);
			scores.setBorder(BorderFactory.createLineBorder(Color.WHITE));
			scores.setPreferredScrollableViewportSize(scores.getPreferredSize());
		}
		
		jsp = new JScrollPane(scores); {
			jsp.setOpaque(false);
			jsp.getViewport().setOpaque(false);
		}
	}

	protected void placeComponents() {
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		
		add(Box.createRigidArea(new Dimension(0,Game.getHeightRelativeToScreen(200))));
		
		JPanel p = new JPanel(); {
			p.setOpaque(false);
			p.add(jsp);
		}
		add(p);
	}

	protected void createController() {
		model.addGameListener(new GameAdapter() {
			public void gameStarted() {
				table = new DefaultScoreTableModel(model.getPlayers());
				scores.setModel(table);
		    	
				scores.setRowHeight(scores.getPreferredSize().height / table.getRowCount());
		    	TableColumnModel columnModel = scores.getColumnModel();
		    	for (int i = 0; i < scores.getColumnCount(); i++) {
		    		int size = (i == 0 || i == 1) ? Game.getWidthRelativeToScreen(350) : Game.getWidthRelativeToScreen(200);
		    		columnModel.getColumn(i).setPreferredWidth(size);
		    	}
		    	
				scores.setDefaultRenderer(Player.class, new BombermanTableRenderer());
				scores.setDefaultRenderer(ImageIcon.class, new BombermanTableRenderer());
			}
		});
	}
}