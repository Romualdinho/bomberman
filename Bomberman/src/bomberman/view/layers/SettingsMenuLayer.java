package bomberman.view.layers;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;

import bomberman.model.GameModel;
import bomberman.model.TexturesManager;
import bomberman.model.ResourcesInstaller;
import bomberman.model.Settings;
import bomberman.view.BombermanButton;
import bomberman.view.Game;

public class SettingsMenuLayer extends AbstractBombermanLayer {

	private static final long serialVersionUID = 1L;
	
	private static final List<String> REQUIRED_TEXTURES = Arrays.asList(new String[]{"add_bomb.png", "bomb.png", "empty.png", "explosion.png",
			"gold_bomb.png", "player.png", "range_bomb.png", "time_bomb.png", "wall.png", "wooden_box.png"});
	
	private Color success;
	private Color error;
	
	private Image soundsOn;
	private Image soundsOff;
	
	private JButton music;
	private JButton sounds;
	private JButton texturesPack;
	private JButton backToHomeButton;
	private JLabel texturesPackName;
	private JLabel informations;
	
	public SettingsMenuLayer(GameModel model) {
		super(model);
	}

	@Override
	protected void createView() {
		success = new Color(153, 255, 153);
		error = new Color(255, 102, 102);
		
		soundsOn = new ImageIcon(getClass().getClassLoader().getResource("images/sounds_on.png")).getImage().getScaledInstance(Game.getWidthRelativeToScreen(110), Game.getWidthRelativeToScreen(110), Image.SCALE_DEFAULT);
		soundsOff = new ImageIcon(getClass().getClassLoader().getResource("images/sounds_off.png")).getImage().getScaledInstance(Game.getWidthRelativeToScreen(110), Game.getWidthRelativeToScreen(110), Image.SCALE_DEFAULT);
		
		music = new BombermanButton(Settings.getBooleanValue("music") ? new ImageIcon(soundsOn) : new ImageIcon(soundsOff));
		sounds = new BombermanButton(Settings.getBooleanValue("sounds") ? new ImageIcon(soundsOn) : new ImageIcon(soundsOff));
		texturesPack = new BombermanButton("..."); {
			texturesPack.setPreferredSize(new Dimension(Game.getWidthRelativeToScreen(110), Game.getWidthRelativeToScreen(110)));
		}
		
		Font font = Game.BOMBERMAN_FONT.deriveFont(Font.PLAIN, Game.getWidthRelativeToScreen(30));
		texturesPackName = new JLabel(Settings.getStringValue("textures-pack"), JLabel.CENTER);
		texturesPackName.setFont(font);
		texturesPackName.setForeground(Color.LIGHT_GRAY);
		
		informations = new JLabel(" "); {
			informations.setFont(font);
		}
		
		backToHomeButton = new BombermanButton("RETOUR"); {
			backToHomeButton.setPreferredSize(new Dimension(Game.getWidthRelativeToScreen(350), backToHomeButton.getPreferredSize().height));
		}
	}

	@Override
	protected void placeComponents() {
		final Font font1 = Game.BOMBERMAN_FONT.deriveFont(Font.PLAIN, Game.getWidthRelativeToScreen(50));
		final Font font2 = Game.BOMBERMAN_FONT.deriveFont(Font.PLAIN, Game.getWidthRelativeToScreen(35));
		final Color color = new Color(170,170,170);
		
		setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
		
		JPanel settings = new JPanel(); {
			settings.setLayout(new BoxLayout(settings, BoxLayout.Y_AXIS));
			settings.setOpaque(false);
			
			settings.add(Box.createRigidArea(new Dimension(0, Game.getHeightRelativeToScreen(75))));
			
			JPanel p = new JPanel(); {
				p.setOpaque(false);
				JLabel title = new JLabel("PARAMETRES"); {
					title.setForeground(Color.LIGHT_GRAY);
					title.setFont(Game.BOMBERMAN_FONT.deriveFont(Font.BOLD, Game.getWidthRelativeToScreen(120)));
				}
				p.add(title);
			}
			settings.add(p);
			
			settings.add(Box.createRigidArea(new Dimension(0, Game.getHeightRelativeToScreen(50))));
			
			p = new JPanel(); {
				p.setOpaque(false);
				
				JPanel q = new JPanel(new GridLayout(0,2,30,30)); {
					q.setOpaque(false);
					
					JLabel label = new JLabel("Musique", JLabel.CENTER);
					label.setFont(font1);
					label.setForeground(Color.LIGHT_GRAY);
					q.add(label);
					
					JPanel r = new JPanel(new GridBagLayout()); {
						r.setOpaque(false);
						r.add(music);
					}
					q.add(r);
					
					label = new JLabel("Sons", JLabel.CENTER);
					label.setFont(font1);
					label.setForeground(Color.LIGHT_GRAY);
					q.add(label);
					
					r = new JPanel(new GridBagLayout()); {
						r.setOpaque(false);
						r.add(sounds);
					}
					q.add(r);
					
					r = new JPanel(new GridLayout(0,1)); {
						r.setOpaque(false);
						
						label = new JLabel("Textures", JLabel.CENTER);
						label.setFont(font1);
						label.setForeground(Color.LIGHT_GRAY);
						r.add(label);
						r.add(texturesPackName);
					}
					q.add(r);
		
					
					r = new JPanel(new GridBagLayout()); {
						r.setOpaque(false);
						r.add(texturesPack);
					}
					q.add(r);
				}
				p.add(q);
			}
			settings.add(p);

			p = new JPanel(); {
				p.setOpaque(false);
				p.add(informations);
			}
			settings.add(p);
			
			p = new JPanel(); {
				p.setOpaque(false);
				p.add(backToHomeButton);
			}
			settings.add(p);
			settings.add(Box.createRigidArea(new Dimension(0,Game.getHeightRelativeToScreen(50))));
		}
		add(settings);
		
		JPanel details = new JPanel(); {
			details.setLayout(new BoxLayout(details, BoxLayout.Y_AXIS));
			details.setBorder(BorderFactory.createMatteBorder(0, 5, 0, 0, Color.LIGHT_GRAY));
			details.setBackground(new Color(0,0,0,150));
			
			details.add(Box.createRigidArea(new Dimension(0,Game.getHeightRelativeToScreen(30))));
			
			JPanel p = new JPanel(); {
				p.setOpaque(false);
				JLabel label = new JLabel("Projet"); {
					label.setFont(font1);
					label.setForeground(Color.LIGHT_GRAY);
				}
				p.add(label);
			}
			details.add(p);
			
			p = new JPanel(); {
				p.setOpaque(false);
				JLabel label = new JLabel("Application Informatique (2017)", JLabel.CENTER); {
					label.setFont(font2);
					label.setForeground(color);
				}
				p.add(label);
			}
			details.add(p);
			
			details.add(Box.createRigidArea(new Dimension(0,Game.getHeightRelativeToScreen(100))));
			
			p = new JPanel(); {
				p.setOpaque(false);
				JLabel label = new JLabel("D�veloppeurs"); {
					label.setFont(font1);
					label.setForeground(Color.LIGHT_GRAY);
				}
				p.add(label);
			}
			details.add(p);
			
			p = new JPanel(new GridLayout(2,2)); {
				p.setOpaque(false);
				
				JLabel label = new JLabel("Benjamin LE CREURER", JLabel.CENTER); {
					label.setFont(font2);
					label.setForeground(color);
				}
				p.add(label);
				
				label = new JLabel("Olivia HISSE", JLabel.CENTER); {
					label.setFont(font2);
					label.setForeground(color);
				}
				p.add(label);
				
				label = new JLabel("Yohann LEBIDOIS", JLabel.CENTER); {
					label.setFont(font2);
					label.setForeground(color);
				}
				p.add(label);
				
				label = new JLabel("Romuald LEROUX", JLabel.CENTER); {
					label.setFont(font2);
					label.setForeground(color);
				}
				p.add(label);
			}
			details.add(p);
			
			details.add(Box.createRigidArea(new Dimension(0,Game.getHeightRelativeToScreen(100))));
			
			p = new JPanel(); {
				p.setOpaque(false);
				JLabel label = new JLabel("Tuteur"); {
					label.setFont(font1);
					label.setForeground(Color.LIGHT_GRAY);
				}
				p.add(label);
			}
			details.add(p);
			
			p = new JPanel(); {
				p.setOpaque(false);
				JLabel label = new JLabel("M. Yannick GUESNET", JLabel.CENTER); {
					label.setFont(font2);
					label.setForeground(color);
				}
				p.add(label);
			}
			details.add(p);
			
			details.add(Box.createRigidArea(new Dimension(0,Game.getHeightRelativeToScreen(100))));
			
			p = new JPanel(); {
				p.setOpaque(false);
				JLabel label = new JLabel(); {
					Image logoUniv = new ImageIcon(getClass().getClassLoader().getResource("images/logo_univ.png")).getImage().getScaledInstance(Game.getWidthRelativeToScreen(400), Game.getHeightRelativeToScreen(145), Image.SCALE_DEFAULT);
					label.setIcon(new ImageIcon(logoUniv));
				}
				p.add(label);
			}
			details.add(p);
			
			details.add(Box.createRigidArea(new Dimension(0,Game.getHeightRelativeToScreen(50))));
		}
		add(details);
	}

	@Override
	protected void createController() {
		music.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Image image = null;
				if (Settings.getBooleanValue("music")) {
					Settings.setValue("music", "false");
					image = soundsOff;
				} else {
					Settings.setValue("music", "true");
					image = soundsOn;
				}
				music.setIcon(new ImageIcon(image));
			}
		});
		
		sounds.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Image image = null;
				if (Settings.getBooleanValue("sounds")) {
					Settings.setValue("sounds", "false");
					image = soundsOff;
				} else {
					Settings.setValue("sounds", "true");
					image = soundsOn;
				}
				sounds.setIcon(new ImageIcon(image));
			}
		});
		
		texturesPack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser chooser = new JFileChooser(ResourcesInstaller.GAME_PATH + "/textures");
				chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				int choice = chooser.showOpenDialog(null);
				if (choice == JFileChooser.APPROVE_OPTION) {
					loadTexturePack(chooser.getSelectedFile());		
				}
			}
		});
		
		backToHomeButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				informations.setText(" ");
				showLayer("home");
			}
		});
	}
	
	private void loadTexturePack(File folder) {
		informations.setForeground(error);
		if (!new File(new File(ResourcesInstaller.GAME_PATH + "/textures"), folder.getName()).exists()) {
			informations.setText("Le texture pack doit �tre install� dans le dossier \"textures\"");
			return;
		}
		
		ArrayList<String> filesFound = new ArrayList<String>();
		for (File file : folder.listFiles()) {
			filesFound.add(file.getName());
		}
		
		for (String fileName : REQUIRED_TEXTURES) {
			if (!filesFound.contains(fileName)) {
				informations.setText("Fichier manquant (" + fileName + ")");
				return;
			}
		}
		
		Settings.setValue("textures-pack", folder.getName());
		TexturesManager.init();
		
		texturesPackName.setText(folder.getName());
		informations.setForeground(success);
		informations.setText("Le texture pack "+  folder.getName() + " a �t� charg� avec succ�s");
	}
}