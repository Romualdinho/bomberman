package bomberman.view.layers;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import bomberman.model.GameModel;
import bomberman.model.entity.player.Player;
import bomberman.model.event.GameAdapter;
import bomberman.model.gui.DefaultLobbyTableModel;
import bomberman.view.BombermanButton;
import bomberman.view.BombermanTableRenderer;
import bomberman.view.Game;

public class MultiplayerWaitingLayer extends AbstractBombermanLayer {

	private static final long serialVersionUID = 1L;

	private JTable players;
	private JScrollPane jsp;
	private JButton disconnectButton;
	
	public MultiplayerWaitingLayer(GameModel model) {
		super(model);
	}
	
	protected void createView() {
		players = new JTable(); {
			players.setPreferredSize(new Dimension(Game.getWidthRelativeToScreen(850), Game.getHeightRelativeToScreen(475)));
			players.setOpaque(false);
			players.setBorder(BorderFactory.createLineBorder(Color.WHITE));
	    	
			players.getTableHeader().setDefaultRenderer(new BombermanTableRenderer());
	    	players.setDefaultRenderer(Player.class, new BombermanTableRenderer());
	    	players.setPreferredScrollableViewportSize(players.getPreferredSize());
		}
		
		jsp = new JScrollPane(players); {
			jsp.setOpaque(false);
			jsp.getViewport().setOpaque(false);
		}
		
		disconnectButton = new BombermanButton("DECONNEXION"); {
			disconnectButton.setPreferredSize(new Dimension(Game.getWidthRelativeToScreen(450), disconnectButton.getPreferredSize().height));
		}
	}
	protected void placeComponents() {
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		
		add(Box.createRigidArea(new Dimension(0,Game.getHeightRelativeToScreen(75))));
		
		JPanel p = new JPanel(); {
			p.setOpaque(false);
			JLabel title = new JLabel("MULTIJOUEUR"); {
				title.setForeground(Color.LIGHT_GRAY);
				title.setFont(Game.BOMBERMAN_FONT.deriveFont(Font.BOLD, Game.getWidthRelativeToScreen(120)));
			}
			p.add(title);
		}
		add(p);
		
		p = new JPanel(); {
			p.setOpaque(false);
			p.add(jsp);
		}
		add(p);
		
		add(Box.createRigidArea(new Dimension(0,Game.getHeightRelativeToScreen(90))));
		
		p = new JPanel(); {
			p.setOpaque(false);
			p.add(disconnectButton);
		}
		add(p);
		
		add(Box.createRigidArea(new Dimension(0,Game.getHeightRelativeToScreen(90))));
	}

	protected void createController() {
		model.addGameListener(new GameAdapter() {
			public void serverJoined() {
				DefaultLobbyTableModel tableModel = new DefaultLobbyTableModel(model);
				players.setModel(tableModel);
				players.setRowHeight(players.getPreferredSize().height / tableModel.getRowCount());
				players.setPreferredScrollableViewportSize(players.getPreferredSize());
			}
		});
		
		disconnectButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				model.exit();
				showLayer("home");
			}
		});
	}
}
