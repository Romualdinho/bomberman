package bomberman.view.layers;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Image;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import bomberman.model.GameModel;
import bomberman.model.entity.bomb.StockBomb;
import bomberman.model.event.GameAdapter;
import bomberman.view.Countdown;
import bomberman.view.Game;
import bomberman.view.SoundManager;

public class GameLayer extends AbstractBombermanLayer {

	private static final long serialVersionUID = 1L;

	private Countdown countdown;
	private JLabel rounds;
	private JLabel bombStock;
	private JLabel bombRange;
	private JLabel bombTime;
	
	public GameLayer(GameModel model) {
		super(model);
	}

	protected void createView() {
		final Font font = Game.BOMBERMAN_FONT.deriveFont(Font.PLAIN, Game.getWidthRelativeToScreen(60));
		
		int countdownSize = 80;
		countdown = new Countdown(GameModel.ROUND_DURATION, Countdown.LONG_COUNTDOWN, countdownSize, false); {
			int padding = -(int)(Game.getWidthRelativeToScreen(countdownSize) / 2.6);
			Border paddingBorder = BorderFactory.createEmptyBorder(padding,0,padding,0);
			countdown.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEmptyBorder(), paddingBorder));
		}
		
		rounds = new JLabel(); {
			int roundsSize = Game.getWidthRelativeToScreen(40);
			rounds.setFont(Game.BOMBERMAN_FONT.deriveFont(Font.PLAIN, roundsSize));
			rounds.setForeground(Color.LIGHT_GRAY);
			int padding = -(int)(roundsSize / 2.6);
			Border paddingBorder = BorderFactory.createEmptyBorder(padding,0,padding,0);
			rounds.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEmptyBorder(), paddingBorder));
		}
		
		bombStock = new JLabel(); {
			bombStock.setFont(font);
			bombStock.setForeground(Color.LIGHT_GRAY);
		}
		
		bombRange = new JLabel(); {
			bombRange.setFont(font);
			bombRange.setForeground(Color.LIGHT_GRAY);
		}
		
		bombTime = new JLabel(); {
			bombTime.setFont(font);
			bombTime.setForeground(Color.LIGHT_GRAY);
		}
	}

	protected void placeComponents() {
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		
		JPanel p = new JPanel(); {
			p.setOpaque(false);
			JPanel q = new JPanel(new GridLayout(0,1)); {
				q.setOpaque(false);
				JPanel r = new JPanel(); {
					r.setOpaque(false);
					r.add(countdown);
				}
				q.add(r);
				
				r = new JPanel(); {
					r.setOpaque(false);
					r.add(rounds);
				}
				q.add(r);
			}
			p.add(q);
		}
		add(p);
		
		add(Box.createRigidArea(new Dimension(0, Game.getHeightRelativeToScreen(810))));
		
		p = new JPanel(); {
			//p.setBackground(new Color(0,0,0,100));
			p.setOpaque(false);
			JPanel q = new JPanel(new GridLayout(1, 0, Game.getHeightRelativeToScreen(150), 0)); {
				q.setOpaque(false);
				JPanel r = new JPanel(); {
					r.setOpaque(false);
					JLabel label = new JLabel(); {
						Image img = new ImageIcon(getClass().getClassLoader().getResource("images/stock_bomb.png")).getImage().getScaledInstance(Game.getWidthRelativeToScreen(80), Game.getHeightRelativeToScreen(80), Image.SCALE_DEFAULT);
						label.setIcon(new ImageIcon(img));
					}
					r.add(label);
					r.add(bombStock);
				}
				q.add(r);
				
				r = new JPanel(); {
					r.setOpaque(false);
					JLabel label = new JLabel(); {
						Image img = new ImageIcon(getClass().getClassLoader().getResource("images/bomb_range.png")).getImage().getScaledInstance(Game.getWidthRelativeToScreen(80), Game.getHeightRelativeToScreen(80), Image.SCALE_DEFAULT);
						label.setIcon(new ImageIcon(img));
					}
					r.add(label);
					r.add(bombRange);
				}
				q.add(r);
				
				r = new JPanel(); {
					r.setOpaque(false);
					JLabel label = new JLabel(); {
						Image img = new ImageIcon(getClass().getClassLoader().getResource("images/bomb_time.png")).getImage().getScaledInstance(Game.getWidthRelativeToScreen(80), Game.getHeightRelativeToScreen(80), Image.SCALE_DEFAULT);
						label.setIcon(new ImageIcon(img));
					}
					r.add(label);
					r.add(bombTime);
				}
				q.add(r);
			}
			p.add(q);
		}
		add(p);
	}

	protected void createController() {
		model.addGameListener(new GameAdapter() {
			public void roundStarted() {
				countdown.restart();
				rounds.setText((model.getTotalRounds() - model.getRounds() + 1) + "/" + model.getTotalRounds());
				updateHUD();
			}
			
			public void playerBonusUpdated() {
				SoundManager.playSoundEffect("bonus.wav");
				updateHUD();
			}
			
			public void gamePaused() {
				// On met en pause le compte � rebours uniquement si il ne s'agit pas d'une partie multijoueur
				if (model.getIPServer() == null) {
					countdown.pause();
				}
			}
			
			public void gameResumed() {
				// On met en pause le compte � rebours uniquement si il ne s'agit pas d'une partie multijoueur
				if (model.getIPServer() == null) {
					countdown.resume();
				}
			}
		});
		
		countdown.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				if (model.inGame()) {
					SoundManager.playSoundEffect("round_ended.wav");
				}
				model.roundTimedOut();
			}
		});
	}
	
	private void updateHUD() {
		StockBomb playerBombs = model.getPlayers().get(0).getBombs();
		bombStock.setText(String.valueOf(playerBombs.getStock().size()));
		bombRange.setText(String.valueOf(playerBombs.getStdBomb().getRange()));
		bombTime.setText(String.valueOf(playerBombs.getStdBomb().getTime()));
	}
}