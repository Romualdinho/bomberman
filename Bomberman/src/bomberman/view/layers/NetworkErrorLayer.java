package bomberman.view.layers;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import bomberman.model.GameModel;
import bomberman.model.event.GameAdapter;
import bomberman.view.BombermanButton;
import bomberman.view.Game;

public class NetworkErrorLayer extends AbstractBombermanLayer {

	private static final long serialVersionUID = 1L;

	private JLabel error;
	private JButton backToHomeButton;
	
	public NetworkErrorLayer(GameModel model) {
		super(model);
	}

	protected void createView() {
		error = new JLabel("Connexion avec le serveur interrompue", JLabel.CENTER);
		error.setFont(Game.BOMBERMAN_FONT.deriveFont(Font.PLAIN, Game.getWidthRelativeToScreen(60)));
		error.setForeground(new Color(255,102,102));
		
		backToHomeButton = new BombermanButton("QUITTER"); {
			backToHomeButton.setPreferredSize(new Dimension(Game.getWidthRelativeToScreen(350), backToHomeButton.getPreferredSize().height));
		}
	}

	protected void placeComponents() {
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		
		add(Box.createRigidArea(new Dimension(0,Game.getHeightRelativeToScreen(90))));
		
		JPanel p = new JPanel(); {
			p.setOpaque(false);
			JLabel label = new JLabel(); {
				Image logoError = new ImageIcon(getClass().getClassLoader().getResource("images/network_error.png")).getImage().getScaledInstance(Game.getWidthRelativeToScreen(400), Game.getHeightRelativeToScreen(400), Image.SCALE_DEFAULT);
				label.setIcon(new ImageIcon(logoError));
			}
			p.add(label);
		}
		add(p);
		
		p = new JPanel(); {
			p.setOpaque(false);
			p.add(error);
		}
		add(p);
		
		add(Box.createRigidArea(new Dimension(0,Game.getHeightRelativeToScreen(110))));
		
		p = new JPanel(); {
			p.setOpaque(false);
			p.add(backToHomeButton);
		}
		add(p);
		
		add(Box.createRigidArea(new Dimension(0,Game.getHeightRelativeToScreen(150))));
	}

	protected void createController() {
		model.addGameListener(new GameAdapter() {
			public void networkError(String errorValue) {
				error.setText(errorValue);
			}
		});
		
		backToHomeButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				model.exit();
			}
		});
	}
}