package bomberman.view.layers;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import bomberman.model.GameModel;
import bomberman.view.BombermanButton;
import bomberman.view.Game;

public class MultiplayerMenuLayer extends AbstractBombermanLayer {

	private static final long serialVersionUID = 1L;
	
	private JButton createMultiGameButton;
	private JButton joinMultiGameButton;
	private JButton backToHomeButton;
	
	public MultiplayerMenuLayer(GameModel model) {
		super(model);
	}
	
	public void createView() {	
		createMultiGameButton = new BombermanButton("CREER"); {
			createMultiGameButton.setPreferredSize(new Dimension(Game.getWidthRelativeToScreen(350), createMultiGameButton.getPreferredSize().height));
		}

		joinMultiGameButton = new BombermanButton("REJOINDRE"); {
			joinMultiGameButton.setPreferredSize(new Dimension(Game.getWidthRelativeToScreen(350), joinMultiGameButton.getPreferredSize().height));
		}
		backToHomeButton = new BombermanButton("RETOUR"); {
			backToHomeButton.setPreferredSize(new Dimension(Game.getWidthRelativeToScreen(350), backToHomeButton.getPreferredSize().height));
		}
	}
	
	public void placeComponents() {
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		
		add(Box.createRigidArea(new Dimension(0,Game.getHeightRelativeToScreen(75))));
		
		JPanel p = new JPanel(); {
			p.setOpaque(false);
			JLabel title = new JLabel("MULTIJOUEUR"); {
				title.setForeground(Color.LIGHT_GRAY);
				title.setFont(Game.BOMBERMAN_FONT.deriveFont(Font.BOLD, Game.getWidthRelativeToScreen(120)));
			}
			p.add(title);
		}
		add(p);
		
		add(Box.createRigidArea(new Dimension(0,Game.getHeightRelativeToScreen(170))));
		
		p = new JPanel(); {
			p.setOpaque(false);
			p.add(createMultiGameButton);
		}
		add(p);
		
		p = new JPanel(); {
			 p.setOpaque(false);
				p.add(joinMultiGameButton);
			}
		add(p);
		
		add(Box.createRigidArea(new Dimension(0,Game.getHeightRelativeToScreen(200))));
		
		p = new JPanel(); {
			 p.setOpaque(false);
				p.add(backToHomeButton);
			}
		add(p);
		
		add(Box.createRigidArea(new Dimension(0,Game.getHeightRelativeToScreen(250))));
	}

	public void createController() {
		createMultiGameButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				showLayer("multiplayer_create");
			}
		});
		
		joinMultiGameButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				showLayer("multiplayer_join");
			}
		});
		
		backToHomeButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				showLayer("home");
			}
		});
	}
}