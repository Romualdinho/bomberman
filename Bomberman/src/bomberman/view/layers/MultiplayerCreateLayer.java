package bomberman.view.layers;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.SocketException;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import bomberman.model.GameModel;
import bomberman.model.entity.player.StdPlayer;
import bomberman.view.BombermanButton;
import bomberman.view.Game;
import bomberman.view.GridSelector;

public class MultiplayerCreateLayer extends AbstractBombermanLayer {

	private static final long serialVersionUID = 1L;
	
	private JLabel selectedPlayersLabel;
	private JSlider selectedPlayersSlider;
	private JLabel selectedRoundsLabel;
	private JSlider selectedRoundsSlider;
	private GridSelector gridSelector;
	private JTextField username;
	private JLabel error;
	private JButton createMultiGameButton;
	private JButton backToHomeButton;
	
	public MultiplayerCreateLayer(GameModel model) {
		super(model);
	}
	
	public void createView() {
		final Font font1 = Game.BOMBERMAN_FONT.deriveFont(Font.PLAIN, Game.getWidthRelativeToScreen(60));
		final Font font2 = Game.BOMBERMAN_FONT.deriveFont(Font.PLAIN, Game.getWidthRelativeToScreen(40));
		final Color bgColor = new Color(0,0,0,80);
		
		selectedPlayersLabel = new JLabel("4 joueurs", JLabel.CENTER); {
			selectedPlayersLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
			selectedPlayersLabel.setForeground(Color.LIGHT_GRAY);
			selectedPlayersLabel.setFont(font1);
		}
		
		selectedPlayersSlider = new JSlider(JSlider.HORIZONTAL, GameModel.MIN_PLAYERS, GameModel.MAX_PLAYERS, 4); {
			selectedPlayersSlider.setOpaque(false);
			selectedPlayersSlider.setPreferredSize(new Dimension(Game.getWidthRelativeToScreen(500), selectedPlayersSlider.getPreferredSize().height));
		}
		
		selectedRoundsLabel = new JLabel("10 rounds", JLabel.CENTER); {
			selectedRoundsLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
			selectedRoundsLabel.setForeground(Color.LIGHT_GRAY);
			selectedRoundsLabel.setFont(font1);
		}
		
		selectedRoundsSlider = new JSlider(JSlider.HORIZONTAL, GameModel.MIN_ROUNDS, GameModel.MAX_ROUNDS, 10); {
			selectedRoundsSlider.setOpaque(false);
			selectedRoundsSlider.setPreferredSize(new Dimension(Game.getWidthRelativeToScreen(500), selectedRoundsSlider.getPreferredSize().height));
		}
		
		gridSelector = new GridSelector();
		
		username = new JTextField(); {
			username.setPreferredSize(new Dimension(Game.getWidthRelativeToScreen(500), Game.getHeightRelativeToScreen(65)));
			username.setAlignmentX(Component.CENTER_ALIGNMENT);
			username.setForeground(Color.LIGHT_GRAY);
			username.setBackground(bgColor);
			username.setFont(font1);
			username.setCaretColor(Color.LIGHT_GRAY);
		}
		
		error = new JLabel(" "); {
			error.setForeground(new Color(255,102,102));
			error.setFont(font2);
		}
		
		createMultiGameButton = new BombermanButton("CREER"); {
			createMultiGameButton.setPreferredSize(new Dimension(Game.getWidthRelativeToScreen(350), createMultiGameButton.getPreferredSize().height));
		}

		backToHomeButton = new BombermanButton("RETOUR"); {
			backToHomeButton.setPreferredSize(new Dimension(Game.getWidthRelativeToScreen(350), backToHomeButton.getPreferredSize().height));
		}
	}
	
	public void placeComponents() {
		Font font = Game.BOMBERMAN_FONT.deriveFont(Font.PLAIN, Game.getWidthRelativeToScreen(40));
		
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		
		add(Box.createRigidArea(new Dimension(0,Game.getHeightRelativeToScreen(75))));
		
		JPanel p = new JPanel(); {
			p.setOpaque(false);
			JLabel title = new JLabel("MULTIJOUEUR"); {
				title.setForeground(Color.LIGHT_GRAY);
				title.setFont(Game.BOMBERMAN_FONT.deriveFont(Font.BOLD, Game.getWidthRelativeToScreen(120)));
			}
			p.add(title);
		}
		add(p);
		
		p = new JPanel(); {
			p.setOpaque(false);
			
			JPanel q = new JPanel(new GridLayout(1,2,50,0)); {
				q.setOpaque(false);
				
				JPanel r = new JPanel(new GridLayout(0,1)); {
					r.setOpaque(false);
					r.add(selectedPlayersLabel);
					r.add(selectedPlayersSlider);
					r.add(selectedRoundsLabel);
					r.add(selectedRoundsSlider);
				}
				q.add(r);
				q.add(gridSelector);
			}
			p.add(q);
		}
		add(p);
		
		p = new JPanel(); {
			p.setOpaque(false);
			
			JPanel q = new JPanel(new GridLayout(1,2,50,0)); {
				q.setOpaque(false);
				
				JPanel r = new JPanel(new GridLayout(0,1)); {
					r.setOpaque(false);
					r.add(selectedPlayersLabel);
					r.add(selectedPlayersSlider);
					r.add(selectedRoundsLabel);
					r.add(selectedRoundsSlider);
				}
				q.add(r);
				q.add(gridSelector);
			}
			p.add(q);
		}
		add(p);
		
		add(Box.createRigidArea(new Dimension(0,Game.getHeightRelativeToScreen(50))));
		
		
		p = new JPanel(); {
			p.setOpaque(false);
			
			JPanel q = new JPanel(new GridLayout(1,0,Game.getWidthRelativeToScreen(30), 0)); {
				q.setOpaque(false);
				
				JPanel r = new JPanel(new FlowLayout(FlowLayout.RIGHT)); {
					r.setOpaque(false);
					JLabel label = new JLabel("Pseudo", JLabel.RIGHT); {
						label.setForeground(Color.LIGHT_GRAY);
						label.setFont(font);
					}
					r.add(label);
				}
				q.add(r);
				
				r = new JPanel(); {
					r.setOpaque(false);
					r.add(username);
				}
				q.add(r);
				
				r = new JPanel(new FlowLayout(FlowLayout.LEFT)); {
					r.setOpaque(false);
					r.add(error);
				}
				q.add(r);
			}
			p.add(q);
		}
		add(p);
		
		add(Box.createRigidArea(new Dimension(0,Game.getHeightRelativeToScreen(60))));

		p = new JPanel(new FlowLayout(FlowLayout.CENTER, 50, 0)); {
			p.setOpaque(false);
			p.add(createMultiGameButton);
			p.add(backToHomeButton);
		}
		add(p);
		add(Box.createRigidArea(new Dimension(0,Game.getHeightRelativeToScreen(60))));
	}

	public void createController() {
		gridSelector.setGrids(model.getAvailableGrids(selectedPlayersSlider.getValue()));
		
		selectedPlayersSlider.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				int value = selectedPlayersSlider.getValue();
				selectedPlayersLabel.setText(value + " joueurs");
				gridSelector.setGrids(model.getAvailableGrids(value));
				createMultiGameButton.setEnabled(gridSelector.getSelectedGrid() != null ? true : false);
			}
		});
		
		selectedRoundsSlider.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				selectedRoundsLabel.setText(selectedRoundsSlider.getValue() + " rounds");
			}
		});
		
		createMultiGameButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				String usernameValue = username.getText().trim();
				if (usernameValue.isEmpty()) {
					error.setText("Veuillez saisir votre pseudo");
					return;
				}
				
				try {
					new StdPlayer(usernameValue);
				} catch (AssertionError ae) {
					error.setText("Pseudo invalide");
					return;
				}
				
				try {
					model.createMultiplayerGame(usernameValue, gridSelector.getSelectedGrid(), selectedPlayersSlider.getValue(), selectedRoundsSlider.getValue());
					error.setText(" ");
					showLayer("multiplayer_lobby");
				} catch (SocketException e1) {
					model.networkError("Un serveur est d�j� lanc� sur cet ordinateur");
				}
			}
		});
		
		backToHomeButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				showLayer("home");
			}
		});
	}
}