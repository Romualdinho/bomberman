package bomberman.view.layers;

import java.awt.CardLayout;

import javax.swing.JPanel;

import bomberman.model.GameModel;
import bomberman.view.Game;

public abstract class AbstractBombermanLayer extends JPanel {

	private static final long serialVersionUID = 1L;
	
	protected GameModel model;
	
	public AbstractBombermanLayer(GameModel model) {
		super();
		this.model = model;
		
		setOpaque(false);
		
		createView();
		placeComponents();
		createController();
	}
	
	protected void showLayer(String idLayer) {
		Game.CURRENT_LAYER = idLayer;
		((CardLayout)(getParent().getLayout())).show(getParent(), idLayer);
	}
	
	protected abstract void createView();
	protected abstract void placeComponents();
	protected abstract void createController();
}