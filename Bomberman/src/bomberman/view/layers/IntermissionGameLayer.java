package bomberman.view.layers;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import bomberman.model.GameModel;
import bomberman.model.event.GameAdapter;
import bomberman.view.Countdown;
import bomberman.view.Game;

public class IntermissionGameLayer extends AbstractBombermanLayer {
	
	private static final long serialVersionUID = 1L;
	
	private Countdown countdown;
	
	public IntermissionGameLayer(GameModel model) {
		super(model);
	}

	protected void createView() {
		countdown = new Countdown(GameModel.INTERMISSION_DURATION, Countdown.SHORT_COUNTDOWN, 100, true);
	}

	protected void placeComponents() {
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		
		add(Box.createRigidArea(new Dimension(0,Game.getHeightRelativeToScreen(300))));
		
		JPanel p = new JPanel(); {
			p.setOpaque(false);
			JLabel nextRound = new JLabel("PROCHAIN ROUND"); {
				nextRound.setForeground(Color.LIGHT_GRAY);
				nextRound.setFont(Game.BOMBERMAN_FONT.deriveFont(Font.PLAIN, Game.getWidthRelativeToScreen(80)));
			}
			p.add(nextRound);
		}
		add(p);
		
		p = new JPanel(); {
			p.setOpaque(false);
			p.add(countdown);
		}
		add(p);
		
		add(Box.createRigidArea(new Dimension(0, Game.getHeightRelativeToScreen(400))));
	}

	protected void createController() {
		model.addGameListener(new GameAdapter() {
			public void roundEnded() {
				if (model.getRounds() > 1) {
					countdown.restart();
				}
			}
		});
		
		countdown.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				model.startNextRound();
			}
		});
	}
}