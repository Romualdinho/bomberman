package bomberman.view.layers;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.border.Border;

import bomberman.model.GameModel;
import bomberman.model.event.GameAdapter;
import bomberman.view.Countdown;
import bomberman.view.Game;

public class EmptyLayer extends AbstractBombermanLayer {

	private static final long serialVersionUID = 1L;

	private Countdown countdown;
	
	public EmptyLayer(GameModel model) {
		super(model);
	}

	@Override
	protected void createView() {
		int countdownSize = 80;
		countdown = new Countdown(GameModel.ROUND_DURATION, Countdown.LONG_COUNTDOWN, countdownSize, false); {
			int padding = -(int)(Game.getWidthRelativeToScreen(countdownSize) / 2.6);
			Border paddingBorder = BorderFactory.createEmptyBorder(padding,0,padding,0);
			countdown.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEmptyBorder(), paddingBorder));
		}
	}

	@Override
	protected void placeComponents() {
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		JPanel p = new JPanel(); {
			p.setOpaque(false);
			p.add(countdown);
		}
		add(p);
	}

	@Override
	protected void createController() {
		model.addGameListener(new GameAdapter() {
			public void roundStarted() {
				countdown.restart();
			}
		});
	}
}