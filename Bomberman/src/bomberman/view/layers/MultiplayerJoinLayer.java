package bomberman.view.layers;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import bomberman.model.GameModel;
import bomberman.model.entity.player.StdPlayer;
import bomberman.view.BombermanButton;
import bomberman.view.Game;

public class MultiplayerJoinLayer extends AbstractBombermanLayer {

	private static final long serialVersionUID = 1L;
	
	private JTextField username;
	private JTextField ip;
	private JLabel error;
	private JButton connectionButton;
	private JButton backToMultiplayerButton;
	
	public MultiplayerJoinLayer(GameModel model) {
		super(model);
	}
	
	public void createView() {
		final Font font1 = Game.BOMBERMAN_FONT.deriveFont(Font.PLAIN, Game.getWidthRelativeToScreen(55));
		final Font font2 = Game.BOMBERMAN_FONT.deriveFont(Font.PLAIN, Game.getWidthRelativeToScreen(40));
		final Color bgColor = new Color(0,0,0,80);
		
		username = new JTextField(); {
			username.setPreferredSize(new Dimension(Game.getWidthRelativeToScreen(500), Game.getHeightRelativeToScreen(65)));
			username.setAlignmentX(Component.CENTER_ALIGNMENT);
			username.setForeground(Color.LIGHT_GRAY);
			username.setBackground(bgColor);
			username.setFont(font1);
			username.setCaretColor(Color.LIGHT_GRAY);
		}
		
		ip = new JTextField(); {
			ip.setPreferredSize(new Dimension(Game.getWidthRelativeToScreen(500), Game.getHeightRelativeToScreen(65)));
			ip.setAlignmentX(Component.CENTER_ALIGNMENT);
			ip.setForeground(Color.LIGHT_GRAY);
			ip.setBackground(bgColor);
			ip.setFont(font1);
			ip.setCaretColor(Color.LIGHT_GRAY);
		}
		
		error = new JLabel(" "); {
			error.setForeground(new Color(255,102,102));
			error.setFont(font2);
		}
		
		connectionButton = new BombermanButton("CONNEXION"); {
			connectionButton.setPreferredSize(new Dimension(Game.getWidthRelativeToScreen(350), connectionButton.getPreferredSize().height));
		}

		backToMultiplayerButton = new BombermanButton("RETOUR"); {
			backToMultiplayerButton.setPreferredSize(new Dimension(Game.getWidthRelativeToScreen(350), backToMultiplayerButton.getPreferredSize().height));
		}
	}
	
	public void placeComponents() {
		Font font = Game.BOMBERMAN_FONT.deriveFont(Font.PLAIN, Game.getWidthRelativeToScreen(40));
		
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		
		add(Box.createRigidArea(new Dimension(0,Game.getHeightRelativeToScreen(75))));
		
		JPanel p = new JPanel(); {
			p.setOpaque(false);
			JLabel title = new JLabel("MULTIJOUEUR"); {
				title.setForeground(Color.LIGHT_GRAY);
				title.setFont(Game.BOMBERMAN_FONT.deriveFont(Font.BOLD, Game.getWidthRelativeToScreen(120)));
			}
			p.add(title);
		}
		add(p);
		
		add(Box.createRigidArea(new Dimension(0,Game.getHeightRelativeToScreen(40))));

		p = new JPanel(new GridLayout(0,1)); {
			p.setOpaque(false);
			JLabel label = new JLabel("Pseudo", JLabel.CENTER); {
				label.setForeground(Color.LIGHT_GRAY);
				label.setFont(font);
			}
			p.add(label);
			JPanel q = new JPanel(); {
				q.setOpaque(false);
				q.add(username);
			}
			p.add(q);
		}
		add(p);
		
		p = new JPanel(new GridLayout(0,1)); {
			p.setOpaque(false);
			JLabel label = new JLabel("Adresse IP", JLabel.CENTER); {
				label.setForeground(Color.LIGHT_GRAY);
				label.setFont(font);
			}
			p.add(label);
			JPanel q = new JPanel(); {
				q.setOpaque(false);
				q.add(ip);
			}
			p.add(q);
		}
		add(p);
		
		add(Box.createRigidArea(new Dimension(0,Game.getHeightRelativeToScreen(30))));
		
		p = new JPanel(); {
			p.setOpaque(false);
			p.add(error);
		}
		add(p);
		
		add(Box.createRigidArea(new Dimension(0,Game.getHeightRelativeToScreen(30))));
		
		p = new JPanel(new FlowLayout(FlowLayout.CENTER, 50, 0)); {
			p.setOpaque(false);
			p.add(connectionButton);
			p.add(backToMultiplayerButton);
		}
		add(p);
		
		add(Box.createRigidArea(new Dimension(0,Game.getHeightRelativeToScreen(150))));
	}

	public void createController() {
		connectionButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String usernameValue = username.getText().trim();
				String ipValue = ip.getText().trim();
				
				if (usernameValue.isEmpty()) {
					error.setText("Veuillez saisir votre pseudo");
					return;
				}
				
				if (ipValue.isEmpty()) {
					error.setText("Veuillez saisir une adresse IP");
					return;
				}
				
				try {
					new StdPlayer(usernameValue);
				} catch (AssertionError ae) {
					error.setText("Pseudo invalide");
					return;
				}
		
				if (!model.joinMultiplayerGame(ip.getText().trim(), username.getText().trim())) {
					error.setText("Aucune r�ponse du serveur");
					return;
				}
				ip.setText("");
				error.setText(" ");
				showLayer("multiplayer_waiting");
			}
		});
		
		backToMultiplayerButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ip.setText("");
				error.setText(" ");
				showLayer("home");
			}
		});
	}
}