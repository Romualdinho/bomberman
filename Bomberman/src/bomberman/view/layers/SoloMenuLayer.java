package bomberman.view.layers;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import bomberman.model.GameModel;
import bomberman.view.BombermanButton;
import bomberman.view.Game;
import bomberman.view.GridSelector;

public class SoloMenuLayer extends AbstractBombermanLayer {

	private static final long serialVersionUID = 1L;
	
	private JLabel selectedPlayersLabel;
	private JSlider selectedPlayersSlider;
	private JLabel selectedRoundsLabel;
	private JSlider selectedRoundsSlider;
	private GridSelector gridSelector;
	private JButton launchSoloGameButton;
	private JButton backToHomeButton;
	
	public SoloMenuLayer(GameModel model) {
		super(model);
	}
	
	public void createView() {
		Font font = Game.BOMBERMAN_FONT.deriveFont(Font.PLAIN, Game.getWidthRelativeToScreen(60));
		
		selectedPlayersLabel = new JLabel("4 joueurs", JLabel.CENTER); {
			selectedPlayersLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
			selectedPlayersLabel.setForeground(Color.LIGHT_GRAY);
			selectedPlayersLabel.setFont(font);
		}
		
		selectedPlayersSlider = new JSlider(JSlider.HORIZONTAL, GameModel.MIN_PLAYERS, GameModel.MAX_PLAYERS, 4); {
			selectedPlayersSlider.setOpaque(false);
			selectedPlayersSlider.setPreferredSize(new Dimension(Game.getWidthRelativeToScreen(500), selectedPlayersSlider.getPreferredSize().height));
		}
		
		selectedRoundsLabel = new JLabel("10 rounds", JLabel.CENTER); {
			selectedRoundsLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
			selectedRoundsLabel.setForeground(Color.LIGHT_GRAY);
			selectedRoundsLabel.setFont(font);
		}
		
		selectedRoundsSlider = new JSlider(JSlider.HORIZONTAL, GameModel.MIN_ROUNDS, GameModel.MAX_ROUNDS, 10); {
			selectedRoundsSlider.setOpaque(false);
			selectedRoundsSlider.setPreferredSize(new Dimension(Game.getWidthRelativeToScreen(500), selectedRoundsSlider.getPreferredSize().height));
		}
		
		gridSelector = new GridSelector();
		
		launchSoloGameButton = new BombermanButton("LANCER"); {
			launchSoloGameButton.setPreferredSize(new Dimension(Game.getWidthRelativeToScreen(350), launchSoloGameButton.getPreferredSize().height));
		}
		
		backToHomeButton = new BombermanButton("RETOUR"); {
			backToHomeButton.setPreferredSize(new Dimension(Game.getWidthRelativeToScreen(350), backToHomeButton.getPreferredSize().height));
		}
	}
	
	public void placeComponents() {
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		
		add(Box.createRigidArea(new Dimension(0,Game.getHeightRelativeToScreen(75))));
		
		JPanel p = new JPanel(); {
			p.setOpaque(false);
			JLabel title = new JLabel("SOLO"); {
				title.setForeground(Color.LIGHT_GRAY);
				title.setFont(Game.BOMBERMAN_FONT.deriveFont(Font.BOLD, Game.getWidthRelativeToScreen(120)));
			}
			p.add(title);
		}
		add(p);
		
		add(Box.createRigidArea(new Dimension(0,Game.getHeightRelativeToScreen(50))));
		
		p = new JPanel(); {
			p.setOpaque(false);
			
			JPanel q = new JPanel(new GridLayout(1,2,50,0)); {
				q.setOpaque(false);
				
				JPanel r = new JPanel(new GridLayout(0,1)); {
					r.setOpaque(false);
					r.add(selectedPlayersLabel);
					r.add(selectedPlayersSlider);
					r.add(selectedRoundsLabel);
					r.add(selectedRoundsSlider);
				}
				q.add(r);
				q.add(gridSelector);
			}
			p.add(q);
		}
		add(p);
		
		add(Box.createRigidArea(new Dimension(0,Game.getHeightRelativeToScreen(100))));
		
		p = new JPanel(new FlowLayout(FlowLayout.CENTER, 50, 0)); {
			p.setOpaque(false);
			p.add(launchSoloGameButton);
			p.add(backToHomeButton);
		}
		add(p);
		add(Box.createRigidArea(new Dimension(0,Game.getHeightRelativeToScreen(150))));
	}

	public void createController() {
		gridSelector.setGrids(model.getAvailableGrids(selectedPlayersSlider.getValue()));
		
		selectedPlayersSlider.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				int value = selectedPlayersSlider.getValue();
				selectedPlayersLabel.setText(value + " joueurs");
				gridSelector.setGrids(model.getAvailableGrids(value));
				launchSoloGameButton.setEnabled(gridSelector.getSelectedGrid() != null ? true : false);
			}
		});
		
		selectedRoundsSlider.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				selectedRoundsLabel.setText(selectedRoundsSlider.getValue() + " rounds");
			}
		});
		
		launchSoloGameButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				model.startSoloGame(gridSelector.getSelectedGrid(), selectedPlayersSlider.getValue(), selectedRoundsSlider.getValue());
			}
		});
		
		backToHomeButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				showLayer("home");
			}
		});
	}
}