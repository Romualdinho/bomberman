package bomberman.view.layers;

import java.awt.Dimension;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import bomberman.model.GameModel;
import bomberman.view.BombermanButton;
import bomberman.view.Game;

public class HomeMenuLayer extends AbstractBombermanLayer {

	private static final long serialVersionUID = 1L;
	
	private JButton soloButton;
	private JButton multiplayerButton;
	private JButton settingsButton;
	private JButton quitButton;
	
	public HomeMenuLayer(GameModel model) {
		super(model);
	}

	@Override
	protected void createView() {
		soloButton = new BombermanButton("SOLO"); {
			soloButton.setPreferredSize(new Dimension(Game.getWidthRelativeToScreen(500), soloButton.getPreferredSize().height));
		}
		
		multiplayerButton = new BombermanButton("MULTIJOUEUR"); {
			multiplayerButton.setPreferredSize(new Dimension(Game.getWidthRelativeToScreen(500), multiplayerButton.getPreferredSize().height));
		}
		
		settingsButton = new BombermanButton("PARAMETRES"); {
			settingsButton.setPreferredSize(new Dimension(Game.getWidthRelativeToScreen(500), settingsButton.getPreferredSize().height));
		}
		
		quitButton = new BombermanButton("QUITTER"); {
			quitButton.setPreferredSize(new Dimension(Game.getWidthRelativeToScreen(500), quitButton.getPreferredSize().height));
		}
	}

	@Override
	protected void placeComponents() {
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		
		add(Box.createRigidArea(new Dimension(0,Game.getHeightRelativeToScreen(150))));
		
		JPanel p = new JPanel(); {
			p.setOpaque(false);
			JLabel label = new JLabel(); {
				Image bomberman = new ImageIcon(getClass().getClassLoader().getResource("images/bomberman_title.gif")).getImage().getScaledInstance(Game.getWidthRelativeToScreen(900), Game.getHeightRelativeToScreen(250), Image.SCALE_DEFAULT);
				label.setIcon(new ImageIcon(bomberman));
			}
			p.add(label);
		}
		add(p);
			
		add(Box.createRigidArea(new Dimension(0,Game.getHeightRelativeToScreen(75))));
	
		p = new JPanel(); {
			p.setOpaque(false);
			p.add(soloButton);
		}
		add(p);
		
		p = new JPanel(); {
			p.setOpaque(false);
			p.add(multiplayerButton);
		}
		add(p);
		
		p = new JPanel(); {
			p.setOpaque(false);
			p.add(settingsButton);
		}
		add(p);
		
		p = new JPanel(); {
			p.setOpaque(false);
			p.add(quitButton);
		}
		add(p);
		add(Box.createRigidArea(new Dimension(0,Game.getHeightRelativeToScreen(250))));
	}

	@Override
	protected void createController() {
		soloButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				showLayer("solo");
			}
		});
		
		multiplayerButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				showLayer("multiplayer");
			}
		});
		
		settingsButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				showLayer("settings");
			}
		});
		
		quitButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
	}
}