package bomberman.view;
import java.awt.CardLayout;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.OverlayLayout;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import bomberman.model.GameModel;
import bomberman.model.StdGameModel;
import bomberman.model.event.GameAdapter;
import bomberman.view.binding.BindingManager;
import bomberman.view.layers.EmptyLayer;
import bomberman.view.layers.EndGameLayer;
import bomberman.view.layers.MultiplayerWaitingLayer;
import bomberman.view.layers.GameLayer;
import bomberman.view.layers.HomeMenuLayer;
import bomberman.view.layers.IntermissionGameLayer;
import bomberman.view.layers.MultiplayerLobbyLayer;
import bomberman.view.layers.MultiplayerMenuLayer;
import bomberman.view.layers.NetworkErrorLayer;
import bomberman.view.layers.PauseGameLayer;
import bomberman.view.layers.ScoreGameLayer;
import bomberman.view.layers.SettingsMenuLayer;
import bomberman.view.layers.SoloMenuLayer;
import bomberman.view.layers.MultiplayerCreateLayer;
import bomberman.view.layers.MultiplayerJoinLayer;

public class Game {

	public static Font BOMBERMAN_FONT;
	public static String CURRENT_LAYER;
	
	private static final double SCREEN_WIDTH_RATIO = (double)Toolkit.getDefaultToolkit().getScreenSize().width / 1920;
	private static final double SCREEN_HEIGHT_RATIO = (double)Toolkit.getDefaultToolkit().getScreenSize().height / 1080;
	private static final Cursor blankCursor = Toolkit.getDefaultToolkit().createCustomCursor(new BufferedImage(16, 16, BufferedImage.TYPE_INT_ARGB), new Point(0, 0), "blank cursor");
	private static Cursor defaultCursor = new Cursor(Cursor.DEFAULT_CURSOR);
	
	private GameModel model;
	
	@SuppressWarnings("unused")
	private BindingManager bindingManager;
	
	private JFrame mainFrame;
	private JPanel cards;
	private Map<String, JPanel> layers;
	
	private GameSurfaceView gsw;
	
	public Game() {
		createModel();
		createView();
		placeComponents();
		createController();
	}
	
	public GameModel getModel() {
		return model;
	}

	public void display() {
		mainFrame.pack();
		mainFrame.setVisible(true);
	}
	
	private void createModel() {
		model = new StdGameModel();
	}
	
	private void createView() {
		try {
			BOMBERMAN_FONT = Font.createFont(Font.TRUETYPE_FONT, getClass().getClassLoader().getResourceAsStream("fonts/bauhaus-93.ttf"));
		} catch (Exception e) {
			BOMBERMAN_FONT = new Font("Bauhaus 93", Font.PLAIN, Game.getWidthRelativeToScreen(12));
		}
		
		Image cursorImage = new ImageIcon(getClass().getClassLoader().getResource("images/cursor.png")).getImage();
		defaultCursor = Toolkit.getDefaultToolkit().createCustomCursor(cursorImage, new Point(16,16), "bomberman_cursor");
		
		mainFrame = new JFrame("Bomberman"); {
			mainFrame.setExtendedState(JFrame.MAXIMIZED_BOTH);
			mainFrame.setUndecorated(true);
			mainFrame.setCursor(defaultCursor);
		}

		mainFrame.setCursor(defaultCursor);
		
		gsw = new GameSurfaceView(true); {
			gsw.setGrid(model.getGrid());
			gsw.setOverlay(true);
		}
		cards = new JPanel(new CardLayout()); {
			cards.setOpaque(false);
		}
		
		layers = new HashMap<String, JPanel>();
		layers.put("empty", new EmptyLayer(model));
		layers.put("home", new HomeMenuLayer(model));
		layers.put("solo", new SoloMenuLayer(model));
		layers.put("settings", new SettingsMenuLayer(model));
		layers.put("game", new GameLayer(model));
		layers.put("pause", new PauseGameLayer(model));
		layers.put("score", new ScoreGameLayer(model));
		layers.put("game_ended", new EndGameLayer(model));
		layers.put("multiplayer", new MultiplayerMenuLayer(model));
		layers.put("multiplayer_create", new MultiplayerCreateLayer(model));
		layers.put("multiplayer_join", new MultiplayerJoinLayer(model));
		layers.put("multiplayer_lobby", new MultiplayerLobbyLayer(model));
		layers.put("multiplayer_waiting", new MultiplayerWaitingLayer(model));
		layers.put("network_error", new NetworkErrorLayer(model));
		layers.put("intermission", new IntermissionGameLayer(model));
				
		for (Entry<String, JPanel> entry : layers.entrySet()) {
			cards.add(entry.getValue(), entry.getKey());
		}
	}
	
	private void placeComponents() {
		JPanel p = new JPanel() {
			private static final long serialVersionUID = 1L;

			public boolean isOptimizedDrawingEnabled() {
				return false;
			}
		};
		p.setLayout(new OverlayLayout(p));
		
		p.add(cards);
		p.add(gsw);
			
		mainFrame.add(p);
	}
	
	private void createController() {
		mainFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		mainFrame.setFocusTraversalKeysEnabled(false);
		mainFrame.setFocusable(true);
		mainFrame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				model.exit();
				System.exit(0);
			}
		});
		
		final StandbyDetector sd = new StandbyDetector();
		sd.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				if (model.inGame() || !CURRENT_LAYER.equals("home")) {
					return;
				}
				
				if (sd.isStandBy()) {
					hideCursor();
					gsw.setOverlay(false);
					((CardLayout)cards.getLayout()).show(cards, "empty");
				} else {
					showCursor();
					gsw.setOverlay(true);
					showLayer(CURRENT_LAYER);
				}
			}
		});
		mainFrame.addMouseListener(new MouseListener() {
			public void mouseClicked(MouseEvent e) {
				sd.userDidSomething();
			}
			public void mouseEntered(MouseEvent e) {
				sd.userDidSomething();
			}
			public void mouseExited(MouseEvent e) {
				sd.userDidSomething();
			}
			public void mousePressed(MouseEvent e) {
				sd.userDidSomething();
			}
			public void mouseReleased(MouseEvent e) {
				sd.userDidSomething();
			}
		});
		mainFrame.addMouseMotionListener(new MouseMotionListener() {
			public void mouseDragged(MouseEvent e) {
				sd.userDidSomething();
			}
			public void mouseMoved(MouseEvent e) {
				sd.userDidSomething();
			}
		});
		mainFrame.addKeyListener(new KeyListener() {
			public void keyPressed(KeyEvent e) {
				sd.userDidSomething();
			}
			public void keyReleased(KeyEvent e) {
				sd.userDidSomething();
			}
			public void keyTyped(KeyEvent e) {
				sd.userDidSomething();
			}
		});
		
		mainFrame.addComponentListener(new ComponentAdapter(){
			public void componentShown(ComponentEvent e) {
				model.lauchAIGames();
			}
		});

		
		bindingManager = new BindingManager(gsw, model);
		
		model.addGameListener(new GameAdapter() {
			public void gameStarted() {
				if (model.inGame()) {
					hideCursor();
					showLayer("multiplayer_waiting");
				}
			}
			
			public void gameEnded() {
				showLayer("game_ended");
				gsw.setOverlay(true);
				showCursor();
			}
					
			public void roundStarted() {
				gsw.setGrid(model.getGrid());
				if (model.inGame()) {
					showLayer("game");
					gsw.setOverlay(false);
				}
			}
					
			public void roundEnded() {
				if (model.inGame()) {
					if (model.getRounds() > 1) {
						showLayer("intermission");
						gsw.setOverlay(true);
					}
				}
			}
			
			public void gamePaused() {
				if (model.inGame()) {
					showLayer("pause");
					gsw.setOverlay(true);
					showCursor();
				}
			}
				
			public void gameResumed() {
				if (model.inGame()) {
					showLayer("game");
					gsw.setOverlay(false);
					hideCursor();
				}
			}
			
			public void gameExited() {
				gsw.setOverlay(true);
				gsw.setGrid(model.getGrid());
				showCursor();
				showLayer("home");
				model.lauchAIGames();
			}
			
			public void gameScoreShow() {
				if (model.inGame()) {
					showLayer("score");
					gsw.setOverlay(true);
					showCursor();
				}
			}
			
			public void gameScoreHide() {
				if (model.inGame()) {
					showLayer("game");
					gsw.setOverlay(false);
					hideCursor();
				}
			}

			
			public void networkError(String error) {
				showLayer("network_error");
				gsw.setOverlay(true);
				showCursor();
			}
		});
		
		SoundManager.playBackgroundMusic("chibi_ninja.mp3");
		
		showLayer("home");
	}
	
	private void showLayer(String idLayer) {
		CURRENT_LAYER = idLayer;
		((CardLayout)cards.getLayout()).show(cards, idLayer);
	}
	
	private void hideCursor() {
		cards.setCursor(blankCursor);
	}
	
	private void showCursor() {
		cards.setCursor(defaultCursor);
	}
	
	public static int getWidthRelativeToScreen(int width) {
		return (int)(SCREEN_WIDTH_RATIO * width);
	}
	
	public static int getHeightRelativeToScreen(int height) {
		return (int)(SCREEN_HEIGHT_RATIO * height);
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				new Game().display();
			}
		});
	}
}