package bomberman.model.event;

import javax.swing.SwingUtilities;
import javax.swing.event.EventListenerList;

public class GameSupport {

    // ATTRIBUTS

	private EventListenerList listenersList;
    
    @SuppressWarnings("unused")
	private final Object owner;

    // CONSTRUCTEURS
    
    public GameSupport(Object owner) {
    	super();
        this.owner = owner;
    }
    
    // REQUETES
 
    public synchronized void addGameListener(GameListener listener) {
        if (listener == null) {
            return;
        }
        if (listenersList == null) {
        	listenersList = new EventListenerList();
        }
        listenersList.add(GameListener.class, listener);
    }
    
    public synchronized void removeGameListener(GameListener listener) {
        if (listener == null || listenersList == null) {
            return;
        }
        listenersList.remove(GameListener.class, listener);
    }
    
    // COMMANDES
    
	public void fireGameStarted() {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				for (GameListener listener : listenersList.getListeners(GameListener.class)) {
					listener.gameStarted();
				}
			}
		});
	}
	
	public void fireGameEnded() {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				for (GameListener listener : listenersList.getListeners(GameListener.class)) {
					listener.gameEnded();
				}
			}
		});
	}
	
	public void fireRoundStarted() {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				for (GameListener listener : listenersList.getListeners(GameListener.class)) {
					listener.roundStarted();
				}
			}
		});
	}
	
	public void fireRoundEnded() {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				for (GameListener listener : listenersList.getListeners(GameListener.class)) {
					listener.roundEnded();
				}
			}
		});
	}
	
	public void fireGamePaused() {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				for (GameListener listener : listenersList.getListeners(GameListener.class)) {
					listener.gamePaused();
				}
			}
		});
	}
	
	public void fireGameResumed() {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				for (GameListener listener : listenersList.getListeners(GameListener.class)) {
					listener.gameResumed();
				}
			}
		});
	}
	
	public void fireGameExited() {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				for (GameListener listener : listenersList.getListeners(GameListener.class)) {
					listener.gameExited();
				}
			}
		});
	}
	
	public void fireGameScoreShow() {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				for (GameListener listener : listenersList.getListeners(GameListener.class)) {
					listener.gameScoreShow();
				}
			}
		});
	}
	
	public void fireGameScoreHide() {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				for (GameListener listener : listenersList.getListeners(GameListener.class)) {
					listener.gameScoreHide();
				}
			}
		});
	}
	
	public void firePlayerBonusUpdated() {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				for (GameListener listener : listenersList.getListeners(GameListener.class)) {
					listener.playerBonusUpdated();
				}
			}
		});
	}
	
	public void fireServerCreated() {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				for (GameListener listener : listenersList.getListeners(GameListener.class)) {
					listener.serverCreated();
				}
			}
		});
	}
	
	public void fireServerJoined() {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				for (GameListener listener : listenersList.getListeners(GameListener.class)) {
					listener.serverJoined();
				}
			}
		});
	}
	
	public void fireNetworkError(final String error) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				for (GameListener listener : listenersList.getListeners(GameListener.class)) {
					listener.networkError(error);
				}
			}
		});
	}
}