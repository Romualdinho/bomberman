package bomberman.model.event;

import java.util.EventListener;

public interface GameListener extends EventListener {
	void gameStarted();
	void gameEnded();
	void roundStarted();
	void roundEnded();
	void gamePaused();
	void gameResumed();
	void gameExited();
	void gameScoreShow();
	void gameScoreHide();
	void playerBonusUpdated();
	void serverCreated();
	void serverJoined();
	void networkError(String error);
}