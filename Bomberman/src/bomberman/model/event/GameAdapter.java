package bomberman.model.event;

public class GameAdapter implements GameListener {
	public void gameStarted()  {}
	public void gameEnded()  {}
	public void roundStarted()  {}
	public void roundEnded()  {}
	public void gamePaused()  {}
	public void gameResumed()  {}
	public void gameExited()  {}
	public void gameScoreShow()  {}
	public void gameScoreHide() {}
	public void playerBonusUpdated() {}
	public void serverCreated() {}
	public void serverJoined() {}
	public void networkError(String error) {}
}