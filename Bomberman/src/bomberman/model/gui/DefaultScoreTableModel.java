package bomberman.model.gui;

import java.awt.Image;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.table.AbstractTableModel;

import bomberman.model.entity.player.Player;
import bomberman.model.util.Contract;
import bomberman.view.Game;

public class DefaultScoreTableModel extends AbstractTableModel {

	private static final long serialVersionUID = 1L;
	private static final String[] HEADERS = new String[] {"Joueur", "Score", "Status"};
	private final ImageIcon ALIVE_IMAGE;
	private final ImageIcon DEAD_IMAGE;

	private List<Player> players;
	
	public DefaultScoreTableModel(List<Player> players) {
		if (players.size() == 0) System.out.println("ERROR");
		this.players = players;
		int imageSize = Math.min(100, (int)(100 * (8.0/players.size())));
		ALIVE_IMAGE = new ImageIcon(new ImageIcon(getClass().getClassLoader().getResource("images/alive.png")).getImage().getScaledInstance(Game.getWidthRelativeToScreen(imageSize), Game.getWidthRelativeToScreen(imageSize), Image.SCALE_DEFAULT));
		DEAD_IMAGE = new ImageIcon(new ImageIcon(getClass().getClassLoader().getResource("images/dead.png")).getImage().getScaledInstance(Game.getWidthRelativeToScreen(imageSize), Game.getWidthRelativeToScreen(imageSize), Image.SCALE_DEFAULT));
	}

	@Override
	public int getColumnCount() {
		return 3;
	}

	@Override
	public int getRowCount(){
		return players.size();
	}
	
	@Override
	public String getColumnName(int columnIndex){
		return HEADERS[columnIndex];
	}
	
	@Override
	public Class<?> getColumnClass(int i){
		if (i == 2) {
			return ImageIcon.class;
		} else {
			return Player.class;
		}
	}

	@Override
	public Object getValueAt(int row, int column) {
		Contract.checkCondition(row >= 0 && row < players.size() && column >= 0 && column < getColumnCount());
		
		Player player = players.get(row);
		if (column == 0) {
			return player.getName();
		} else if (column == 1) {
			return player.getScore();
		} else {
			return player.isAlive() ? ALIVE_IMAGE : DEAD_IMAGE;
		}
	}
}