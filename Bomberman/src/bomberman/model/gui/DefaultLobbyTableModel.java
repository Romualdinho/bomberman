package bomberman.model.gui;

import java.util.List;

import javax.swing.table.AbstractTableModel;

import bomberman.model.GameModel;
import bomberman.model.entity.player.Player;
import bomberman.model.util.Contract;

public class DefaultLobbyTableModel extends AbstractTableModel {

	private static final long serialVersionUID = 1L;
	
	private GameModel model;
	 
	public DefaultLobbyTableModel(GameModel model) {
		this.model = model;
	}

	@Override
	public int getColumnCount() {
		return 1;
	}

	@Override
	public int getRowCount(){
		return model.getMaxPlayers();
	}
	
	@Override
	public String getColumnName(int columnIndex) {
		String ipServer = "???";
		if (model.getIPServer() != null) {
			ipServer = model.getIPServer();
		}
		return "Joueurs connect�s (" + ipServer + ")";
	}
	
	@Override
	public Class<?> getColumnClass(int i){
		return Player.class;
	}

	@Override
	public Object getValueAt(int row, int column) {
		Contract.checkCondition(row >= 0 && row < getRowCount() && column >= 0 && column < getColumnCount());
		List<Player> players = model.getPlayers();
		return (row > players.size() - 1) ? "En attente d'un adversaire" : players.get(row).getName();
	}
}