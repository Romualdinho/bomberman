package bomberman.model;

import java.net.SocketException;
import java.util.List;
import java.util.Map;

import bomberman.model.entity.player.Player;
import bomberman.model.event.GameListener;
import bomberman.model.util.Direction;
import bomberman.model.util.grid.Grid;
/**
 * Une interface sp�cifiant une partie du jeu.
 * 
 * @inv <pre>
 *     getAvailableGrids() != null
 *     forall i:[MIN_PLAYERS..MAX_PLAYERS] : 
 *         getAvailableGrids(i) != null
 *         getAvailableGrids(i) == getAvailableGrids().get(i)
 *     getGrid() != null
 *     getPlayers() != null </pre>
 * 
 * @cons <pre>
 * $DESC$
 *     Un constructeur de partie pour quatre joueurs.
 * $POST$
 *     getAvailableGrids() contient toutes les grilles du dossiers maps
 *     forall i:[MIN_PLAYERS..MAX_PLAYERS] : 
 *         getAvailableGrids(i) != null
 *         getAvailableGrids(i) est tri�e selon le nombre de joueurs possible
 *     getPlayers() != null
 *     getPlayers.size() == 0
 *     getGrid().equals(getAvailableGrids(4).get(0)) </pre>
 */
public interface GameModel {
	
	// CONSTANTES
	
	public final static int MIN_PLAYERS = 2;
	public final static int MAX_PLAYERS  = 16;
	public final static int MIN_ROUNDS = 2;
	public final static int MAX_ROUNDS = 20;
	public final static long ROUND_DURATION = 120;
	public final static long INTERMISSION_DURATION = 3;
	
	// REQUETES
	
	/**
	 * Retourne la table associant le nombre de joueurs aux listes des grilles disponibles. 
	 */
	Map<Integer, List<Grid>> getAvailableGrids();
	
	/**
	 * Retourne la liste de grilles dont le nombre de joueurs est <code>nbPlayers</code>.
	 * @pre <pre>
	 *     MIN_PLAYERS <= nbPlayers <= MAX_PLAYERS </pre>
	 */
	List<Grid> getAvailableGrids(int nbPlayers);
	
	/**
	 * Retourne la grille sur laquelle la partie se d�roule.
	 */
	Grid getGrid();
	
	/**
	 * La liste des joueurs sur la grille.
	 */
	List<Player> getPlayers();
	
	/**
	 * Indique si le jeu est en pause ou non.
	 */
	boolean isPaused();
	
	/**
	 * 
	 */
	int getMaxPlayers();
	
	/**
	 * Le nombre de rounds effectu�s de la partie.
	 */
	int getRounds();
	
	/**
	 * Le nombre de rounds de la partie.
	 */
	int getTotalRounds();
	
	/**
	 * 
	 */
	String getIPServer();
	
	// COMMANDES
	/**
	 *
	 */
	void showScore();
	
	/**
	 *
	 */
	void hideScore();
	
	/**
	 * 
	 */
	public boolean inGame();
	
	/**
	 * Met le jeu en pause.
	 * @post <pre>
	 *     isPaused() == true </pre>
	 */
	void pause();
	
	/**
	 * Relance le jeu.
	 * @post <pre>
	 *     isPaused() == false </pre>
	 */
	void resume();
	
	/**
	 * Quitte une partie en cours
	 */
	void exit();
	
	/**
	 * 
	 */
	void roundTimedOut();
	
	/**
	 * D�marre une partie pour <code>nbPlayers</code> en solo dont le nombre 
	 * de rounds est <code>roundsNb</code> et la grille <code>g</code>.
	 * @pre <pre>
	 *     g != null
	 *     MIN_PLAYERS <= nbPlayers <= MAX_PLAYERS
	 *     round > 0 </pre>
	 */
	void startSoloGame(Grid g, int nbPlayers, int roundsNb);
	
	/**
	 * 
	 */
	void createMultiplayerGame(String host, Grid g, int nbPlayers, int rounds) throws SocketException;
	
	/**
	 * 
	 */
	void startMultiplayerGame();
	
	/**
	 * 
	 */
	boolean joinMultiplayerGame(String ip, String username);
	
	/**
	 * 
	 */
	void networkError(String error);
	
	/**
	 * D�place le joueur <code>player</code> dans la direction <code>dir</code>
	 * s'il est vivant et qu'il n'a pas d�j� une destination.
	 * @pre <pre>
	 *     player != null && dir != null </pre>
	 */
	void movePlayer(final Player player, final Direction dir);
	
	/**
	 * 
	 */
	void startNextRound();
	
	/**
	 * Permet au joueur <code>player</code> de poser une bombe.
	 * @pre <pre>
	 *     player != null </pre>
	 */
	void dropBomb(final Player player);
	
	/**
	 * 
	 */
	void addGameListener(GameListener listener);
	
	/**
	 * 
	 */
	void removeGameListener(GameListener listener);
	
	public void lauchAIGames();

}
