package bomberman.model.util;

import java.io.Serializable;

/**
 * Une classe de coordonnées.
 * 
 * @inv <pre>
 *     getX() >= 0
 *     getY() >= 0
 *     getCoord().length == 2
 *     getCoord()[0] == getX() && getCoord()[1] == getY() </pre>
 */
public class Coordinate implements Serializable {

	// ATTRIBUTS
	
	private static final long serialVersionUID = 1L;
	
	private int x;
	private int y;
	
	// CONSTRUCTEURS
	
	/**
	 * @pre <pre>
	 *     x >= 0 && y >= 0 </pre>
	 * @post <pre>
	 *     getX() == x
	 *     getY() == y </pre>
	 */
	public Coordinate(int x, int y) {
		Contract.checkCondition(x >= 0 && y >= 0);
		this.x = x;
		this.y = y;
	}
	
	// REQUETES
	
	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}
	
	public int[] getCoord() {
		return new int[] {x, y};
	}
	
	public boolean equals(Object obj) {
		if (obj != null && obj.getClass() == this.getClass()) {
			Coordinate o = (Coordinate) obj;
			return o.x == this.x && o.y == this.y;
		}
		return false;
	}
	
	public int hashCode() {
		int res = 11;
		res += x * 13;
		res += y * 17;
		return res;
	}
	
	// COMMANDES
	
	/**
	 * @pre <pre>
	 *     x >= 0 </pre>
	 * @post <pre>
	 *     getX() == x </pre>
	 */
	public void setX(int x) {
		Contract.checkCondition(x >= 0);
		this.x = x;
	}
	
	/**
	 * @pre <pre>
	 *     y >= 0 </pre>
	 * @post <pre>
	 *     getY() == y </pre>
	 */
	public void setY(int y) {
		Contract.checkCondition(y >= 0);
		this.y = y;
	}
}
