package bomberman.model.util.grid;

import bomberman.model.entity.Entity;
import bomberman.model.entity.MapEntityEnum;
import bomberman.model.entity.bomb.Bomb;
import bomberman.model.entity.bomb.Explosion;
import bomberman.model.entity.bomb.StdExplosion;
import bomberman.model.entity.bonus.Bonus;
import bomberman.model.entity.bonus.BonusType;
import bomberman.model.entity.player.Player;
import bomberman.model.entity.wall.StdWall;
import bomberman.model.util.Contract;
import bomberman.model.util.Coordinate;
import bomberman.model.util.Direction;

import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.event.EventListenerList;

public class StdGrid implements Grid {

	// CONSTANTES
	
	private static final long serialVersionUID = 1L;
	private static int BONUS_NB = BonusType.values().length;
	// ATTRIBUTS
	
	private Map<Coordinate, List<Entity>> grid;
	private List<Player> players;
	private List<DropBomb> bombs;
	private transient List<Coordinate> spawns;
	private transient File file;
	private EventListenerList listeners; 
	private int maxPlayers;
	private int width;
	private int height;

	// CONSTRUCTEURS
	
	public StdGrid(File f) throws FileNotFoundException {
		Contract.checkCondition(f != null);
		grid = new HashMap<Coordinate, List<Entity>>();
		spawns = new ArrayList<Coordinate>();
		listeners = new EventListenerList();
		file = f;
		Scanner sc = new Scanner(f);
		int y = 0;
		int x = 0;
		while (sc.hasNextLine()) {
			String line = sc.nextLine();
		    x = 0;
			for (char c : line.toCharArray()) { 
				List<Entity> ar = new ArrayList<Entity>();
				switch(MapEntityEnum.getEntityByChar(c)) {
					case PLAYER :
						spawns.add(new Coordinate(x,y));
						maxPlayers++;
						break;
					case DESTRUCTIBLE_WALL :
						if (Math.random() < 0.2) {
							ar.add(createBonus());
						}
						ar.add(new StdWall(true));
						break;
					case INDESTRUCTIBLE_WALL :
						ar.add(new StdWall(false));
						break;
					default :
						break;	
				}
				grid.put(new Coordinate(x, y), ar);
				x++;
			}
			y++;
		}
		sc.close();
		width = x;
		height = y;
	}
	
	public StdGrid(Grid g, List<Player> playersEntities) {
		Contract.checkCondition(g != null);	
		Contract.checkCondition(playersEntities != null);
		grid = new HashMap<Coordinate, List<Entity>>(g.getGrid());
		players = new ArrayList<Player>(playersEntities);
		bombs = new ArrayList<DropBomb>();
		spawns = new ArrayList<Coordinate>(g.getSpawns());
		file = new File(g.getFile().getAbsolutePath());
		listeners = g.getListeners();
		maxPlayers = g.getMaxPlayers();
		width = g.getWidth();
		height = g.getHeight();	
		List<Coordinate> tmpSpawns = new ArrayList<Coordinate>(spawns);
		while (!playersEntities.isEmpty()) {
			Random rand = new Random();
			Coordinate coord = tmpSpawns.get(rand.nextInt(tmpSpawns.size()));
			Player player = playersEntities.get(0);
			grid.get(coord).add(player);
			player.setPosition(coord);
			playersEntities.remove(0);
			tmpSpawns.remove(coord);
		}
	}
	
	// REQUETES
	
	public Map<Coordinate, List<Entity>> getGrid() {
		return grid;
	}
	
	public List<Player> getPlayers() {
		return players;
	}
	
	public List<Coordinate> getSpawns() {
		return spawns;
	}
	
	public File getFile() {
		return file;
	}

	public List<Entity> getEntities(Coordinate c) {
		Contract.checkCondition(c != null);
		return grid.get(c);
	}
	
	public int getMaxPlayers() {
		return maxPlayers;
	}
	
	public int getWidth() {
		return width;
	}
	
	public int getHeight() {
		return height;
	}
	
	public EventListenerList getListeners() {
		return listeners;
	}
	
	// COMMANDES
	
	public void pause() {
		for (DropBomb b: bombs) {
			if (b != null) {
				b.interupts();
			} else {
				System.out.println("PROBLEME");
			}
		}
		
	}
	
	public void resume() {
		for (DropBomb b: bombs) {
			Thread thread = new Thread(new DropBomb(b));
			thread.start();
		}
	}
	
	public void movePlayer(final Player player, final Direction direction) {
		new Thread(new Runnable() {
			public void run() {
				try {
					Coordinate playerPosition = player.getPosition();
					Coordinate targetPosition = null;
					switch (direction) {
					case UP:
						targetPosition = new Coordinate(playerPosition.getX(), playerPosition.getY() - 1);
						break;
					case DOWN:
						targetPosition = new Coordinate(playerPosition.getX(), playerPosition.getY() + 1);
						break;
					case LEFT:
						targetPosition = new Coordinate(playerPosition.getX() - 1, playerPosition.getY());
						break;
					case RIGHT:
						targetPosition = new Coordinate(playerPosition.getX() + 1, playerPosition.getY());
						break;
					}
					player.move(targetPosition);
					grid.get(playerPosition).remove(player);
					grid.get(targetPosition).add(player);
					List<Entity> list = grid.get(targetPosition);
					for (int i = 0; i < list.size(); ++i) {
						Entity entity = list.get(i);
						if (entity instanceof Bonus) {
							Bonus b = (Bonus) entity;
							b.bonusAction(player);
							firePlayerBonusUpdated(player);
							list.remove(i);
							grid.remove(targetPosition);
							grid.put(targetPosition, list);
							return;
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}).start();
	}
	
	public boolean canMovePlayer(Player player, Direction direction) {
		Contract.checkCondition(player != null && direction != null);
		
		Coordinate targetPosition = null;
		if (player != null && player.isAlive() && direction != null ) {
			Coordinate playerPosition = player.getPosition();
			switch (direction) {
			case UP:
				targetPosition = new Coordinate(playerPosition.getX(), playerPosition.getY() - 1);
				break;
			case DOWN:
				targetPosition = new Coordinate(playerPosition.getX(), playerPosition.getY() + 1);
				break;
			case LEFT:
				targetPosition = new Coordinate(playerPosition.getX() - 1, playerPosition.getY());
				break;
			case RIGHT:
				targetPosition = new Coordinate(playerPosition.getX() + 1, playerPosition.getY());
				break;
			}
			List<Entity> entitiesToDestination = grid.get(targetPosition);
			for (Entity entity : entitiesToDestination) {
				if (entity.isCollisible()) {
					return false;
				}
			}
			return true;
		}
		return false;
	}
	
	public boolean canDropBomb(Player player) {
		Contract.checkCondition(player != null);
		return player.isAlive() && player.getBombs().canDropBomb() && !containsBomb(player.getPosition());
	}
	
	public void dropBomb(Player player) {
		Thread thread = new Thread(new DropBomb(player));
		thread.start();
	}
	
	public void dropBomb(Player player, Coordinate coordinate) {
		Thread thread = new Thread(new DropBomb(player, coordinate));
		thread.start();
	}
	
	public void addGridListener(GridListener listener){
		Contract.checkCondition(listener != null);
		listeners.add(GridListener.class, listener); 
	}
	
	public void removeGridListener(GridListener listener){
		Contract.checkCondition(listener != null);
		listeners.remove(GridListener.class, listener);
	}
	
	// OUTILS
	
	/*
	 * Retourne vrai si la grille contient une entit� de type bombe
	 * � la coordonn�e <code>c</code>.
	 */
	private boolean containsBomb(Coordinate c) {
		for (Entity entity : grid.get(c)) {
			if (entity instanceof Bomb) {
				return true;
			}
		}
		return false;
	}
	
	/*
	 * Retourne vrai si la grille contient une entit� de type explosion 
	 * � la coordonn�e <code>c</code>.
	 */
	private boolean containsExplosion(Coordinate c) {
		for (Entity entity : grid.get(c)) {
			if (entity instanceof Explosion) {
				return true;
			}
		}
		return false;
	}
	
	/*
	 * Ajoute une entit� de type explosion dans la grille � la coordonn�e
	 * <code>c</code> si elle n'en contient pas d�j� une, pendant 250ms.
	 */
	private void addExplosion(final Coordinate c)  {
		new Thread(new Runnable() {
			public void run() {
				try {
					if (!containsExplosion(c)) {
						Explosion explosion = new StdExplosion();
						grid.get(c).add(explosion);
						Thread.sleep(250);
						grid.get(c).remove(explosion);
					}
				} catch (Exception e) {
					// RIEN
				}
			}
		}).start();
	}
	
	/*
	 * Retourne vrai si au moins un �l�ment plac� � la coordonn�e <code>c</code>
	 * est destructible ou s'il n'y a rien sur cette coordonn�e.
	 */
	private boolean isDestroyableCoordinate(Coordinate c) {
		for (Entity entity : grid.get(c)) {
			if (entity.isDestroyable() || entity instanceof Bonus) {
				return true;
			}
		}
		if (grid.get(c).isEmpty()) {
			return true;
		}
		return false;
	}
	
	/*
	 * Cr�e un bonus al�atoire parmis ceux de l'�num�ration BonusType.
	 */
	private Bonus createBonus() {
		int random = 1 + (int)(Math.random() * ((BONUS_NB - 1) + 1));
		Class<? extends Bonus> bonusClass = BonusType.values()[random - 1].getBonusClass();
		Bonus bonus = null;
		try {
			bonus = bonusClass.newInstance(); 
		} catch (IllegalAccessException e) {
			// RIEN
		} catch (InstantiationException e) {
			// RIEN
		}
		return bonus;
	}
	
	/*
	 * Explose les �l�ments destructibles de la grille contenus � la coordonn�e 
	 * <code>coord</code>. 
	 * Si un joueur est touch�, il meurt et un point est  ajout� au propri�taire 
	 * de la bombe <code>bomb</code> s'il ne s'agit pas de lui-m�me. 
	 * Si une bombe est touch�e, elle explose. 
	 * Une entit� d'explosion est ensuite ajout�e � cette coordonn�e temporairement.
	 * Si un mur destructible est d�truit, un bonus al�atoire peut �tre ajout�
	 * � la coordonn�e <code>coord</code>.
	 */
	private void explodeCoordinate(Coordinate coord, Bomb bomb) {
		List<Entity> notDestroyed = new ArrayList<Entity>();
		List<Entity> listEntity = new ArrayList<Entity>(grid.get(coord));
		for (int i = 0; i < listEntity.size(); ++i) {
			Entity entity = listEntity.get(i);
			if (entity.isDestroyable()) {
				if (entity instanceof Player) {
					Player player = (Player) entity;
					player.kill();
					player.setPosition(null);
					if (bomb.getPlayer() != player) {
					    bomb.getPlayer().increaseScore();
					}
					firePlayerDied(player);
				}
				if (entity instanceof Bomb) {
					explodeBomb((Bomb) entity);
				}
			} else {
				notDestroyed.add(entity);
			}
		}
		grid.remove(coord);
		grid.put(coord, notDestroyed);
		addExplosion(coord);
	}
	
	/*
	 * Explose la bombe <code>bomb</code> et rajoute une bombe au stock du joueur l'ayant pos�.
	 */
	private void explodeBomb(Bomb bomb) {
		if (bomb.hasExplode()) {
			return;
		}
		bomb.explose();
		Coordinate bombCoord = bomb.getCoord();
		grid.get(bombCoord).remove(bomb); 
		bomb.getPlayer().getBombs().addBomb();
	
		boolean north = true;
		boolean south = true;
		boolean west = true;
		boolean east = true;
		explodeCoordinate(bombCoord, bomb);		
		for (int i = 1; i <= bomb.getRange(); i++) {
			if (north && bombCoord.getY() - i > 0) {
				Coordinate coord = new Coordinate(bombCoord.getX(), bombCoord.getY() - i);
				if (isDestroyableCoordinate(coord)) {
					explodeCoordinate(coord, bomb);
				} else {
					north = false;
				}
			}
			if (south && bombCoord.getY() + i < height - 1) {
				Coordinate coord = new Coordinate(bombCoord.getX(), bombCoord.getY() + i);
				if (isDestroyableCoordinate(coord)) {
					explodeCoordinate(coord, bomb);
				} else {
					south = false;
				}
			}
			if (west && bombCoord.getX() - i > 0) {
				Coordinate coord = new Coordinate(bombCoord.getX() - i, bombCoord.getY());
				if (isDestroyableCoordinate(coord)) {
					explodeCoordinate(coord, bomb);
				} else {
					west = false;
				}
			}
			if (east && bombCoord.getX() + i < width - 1) {
				Coordinate coord = new Coordinate(bombCoord.getX() + i, bombCoord.getY());
				if (isDestroyableCoordinate(coord)) {
					explodeCoordinate(coord, bomb);
				} else {
					east = false;
				}
			}
		}
	}
	
	private void firePlayerDied(Player player) {
		for (GridListener listener : listeners.getListeners(GridListener.class)) {
			listener.playerDied(player);
		}
	}
	
	private void firePlayerBonusUpdated(Player player) {
		for (GridListener listener : listeners.getListeners(GridListener.class)) {
			listener.playerBonusUpdated(player);
		}
	}
	
	private class DropBomb implements Runnable {
		public Player player;
		public Bomb bomb;
		public Boolean interupt;
		public Coordinate playerPosition;
		
		public DropBomb(Player p) {
			player = p;
			interupt = false;
			bomb = null;
			playerPosition = p.getPosition();
		}
		
		public DropBomb(Player p, Coordinate c) {
			player = p;
			interupt = false;
			bomb = null;
			playerPosition = c;
		}
		
		public DropBomb(DropBomb db) {
			player = db.player;
			bomb = db.bomb;
			interupt = false;
			playerPosition = db.playerPosition;
		}
		
		public void run() {
			try {				
				if (bomb == null) {
					if (!player.getBombs().canDropBomb() || containsBomb(playerPosition)) {
						return;
					}
					player.getBombs().dropBomb();
					bomb = player.getBombs().getLastDropBomb();
					grid.get(playerPosition).add(bomb);
					bomb.setPlayer(player);
					bomb.setCoord(playerPosition);
					bomb.drop();
					bombs.add(this);
				} else {
					bomb.drop();
				}
				Thread t = new Thread(new Runnable() {
					public void run() {
						try {
							Thread.sleep(bomb.remainingTime());
						} catch (Exception e) {
							// RIEN
						}
					}
				});
				t.start();
				t.join();
				if (!interupt) {
					explodeBomb(bomb);
					bombs.remove(this);
				}
			} catch (Exception e) {
				
			}
		}
		
		public void interupts() {
			interupt = true;
			bomb.stopExplosion();
		}
	}
}