package bomberman.model.util.grid;

import bomberman.model.entity.Entity;
import bomberman.model.entity.player.Player;
import bomberman.model.util.Coordinate;
import bomberman.model.util.Direction;

import java.util.List;
import java.util.Map;
import java.io.File;
import java.io.Serializable;
import javax.swing.event.EventListenerList;

/**
 * Une classe repr�sentant une grille et les donn�es qui lui sont associ�es.
 * 
 * @inv <pre>
 *     getGrid() != null 
 *     getSpawns() != null 
 *     getFile() != null 
 *     getMaxPlayers() >= 0 
 *     getWidth >= 0
 *     getHeight >= 0 </pre>
 *     
 * @cons <pre>
 * $DESC$
 *     Un constructeur de grille en fonction du fichier <code>f</code>.
 * $ARGS$
 *     File f
 * $PRE$
 *     f != null
 * $POST$
 *     getGrid() != null
 *     getGrid() contient les entit�s du fichier en fonction de leurs coordonn�es.
 *     getSpawns() != null
 *     getSpawns() contient la liste des coordonn�es o� se situeront les joueurs.
 *     getListeners() != null
 *     getFile() == f
 *     getMaxPlayers() == getSpawns().size()
 *     getWidth() est la largeur de la grille.
 *     getHeight() est la hauteur de la grille </pre>
 *       
 * @cons <pre>
 * $DESC$
 * 	   Cr�e une grille en fonction de la grille <code>g</code> et de la liste 
 *     de joueurs <code>playersEntities</code>.
 *     Affecte les joueurs al�atoirement aux diff�rents emplacements possibles.
 * $ARGS$
 *     Grid g, List<Player> playersEntities
 * $PRE$
 *     g != null 
 *     playersEntities != null 
 *     playersEntities.size() <= g.getMaxPlayers()
 * $POST$
 *     getGrid().equals(g.getGrid())
 *     getGrid() contient les entit�s correspondants aux joueurs de getPlayers(). 
 *     getPlayers().equals(playersEntities)
 *     getAlivePlayers().equals(getPlayers())
 *     getSpawns().equals(g.getSpawns())
 *     getFile().equals(g.getFile())
 *     getListeners().equals(g.getListeners())
 *     getMaxPlayers() == g.getMaxPlayers()
 *     getWidth() == g.getWidth()
 *     getHeight() == g.getHeight() </pre>
 */

public interface Grid extends Serializable {

	// REQUETES 
	
	/**
	 * La grille.
	 */
	public Map<Coordinate, List<Entity>> getGrid();

	/**
	 * La liste des joueurs, morts ou vivants.
	 */
	public List<Player> getPlayers();

	/**
	 * La liste des coordonn�es o� sont initialement plac�s les joueurs.
	 */
	public List<Coordinate> getSpawns();

	/**
	 * Le fichier de la carte pr�d�finie correspondant � la grille.
	 */
	public File getFile();

	/**
	 * La liste d'entit�s contenues � la coordonn�e <code>c</code>.
	 * @pre <pre>
	 *     c != null </pre>
	 */
	public List<Entity> getEntities(Coordinate c);

	/**
	 * Le nombre maximal de joueurs pouvant appara�tre sur la grille.
	 */
	public int getMaxPlayers();

	/**
	 * La largeur de la grille.
	 */
	public int getWidth();

	/**
	 * La hauteur de la grille.
	 */
	public int getHeight();

	/**
	 * La liste d'�couteurs de la grille.
	 */
	public EventListenerList getListeners();

	// COMMANDES
	
	/**
	 * Mets la grille en pause.
	 */
	public void pause();
	
	/**
	 * Redémarre la grille.
	 */
	public void resume();
	
	/**
	 * D�place le joueur <code>player</code> dans la direction <code>direction</code>.
	 * @pre <pre>
	 *     player != null 
	 *     player.isAlive() == true
	 *     direction != null </pre>
	 * @post <pre>
	 *     Le joueur est d�plac� si la liste d'entit�s contenue dans la direction 
	 *     donn�e ne contient pas d'�l�ment que le joueur ne peut traverser.
	 *     Si la destination contient un bonus, il sera ajout� au joueur.</pre>
	 */
	public void movePlayer(Player player, Direction direction);
	
	/**
	 * 
	 */
	public boolean canMovePlayer(Player player, Direction direction);

	/**
	 * Pose une bombe du joueur <code>player</code> � la position o� il se trouve
	 * s'il n'y a pas d�j� une bombe � cette position.
	 * @pre <pre>
	 *     player != null 
	 *     player.isAlive() == true </pre>
	 * @post <pre>
	 *     La bombe est pos�e si le joueur poss�de au moins une bombe et qu'il
	 *     n'y a pas de bombe � cette position.
	 *     Elle explosera � la fin de son temps imparti.</pre>
	 */
	public void dropBomb(Player player);
	
	
	/**
	 * 
	 */
	public void dropBomb(Player player, Coordinate coordinate);
	
	/**
	 * 
	 */
	public boolean canDropBomb(Player player);

	/**
	 * @pre <pre>
	 *      listener != null </pre>
	 */
	public void addGridListener(GridListener listener);

	/**
	 * @pre <pre>
	 *      listener != null </pre>
	 */
	public void removeGridListener(GridListener listener);

}