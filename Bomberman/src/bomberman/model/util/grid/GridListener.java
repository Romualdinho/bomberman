package bomberman.model.util.grid;

import java.util.EventListener;

import bomberman.model.entity.player.Player;

public interface GridListener extends EventListener {
	
	// COMMANDES
	
	/**
	 * Notifie qu'un joueur a �t� tu�
	 */
	void playerDied(Player player);
	
	/**
	 * Alerte le mod�le qu'un des joueurs a r�cup�rer un bonus
	 */
	void playerBonusUpdated(Player player);
}
