package bomberman.model.util;

public class Contract {

	// METHODES STATIQUES
	
	public static void checkCondition(Boolean b, String s) {
		if (b == false) {
			throw new AssertionError(s);
		}
	}
	
	public static void checkCondition(Boolean b) {
		if (b == false) {
			throw new AssertionError();
		}
	}
}
