package bomberman.model.util;

public enum Direction {
	UP,
	DOWN,
	LEFT,
	RIGHT;
}
