package bomberman.model.net.event;

import java.util.EventObject;

import bomberman.model.util.Coordinate;
import bomberman.model.util.Direction;
import bomberman.model.util.grid.Grid;

public class NetworkEvent extends EventObject {

	private static final long serialVersionUID = 1L;
	
	private Grid grid;
	private String playerName;
	private Coordinate position;
	private Direction direction;
    
	/* Event lors d'une connection/deconnection */
	public NetworkEvent(Object source, String s) {
		super(source);
		playerName = s;
	}
	
	/* Event lorsque la grid est réinitialisée */
    public NetworkEvent(Object source, Grid g) {
        super(source);
        grid = g;
    }
    
    /* Event lorsqu'un joueur se déplace */
    public NetworkEvent(Object source, String s, Coordinate pos, Direction dir) {
        super(source);
        playerName = s;
        position = pos;
        direction = dir;
    }
    
    /* Event lorsqu'un joueur pose une bombe */
    public NetworkEvent(Object source, String s, Coordinate pos) {
    	super(source);
    	playerName = s;
    	position = pos;
    }
    
    public String getPlayerName() {
    	return playerName;
    }
    
    public Coordinate getPosition() {
    	return position;
    }
    
    public Direction getDirection() {
    	return direction;
    }
    
    public Grid getGrid() {
        return grid;
    }
}