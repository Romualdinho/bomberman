package bomberman.model.net.event;

import java.util.EventListener;

public interface NetworkListener extends EventListener {
	void gameStarted(NetworkEvent e);
	void nextRound(NetworkEvent e);
	void playerMoved(NetworkEvent e);
	void playerBomb(NetworkEvent e);
	void playerConnected(NetworkEvent e);
	void playerDisconnected(NetworkEvent e);
}
