package bomberman.model.net.event;

import javax.swing.event.EventListenerList;

import bomberman.model.util.Coordinate;
import bomberman.model.util.Direction;
import bomberman.model.util.grid.Grid;

public class NetworkSupport {

    // ATTRIBUTS

	private EventListenerList listenersList;
    
	private final Object owner;

    // CONSTRUCTEURS
    
    public NetworkSupport(Object owner) {
    	super();
        this.owner = owner;
    }
    
    // REQUETES
 
    public synchronized void addNetworkListener(NetworkListener listener) {
        if (listener == null) {
            return;
        }
        if (listenersList == null) {
        	listenersList = new EventListenerList();
        }
        listenersList.add(NetworkListener.class, listener);
    }
    
    public synchronized void removeNetworkListener(NetworkListener listener) {
        if (listener == null || listenersList == null) {
            return;
        }
        listenersList.remove(NetworkListener.class, listener);
    }
    
    // COMMANDES
    
    public void fireGameStarted(Grid grid) {
		NetworkEvent e = new NetworkEvent(owner, grid);
		for (NetworkListener listener : listenersList.getListeners(NetworkListener.class)) {
			listener.gameStarted(e);
		}
    }
    
	public void fireNextRound(Grid grid) {
		NetworkEvent e = new NetworkEvent(owner, grid);
		for (NetworkListener listener : listenersList.getListeners(NetworkListener.class)) {
			listener.nextRound(e);
		}
	}
    
	public void firePlayerConnected(String username) {
		NetworkEvent e = new NetworkEvent(owner, username);
		for (NetworkListener listener : listenersList.getListeners(NetworkListener.class)) {
			listener.playerConnected(e);
		}
	}
	
	public void firePlayerDisconnected(String username) {
		NetworkEvent e = new NetworkEvent(owner, username);
		for (NetworkListener listener : listenersList.getListeners(NetworkListener.class)) {
			listener.playerDisconnected(e);
		}
	}
	
	public void firePlayerMoved(String username, Coordinate position, Direction direction) {
		NetworkEvent e = new NetworkEvent(owner, username, position, direction);
		for (NetworkListener listener : listenersList.getListeners(NetworkListener.class)) {
			listener.playerMoved(e);
		}
	}
	
	public void firePlayerBomb(String username, Coordinate position) {
		NetworkEvent e = new NetworkEvent(owner, username, position);
		for (NetworkListener listener : listenersList.getListeners(NetworkListener.class)) {
			listener.playerBomb(e);
		}
	}
}