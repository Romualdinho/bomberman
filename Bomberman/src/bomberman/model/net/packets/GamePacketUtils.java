package bomberman.model.net.packets;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;


public class GamePacketUtils {

	public static GamePacket deserializeGamePacket(byte[] data) throws IOException, ClassNotFoundException {
		ByteArrayInputStream bais = new ByteArrayInputStream(data);
		ObjectInputStream ois = new ObjectInputStream(bais);
		GamePacket gamePacket = (GamePacket)ois.readObject();
		bais.close();
		
		return gamePacket;
	}
	
	public static byte[] serializeGamePacket(GamePacket packet) throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ObjectOutputStream oos = new ObjectOutputStream(baos);
		oos.writeObject(packet);
		oos.flush();
		byte[] buffer = baos.toByteArray();
		baos.close();
		
		return buffer;
	}
}
