package bomberman.model.net.packets;

import java.util.List;

public class LobbyPacket extends AbstractGamePacket {

	private static final long serialVersionUID = 1L;
	
	private List<String> players;
	private int maxPlayers;
	private int rounds;
	
	public LobbyPacket(String name, List<String> players, int maxPlayers, int rounds) {
		super(name);
		this.players = players;
		this.maxPlayers = maxPlayers;
		this.rounds = rounds;
	}
	
	public List<String> getConnectedPlayers() {
		return players;
	}
	
	public int getMaxPlayers() {
		return maxPlayers;
	}
	
	public int getRounds() {
		return rounds;
	}

}
