package bomberman.model.net.packets;

public class AbstractGamePacket implements GamePacket {

	private static final long serialVersionUID = 1L;
	
	protected String playerName;
	
	public AbstractGamePacket(String playerName) {
		this.playerName = playerName;
	}

	public String getPlayerName() {
		return playerName;
	}
}
