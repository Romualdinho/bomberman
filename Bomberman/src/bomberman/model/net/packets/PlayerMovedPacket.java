package bomberman.model.net.packets;

import bomberman.model.util.Coordinate;
import bomberman.model.util.Direction;

public class PlayerMovedPacket extends AbstractGamePacket {

	private static final long serialVersionUID = 1L;

	private Coordinate position;
	private Direction direction;
	
	public PlayerMovedPacket(String playerName, Coordinate position, Direction direction) {
		super(playerName);
		this.position = position;
		this.direction = direction;
	}
	
	public Coordinate getPosition() {
		return position;
	}
	
	public Direction getDirection() {
		return direction;
	}
}