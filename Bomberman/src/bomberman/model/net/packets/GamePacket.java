package bomberman.model.net.packets;

import java.io.Serializable;

public interface GamePacket extends Serializable {
	String getPlayerName();
}
