package bomberman.model.net.packets;

import bomberman.model.util.Coordinate;

public class PlayerBombPacket extends AbstractGamePacket {

	private static final long serialVersionUID = 1L;

	private Coordinate coordinate;
	
	public PlayerBombPacket(String playerName, Coordinate coordinate) {
		super(playerName);
		this.coordinate = coordinate;
	}
	
	public Coordinate getCoordinate() {
		return coordinate;
	}

}
