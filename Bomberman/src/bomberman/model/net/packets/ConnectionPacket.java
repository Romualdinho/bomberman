package bomberman.model.net.packets;

public class ConnectionPacket extends AbstractGamePacket {

	private static final long serialVersionUID = 1L;

	public ConnectionPacket(String playerName) {
		super(playerName);
	}

}
