package bomberman.model.net.packets;

public class DisconnectionPacket extends AbstractGamePacket {

	private static final long serialVersionUID = 1L;

	public DisconnectionPacket(String playerName) {
		super(playerName);
	}
}
