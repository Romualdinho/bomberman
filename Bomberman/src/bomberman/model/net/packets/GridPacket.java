package bomberman.model.net.packets;

import bomberman.model.util.grid.Grid;

public class GridPacket extends AbstractGamePacket {

	private static final long serialVersionUID = 1L;

	private Grid grid;
	
	public GridPacket(String playerName, Grid grid) {
		super(playerName);
		this.grid = grid;
	}
	
	public Grid getGrid() {
		return grid;
	}
}
