package bomberman.model.net;

import java.io.File;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import bomberman.model.entity.player.Player;
import bomberman.model.entity.player.StdPlayer;
import bomberman.model.net.packets.ConnectionPacket;
import bomberman.model.net.packets.DisconnectionPacket;
import bomberman.model.net.packets.GamePacket;
import bomberman.model.net.packets.GamePacketUtils;
import bomberman.model.net.packets.GridPacket;
import bomberman.model.net.packets.LobbyPacket;
import bomberman.model.util.grid.Grid;
import bomberman.model.util.grid.StdGrid;

public class GameServer {
	
	public static final int PORT = 9877;
	public static final int BUFFER_SIZE = 15210;
	public static final String SERVER_DISCONNECTED_LOBBY = "SERVER_DISCONNECTED_LOBBY";
	public static final String SERVER_DISCONNECTED_GAME = "SERVER_DISCONNECTED_GAME";

	private Thread[] serverThreads;
	private DatagramSocket serverSocket;
	private InetAddress address;
	private List<Client> clients;
	
	private boolean stopped;
	private boolean gameStarted;
	
	private List<String> playersNames;
	
	private File gridFile;
	private int maxPlayers;
	private int rounds;
	
	public GameServer(File file, int nbPlayers, int r) throws SocketException {
		address = getCurrentIP();
		serverSocket = new DatagramSocket(PORT, address);
		
		gridFile = file;
		maxPlayers = nbPlayers;
		rounds = r;
		
		playersNames = new ArrayList<String>();
		
		clients = new ArrayList<Client>();
		
		serverThreads = new Thread[nbPlayers * 2];
		serverThreads[0] = new Thread(new LobbyLoop());
		serverThreads[0].start();
	}
	
	public void startGame() throws IOException {
		gameStarted = true;
		serverSocket.close();
		serverThreads[0].interrupt();
		
		try {
			serverThreads[0].join();
		} catch (InterruptedException e) {
			// rien
		}

		List<Player> players = new ArrayList<Player>();
		for (String playerName : playersNames) {
			players.add(new StdPlayer(playerName));
		}
		
		Grid grid = new StdGrid(new StdGrid(gridFile), new ArrayList<Player>(players));
		byte[] data = GamePacketUtils.serializeGamePacket(new GridPacket("SERVER", grid));
		
		serverSocket = new DatagramSocket(PORT, address);
		
		for (int i = 0; i < clients.size(); i++) {
			DatagramPacket sendPacket = new DatagramPacket(data, data.length, clients.get(i).getInetAdress(), clients.get(i).getPort());
			serverSocket.send(sendPacket);
		}
		
		for (int i = 0; i < serverThreads.length; i++) {
			serverThreads[i] = new Thread(new GameLoop());
			serverThreads[i].start();
		}
	}
	
	public void nextRound() throws IOException {
		rounds--;
		
		List<Player> players = new ArrayList<Player>();
		for (String playerName : playersNames) {
			players.add(new StdPlayer(playerName));
		}
		
		Grid grid = new StdGrid(new StdGrid(gridFile), new ArrayList<Player>(players));
		byte[] data = GamePacketUtils.serializeGamePacket(new GridPacket("SERVER_HOST", grid));
		
		for (Client client : clients) {
			DatagramPacket sendPacket = new DatagramPacket(data, data.length, client.getInetAdress(), client.getPort());
			serverSocket.send(sendPacket);
		}
	}
	
	public void stop() {
		stopped = true;
		
		try {
			byte[] data = GamePacketUtils.serializeGamePacket(new DisconnectionPacket(gameStarted ? SERVER_DISCONNECTED_GAME : SERVER_DISCONNECTED_LOBBY));
			for (Client client : clients) {
				DatagramPacket sendPacket = new DatagramPacket(data, data.length, client.getInetAdress(), client.getPort());
				serverSocket.send(sendPacket);
			}
		} catch (IOException e1) {
			// rien
		}
		
		serverSocket.close();
		
		for (Thread thread : serverThreads) {
			if (thread != null) {
				thread.interrupt();
				
				try {
					thread.join();
				} catch (InterruptedException e) {
					// rien
				}
			}
		}
	}
	
	public InetAddress getAddress() {
		return address;
	}
	
	public List<String> getPlayersNames() {
		return playersNames;
	}
	
	public int getMaxPlayers() {
		return maxPlayers;
	}
	
	public boolean isStarted() {
		return gameStarted;
	}
	
	private InetAddress getCurrentIP() {
        try {
            Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
            while (networkInterfaces.hasMoreElements()) {
                NetworkInterface ni = (NetworkInterface) networkInterfaces.nextElement();
                Enumeration<InetAddress> nias = ni.getInetAddresses();
                while(nias.hasMoreElements()) {
                    InetAddress ia = (InetAddress) nias.nextElement();
                    if (!ia.isLinkLocalAddress() 
                     && !ia.isLoopbackAddress()
                     && ia instanceof Inet4Address) {
                        return ia;
                    }
                }
            }
        } catch (SocketException e) {
        	// rien
        }
        return null;
    }
	
	private class LobbyLoop implements Runnable {
		public void run() {
			while (!stopped && !gameStarted && playersNames.size() < maxPlayers) {
				try {
					byte[] data = new byte[GameServer.BUFFER_SIZE];
					DatagramPacket receivePacket = new DatagramPacket(data, data.length);
					serverSocket.receive(receivePacket);
					
					Client client = new Client(receivePacket.getAddress(), receivePacket.getPort());
					
					try {
						GamePacket gamePacket = GamePacketUtils.deserializeGamePacket(receivePacket.getData());
						String playerName = gamePacket.getPlayerName();
						
						if (gamePacket instanceof ConnectionPacket) {
							if (!clients.contains(client)) {	
								clients.add(client);
								
								String validName = playerName;
								for (int i = 1; playersNames.contains(validName); i++) {
									validName = playerName + i;
								}
								
								playersNames.add(validName);
								GamePacket lobbyPacket = new LobbyPacket(validName, playersNames, maxPlayers, rounds);
								data = GamePacketUtils.serializeGamePacket(lobbyPacket);
								serverSocket.send(new DatagramPacket(data, data.length, client.getInetAdress(), client.getPort()));
							
								GamePacket connectionPacket = new ConnectionPacket(validName);
								data = GamePacketUtils.serializeGamePacket(connectionPacket);
								for (Client c : clients) {
									if (!c.equals(client)) {
										serverSocket.send(new DatagramPacket(data, data.length, c.getInetAdress(), c.getPort()));
									}
								}
							}
							
						} else if (gamePacket instanceof DisconnectionPacket) {
							if (clients.contains(client)) {
								clients.remove(client);
								playersNames.remove(playerName);
							}
							
							GamePacket connectionPacket = new DisconnectionPacket(playerName);
							data = GamePacketUtils.serializeGamePacket(connectionPacket);
							for (Client c : clients) {
								if (!c.equals(client)) {
									serverSocket.send(new DatagramPacket(data, data.length, c.getInetAdress(), c.getPort()));
								}
							}
						}
					} catch (ClassNotFoundException e) {
						// rien
					}
				} catch (IOException ie) {
					// rien
				}
			}
		}
	}
	
	private class GameLoop implements Runnable {
		public void run() {
			while(!stopped && rounds > 0) {
				try {
					final byte[] data = new byte[GameServer.BUFFER_SIZE];
					DatagramPacket receivePacket = new DatagramPacket(data, data.length);
					serverSocket.receive(receivePacket);
					
					Client client = new Client(receivePacket.getAddress(), receivePacket.getPort());
					
					try {
						GamePacket gamePacket = GamePacketUtils.deserializeGamePacket(receivePacket.getData());
						String playerName = gamePacket.getPlayerName();
						
						if (gamePacket instanceof DisconnectionPacket) {
							if (clients.contains(client)) {
								clients.remove(client);
								playersNames.remove(playerName);
							}
						}
					} catch (ClassNotFoundException e) {
						// rien
					}

					for (final Client c : clients) {
						if (!c.equals(client)) {
							new Thread(new Runnable() {
								public void run() {
									try {
										serverSocket.send(new DatagramPacket(data, data.length, c.getInetAdress(), c.getPort()));
									} catch (IOException e) {
										// rien
									}
								}
							}).start();
						}
					}
					
				} catch (IOException ie) {
					// rien
				}
			}
		}
	}
}
