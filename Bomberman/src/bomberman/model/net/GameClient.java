package bomberman.model.net;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import bomberman.model.net.event.NetworkListener;
import bomberman.model.net.event.NetworkSupport;
import bomberman.model.net.packets.ConnectionPacket;
import bomberman.model.net.packets.DisconnectionPacket;
import bomberman.model.net.packets.GamePacket;
import bomberman.model.net.packets.GamePacketUtils;
import bomberman.model.net.packets.GridPacket;
import bomberman.model.net.packets.LobbyPacket;
import bomberman.model.net.packets.PlayerBombPacket;
import bomberman.model.net.packets.PlayerMovedPacket;
import bomberman.model.util.Contract;

public class GameClient {

	private static final int TIMEOUT = 3000;
	
	private DatagramSocket clientSocket;
	private InetAddress clientAddress;
	private int port;
	private InetAddress serverAddress;
	
	private Thread[] clientThreads;
	private NetworkSupport ns;
	private String username;
	
	private List<String> playersNames;
	private int maxPlayers;
	private int rounds;
	
	private boolean stopped;
	private boolean gameStarted;
	
	public GameClient(String username) throws SocketException {
		this.username = username;
		
		DatagramSocket ds = new DatagramSocket();
		port = ds.getLocalPort();
		ds.close();
		
		clientAddress = getCurrentIP();
		clientSocket = new DatagramSocket(port, clientAddress);
		
		ns = new NetworkSupport(this);
		playersNames = new ArrayList<String>();
	}
	
	public void connect(String ip) throws UnknownHostException, IOException, ClassNotFoundException {
		serverAddress = InetAddress.getByName(ip);
		
		byte[] data = GamePacketUtils.serializeGamePacket(new ConnectionPacket(username));
		
		clientSocket.send(new DatagramPacket(data, data.length, serverAddress, GameServer.PORT));
		
		clientSocket.setSoTimeout(TIMEOUT);
		
		//On attends une r�ponse du serveur pour confirmer
		data = new byte[GameServer.BUFFER_SIZE];
		DatagramPacket receivePacket = new DatagramPacket(data, data.length);
		clientSocket.receive(receivePacket);
		
		LobbyPacket lobbyPacket = (LobbyPacket)GamePacketUtils.deserializeGamePacket(receivePacket.getData());
		username = lobbyPacket.getPlayerName();
		playersNames = lobbyPacket.getConnectedPlayers();
		maxPlayers = lobbyPacket.getMaxPlayers();
		rounds = lobbyPacket.getRounds();
		
		clientSocket.setSoTimeout(0);
		
		clientThreads = new Thread[maxPlayers * 2];
		clientThreads[0] = new Thread(new LobbyLoop());
		clientThreads[0].start();
	}
	
	public void play() throws SocketException {
		gameStarted = true;
		clientSocket.close();
		clientThreads[0].interrupt();
		
		try {
			clientThreads[0].join();
		} catch (InterruptedException e) {
			// rien
		}
		
		clientSocket = new DatagramSocket(port, clientAddress);
		
		for (int i = 0; i < clientThreads.length; i++) {
			clientThreads[i] = new Thread(new GameLoop());
			clientThreads[i].start();
		}
		
	}
	
	public void notifyServer(final GamePacket packet) {
		new Thread(new Runnable() {
			public void run() {
				try {
					byte[] data = GamePacketUtils.serializeGamePacket(packet);
					clientSocket.send(new DatagramPacket(data, data.length, serverAddress, GameServer.PORT));
				} catch (IOException ie) {
					// rien
				}
			}
		}).start();
	}
	
	public void disconnect() throws UnknownHostException, IOException {
		byte[] data = GamePacketUtils.serializeGamePacket(new DisconnectionPacket(username));
		clientSocket.send(new DatagramPacket(data, data.length, serverAddress, GameServer.PORT));
		
		stopped = true;
		clientSocket.close();		
		
		for (Thread thread : clientThreads) {
			if (thread != null) {
				thread.interrupt();
				try {
					thread.join();
				} catch (InterruptedException e) {
					// rien
				}
			}
		}
	}
	
	public String getUsername() {
		return username;
	}
	
	public String getIPServer() {
		return serverAddress.getHostAddress();
	}
	
	public int getMaxPlayers() {
		return maxPlayers;
	}
	
	public int getRounds() {
		return rounds;
	}
	
	public List<String> getPlayersNames() {
		return playersNames;
	}
	
	public void addNetworkListener(NetworkListener listener) {
		Contract.checkCondition(listener != null);
		ns.addNetworkListener(listener);
	}
	
	public void removeNetworkListener(NetworkListener listener) {
		Contract.checkCondition(listener != null);
		ns.removeNetworkListener(listener);
	}
	
	private InetAddress getCurrentIP() {
        try {
            Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
            while (networkInterfaces.hasMoreElements()) {
                NetworkInterface ni = (NetworkInterface) networkInterfaces.nextElement();
                Enumeration<InetAddress> nias = ni.getInetAddresses();
                while(nias.hasMoreElements()) {
                    InetAddress ia = (InetAddress) nias.nextElement();
                    if (!ia.isLinkLocalAddress() 
                     && !ia.isLoopbackAddress()
                     && ia instanceof Inet4Address) {
                        return ia;
                    }
                }
            }
        } catch (SocketException e) {
        	// rien
        }
        return null;
    }
	
	private class LobbyLoop implements Runnable {
		public void run() {
			while (!stopped && !gameStarted) {
				try {
					byte[] data = new byte[GameServer.BUFFER_SIZE];
					
					DatagramPacket receivePacket = new DatagramPacket(data, data.length);
					clientSocket.receive(receivePacket);
					GamePacket gamePacket = GamePacketUtils.deserializeGamePacket(receivePacket.getData());
					
					String playerName = gamePacket.getPlayerName();
					
					if (gamePacket instanceof ConnectionPacket) {
						playersNames.add(playerName);
						ns.firePlayerConnected(playerName);
					} else if (gamePacket instanceof DisconnectionPacket) {
						playersNames.remove(playerName);
						ns.firePlayerDisconnected(playerName);
					} else if (gamePacket instanceof GridPacket) {
						GridPacket gridPacket = (GridPacket)gamePacket;
						ns.fireGameStarted(gridPacket.getGrid());
					}
				} catch (IOException e) {
					// rien
				} catch (ClassNotFoundException e) {
					// rien
				}
			}
		}
	}
	
	private class GameLoop implements Runnable {
		public void run() {
			while (!stopped) {
				try {
					byte[] data = new byte[GameServer.BUFFER_SIZE];
					
					DatagramPacket receivePacket = new DatagramPacket(data, data.length);
					clientSocket.receive(receivePacket);
					
					GamePacket gamePacket = GamePacketUtils.deserializeGamePacket(receivePacket.getData());
					String playerName = gamePacket.getPlayerName();
					if (gamePacket instanceof DisconnectionPacket) {
						playersNames.remove(playerName);
						ns.firePlayerDisconnected(playerName);
					} else if (gamePacket instanceof GridPacket) {
						GridPacket gridPacket = (GridPacket)gamePacket;
						ns.fireNextRound(gridPacket.getGrid());
					} else if (gamePacket instanceof PlayerMovedPacket) {
						PlayerMovedPacket playerMovedPacket = (PlayerMovedPacket)gamePacket;
						ns.firePlayerMoved(playerName, playerMovedPacket.getPosition(), playerMovedPacket.getDirection());
					} else if (gamePacket instanceof PlayerBombPacket) {
						PlayerBombPacket playerBombPacket = (PlayerBombPacket)gamePacket;
						ns.firePlayerBomb(playerName, playerBombPacket.getCoordinate());
					}
				} catch (IOException e) {
					// rien
				} catch (ClassNotFoundException e) {
					// rien
				}
			}
		}
	}
}