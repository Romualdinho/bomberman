package bomberman.model;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.util.Enumeration;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

public class ResourcesInstaller {

	public final static String GAME_PATH = System.getProperty("user.home") + "/bomberman/";
	public static String TEXTURES_PATH = GAME_PATH + "/textures/default/";
	
	public ResourcesInstaller() {
		//Cr�ation des dossiers du jeu
		System.setProperty("net.java.games.input.librarypath", GAME_PATH + "bin/");
		new File(GAME_PATH + "/bin").mkdirs();
		new File(GAME_PATH + "/maps").mkdirs();
		new File(GAME_PATH + "/textures/default").mkdirs();
		new File(GAME_PATH + "/cfg").mkdirs();
	}
		
	public void exportResourcesToGameFolder(String sourceFolder, String destFolder) throws IOException, URISyntaxException {
		File jarFile = new File(getClass().getProtectionDomain().getCodeSource().getLocation().toURI().getPath());
		//Si le jeu est lanc� depuis un JAR
		if (jarFile.isFile()) {
			exportFromJar(jarFile, sourceFolder, destFolder);
		} else {
			exportFromIDE(sourceFolder, destFolder);
		}
	}
	
	private void exportFromJar(File jarFile, String sourceFolder, String destFolder) throws IOException {
		JarFile jar = new JarFile(jarFile);
		Enumeration<JarEntry> entries = jar.entries();
		while(entries.hasMoreElements()) {
			String name = entries.nextElement().getName();
			if (name.startsWith(sourceFolder + "/")) {
				String fileName = name.substring(name.lastIndexOf("/"), name.length());
				
				//Si il s'agit bien d'un nom de fichier et non d'un repertoire
				if (fileName.length() > 1) {
					InputStream in = getClass().getClassLoader().getResourceAsStream(name);
					File dest = new File(GAME_PATH + destFolder + fileName);
					
					//Si le fichier n'existe pas, on le copie dans le dossier
					if (!dest.isFile()) {
						Files.copy(in, dest.toPath());
					}
				}
			}
		}
		jar.close();
	}
	
	private void exportFromIDE(String sourceFolder, String destFolder) throws IOException {
		File folderToExport = new File(getClass().getClassLoader().getResource(sourceFolder).getFile());
		for (File file : folderToExport.listFiles()) {
			File dest = new File(GAME_PATH + destFolder + "/" + file.getName());
			//Si le fichier n'existe pas, on le copie dans le dossier
			if (!dest.isFile()) {
				Files.copy(file.toPath(), dest.toPath());
			}
	    }
	}
}