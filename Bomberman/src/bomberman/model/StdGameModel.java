package bomberman.model;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.SocketException;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import java.util.LinkedList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import bomberman.model.entity.player.AI;
import bomberman.model.entity.player.Player;
import bomberman.model.entity.player.StdPlayer;
import bomberman.model.event.GameListener;
import bomberman.model.event.GameSupport;
import bomberman.model.net.GameClient;
import bomberman.model.net.GameServer;
import bomberman.model.net.event.NetworkEvent;
import bomberman.model.net.event.NetworkListener;
import bomberman.model.net.packets.PlayerBombPacket;
import bomberman.model.net.packets.PlayerMovedPacket;
import bomberman.model.util.Contract;
import bomberman.model.util.Direction;
import bomberman.model.util.grid.StdGrid;
import bomberman.model.util.grid.Grid;
import bomberman.model.util.grid.GridListener;
import bomberman.view.SoundManager;

public class StdGameModel implements GameModel {
	
	private GameSupport gs;
	private Map<Integer, List<Grid>> availableGrids;
	private List<Player> players;
	private boolean pause;
	private boolean inGame;
	private boolean iaGames;
	private int totalRounds;
	private int rounds;
	private Grid grid;
	private GridListener gridListener;
	
	private GameClient gameClient;
	private GameServer gameServer;
	private NetworkListener networkListener;
	private int maxPlayers;
	
	// CONSTRUCTEURS

	public StdGameModel() {
		initGameFolder();
		initAvailableGrids();
		
		gs = new GameSupport(this);
		players = new LinkedList<Player>();
		gridListener = new GridListener() {
			public void playerDied(Player player) {
				if (player == players.get(0)) {
					SoundManager.playSoundEffect("dead.wav");
				} else {
					if (inGame()) {
						SoundManager.playSoundEffect("player_killed.wav");
					}
				}
				
				int alivePlayers = 0;
				for (Player p : players) {
					if (p.isAlive()) {
						alivePlayers++;
					}
				}
				
				if (alivePlayers > 1) {
					return;
				}
				
				for (Player p : players) {
					if (p.isAlive()) {
						p.increaseScore();
					}
					p.getBombs().resetStockBomb();
				}
				
				pause = true;
				
				grid.pause();
				
				gs.fireRoundEnded();
				
				// Si il ne reste plus de rounds et qu'il ne s'agissait pas d'une partie de bots
				if (inGame && rounds == 1) {
					inGame = false;
					gs.fireGameEnded();
				}
			}
			public void playerBonusUpdated(Player player) {
				if (player == players.get(0)) {
					gs.firePlayerBonusUpdated();
				}
			}
		};
		
		networkListener = new NetworkListener() {
			public void gameStarted(NetworkEvent e) {
				pause = false;
				
				grid.removeGridListener(gridListener);
				grid = e.getGrid();
				grid.addGridListener(gridListener);
				
				List<Player> gridPlayers = grid.getPlayers();
				int index = getPlayerByName(gridPlayers, gameClient.getUsername());
				Player gridPlayer = gridPlayers.get(index);
				gridPlayers.remove(index);
				gridPlayers.add(0, gridPlayer);
				
				players.clear();
				for (Player player : grid.getPlayers()) {
					players.add(player);
				}
				
				inGame = true;
				
				try {
					gameClient.play();
				} catch (SocketException e1) {
					// rien
				}
				
				gs.fireGameStarted();
				gs.fireRoundStarted();
			}
			
			public void nextRound(NetworkEvent e) {
				rounds--;
				
				grid.removeGridListener(gridListener);
				grid = e.getGrid();
				grid.addGridListener(gridListener);
				
				List<Player> gridPlayers = grid.getPlayers();
				for (Player gridPlayer : gridPlayers) {
					gridPlayer.setScore(players.get(getPlayerByName(players, gridPlayer.getName())).getScore());
					gridPlayer.getBombs().resetStockBomb();
					gridPlayer.revive();
				}
				
				int index = getPlayerByName(gridPlayers, gameClient.getUsername());
				Player gridPlayer = gridPlayers.get(index);
				gridPlayers.remove(index);
				gridPlayers.add(0, gridPlayer);
				
				players.clear();
				for (Player player : gridPlayers) {
					players.add(player);
				}

				pause = false;
				
				gs.fireRoundStarted();
			}
			
			public void playerConnected(NetworkEvent e) {
				SoundManager.playSoundEffect("connected.wav");
				players.add(new StdPlayer(e.getPlayerName()));
			}

			@Override
			public void playerDisconnected(NetworkEvent e) {
				SoundManager.playSoundEffect("disconnected.wav");
				
				// Si il s'agit d'un client et que le serveur a ferm� le lobby
				String playerName = e.getPlayerName();
				
				if (players.size() > 0 ) {
					if (playerName.equals(GameServer.SERVER_DISCONNECTED_LOBBY)) {
						gs.fireNetworkError("Fermeture du lobby par l'h�bergeur");
						shutdownClient();
						return;
					} else if (playerName.equals(GameServer.SERVER_DISCONNECTED_GAME) && gameClient != null) {
						gs.fireNetworkError("Fermeture du serveur par l'h�bergeur");
						shutdownClient();
						return;
					}
				}
				
				int index = getPlayerByName(players, playerName);
				if (index != -1) {
					players.remove(index);
				}
				
				List<Player> gridPlayers = grid.getPlayers();
				index = getPlayerByName(gridPlayers, playerName);
				if (index != -1) {
					gridPlayers.remove(index);
				}
			}

			public void playerMoved(NetworkEvent e) {
				int index = getPlayerByName(players, e.getPlayerName());
				if (index != -1) {
					Player player = players.get(index);
					player.setPosition(e.getPosition());
					grid.movePlayer(player, e.getDirection());
				}
			}

			public void playerBomb(NetworkEvent e) {
				int index = getPlayerByName(players, e.getPlayerName());
				if (index != -1) {
					Player player = players.get(index);
					grid.dropBomb(player, e.getPosition());
				}
			}
		};
		
		// Grille al�atoire
		List<Grid> grids = availableGrids.get(4);
		grid = grids.get((int)(Math.random() * grids.size()));
	}

	// REQUETES

	public Map<Integer, List<Grid>> getAvailableGrids() {
		return availableGrids;
	}

	public List<Grid> getAvailableGrids(int nbPlayers) {
		Contract.checkCondition(MIN_PLAYERS <= nbPlayers && nbPlayers <= MAX_PLAYERS);
		return availableGrids.get(nbPlayers);
	}

	public Grid getGrid() {
		return grid;
	}
	
	public List<Player> getPlayers() {
		return players;
	}

	public boolean isPaused() {
		return pause;
	}

	public int getRounds() {
		return rounds;
	}
	
	public int getTotalRounds() {
		return totalRounds;
	}
	
	public int getMaxPlayers() {
		return maxPlayers;
	}
	
	public boolean inGame() {
		return inGame;
	}
	
	private int getPlayerByName(List<Player> players, String name) {
		if (players == null) {
			return -1;
		}
		
		for (int i = 0; i < players.size(); i++) {
			if (players.get(i).getName().equals(name)) {
				return i;
			}
		}
		return -1;
	}
	
	public String getIPServer () {
		if (gameClient != null) {
			return gameClient.getIPServer();
		}
		return null;
	}

	// COMMANDES

	public void pause() {
		if (!inGame) {
			return;
		}
		
		if (gameClient == null) {
			grid.pause();
			pause = true;
		}
		
		gs.fireGamePaused();
	}
	
	public void resume() {
		if (!inGame) {
			return;
		}
		
		if (gameClient == null) {
			grid.resume();
			pause = false;
		}
		
		gs.fireGameResumed();
	}

	public void exit() {
		shutdownServer();
		shutdownClient();
		
		pause = true;
		inGame = false;
		
		cleanGame();
		
		gs.fireGameExited();
	}

	public void showScore() {
		if (!inGame || pause) {
			return;
		}
		
		gs.fireGameScoreShow();
	}
	
	public void hideScore() {
		if (!inGame || pause) {
			return;
		}
		
		gs.fireGameScoreHide();
	}
	
	public void roundTimedOut() {
		pause = true;
		
		if (!inGame) {
			cleanGame();
			lauchAIGames();
			return;
		}
		
		for (Player player : players) {
			if (player.isAlive()) {
				player.increaseScore();
			}
			player.getBombs().resetStockBomb();
		}
		
		gs.fireRoundEnded();
		
		// Si il ne reste plus de rounds et qu'il ne s'agissait pas d'une partie de bots
		if (rounds == 1) {
			inGame = false;
			gs.fireGameEnded();
		}
	}
	
	public void startSoloGame(Grid g, int nbPlayers, int rounds) {
		Contract.checkCondition(g != null);
		Contract.checkCondition(MIN_PLAYERS <= nbPlayers && nbPlayers <= MAX_PLAYERS);
		Contract.checkCondition(rounds > 0);
		
		
		cleanGame();
		
		pause = false;
		totalRounds = rounds;
		this.rounds = rounds;
		maxPlayers = nbPlayers;
		
		players.clear();
		players.add(new StdPlayer("Joueur"));
		
		for (int i = 1; i < nbPlayers; i++) {
			 players.add(new AI("Ordinateur " + i, this));
		}

		try {
			grid = new StdGrid(new StdGrid(g.getFile()), new LinkedList<Player>(players));
			grid.addGridListener(gridListener);
			
			for (Player player : players) {
				if (player instanceof AI) {
					((AI) player).start();
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		inGame = true;
		gs.fireGameStarted();
		gs.fireRoundStarted();
		iaGames = false;
	}
	
	public void createMultiplayerGame(String host, Grid g, int nbPlayers, int rounds) throws SocketException {
		Contract.checkCondition(g != null);
		Contract.checkCondition(MIN_PLAYERS <= nbPlayers && nbPlayers <= MAX_PLAYERS);
		Contract.checkCondition(rounds > 0);
		
		if (gameServer != null) {
			gameServer.stop();
		}
		gameServer = new GameServer(g.getFile(), nbPlayers, rounds);
		gs.fireServerCreated();
		iaGames = false;
		joinMultiplayerGame(gameServer.getAddress().getHostAddress(), host);
	}
	
	public void startMultiplayerGame() {
		if (gameServer == null) {
			return;
		}
		
		if (gameServer.isStarted()) {
			return;
		}
		
		try {
			gameServer.startGame();
		} catch (IOException ie) {
			ie.printStackTrace();
		}
		iaGames = false;
	}

	public boolean joinMultiplayerGame(String ip, String username) {
		try {
			if (gameClient != null) {
				gameClient.disconnect();
				gameClient.removeNetworkListener(networkListener);
			}
			iaGames = false;
			gameClient = new GameClient(username);
			gameClient.connect(ip);
			gameClient.addNetworkListener(networkListener);
			
			cleanGame();
			
			List<String> playersNames = gameClient.getPlayersNames();
			for (String playerName : playersNames) {
				players.add(new StdPlayer(playerName));
			}
			
			maxPlayers = gameClient.getMaxPlayers();
			totalRounds = gameClient.getRounds();
			rounds = totalRounds;
			
			gs.fireServerJoined();
		} catch (Exception e) {
			if (gameClient != null) {
				try {
					gameClient.disconnect();
				} catch (Exception e1) {
					//rien
				}
				gameClient = null;
			}
			return false;
		}
		return true;
	}
	
	public void networkError(String error) {
		pause = true;
		inGame = false;
		gs.fireNetworkError(error);
	}
	
	private void shutdownClient() {
		if (gameClient == null) {
			return;
		}
		
		try {
			gameClient.disconnect();
		} catch (UnknownHostException e) {
			// rien
		} catch (IOException e) {
			// rien
		}
		gameClient = null;
		players.clear();
	}
	
	private void shutdownServer() {
		if (gameServer == null) {
			return;
		}
		
		gameServer.stop();
		gameServer = null;
		
		players.clear();
		pause = true;
		inGame = false;
	}

	public void startNextRound() {
		Contract.checkCondition(rounds > 0);
		
		if (gameServer == null && gameClient != null) {
			return;
		}
		
		if (gameServer != null) {
			try {
				gameServer.nextRound();
			} catch (IOException e) {
				// rien
			}
			return;
		}
		
		pause = false;
		
		rounds--;
		for (Player p : players) {
			p.revive();
		}

		grid.removeGridListener(gridListener);

		try {
			grid = new StdGrid(new StdGrid(grid.getFile()), new LinkedList<Player>(players));
			grid.addGridListener(gridListener);

			for (Player player : players) {
				if (player instanceof AI) {
					((AI) player).start();
				}
			}
		} catch (FileNotFoundException e) {
			// rien
		}
		gs.fireRoundStarted();
	}
	
	public void lauchAIGames() {
		cleanGame();
		maxPlayers = grid.getMaxPlayers();
		totalRounds = 1;
		rounds = totalRounds;
		iaGames = true;
					
		players.clear();
		try {
			Thread.sleep(25);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		for (int i = 1; i <= maxPlayers; i++) {
			players.add(new AI("Ordinateur " + i, StdGameModel.this));
		}

		try {
			grid = new StdGrid(new StdGrid(grid.getFile()), new LinkedList<Player>(players));
			grid.addGridListener(new GridListener() {
				public void playerDied(Player player) {
					int alivePlayers = 0;
					for (Player p : players) {
						if (p.isAlive()) {
							alivePlayers++;
						}
					}
					
					if (alivePlayers > 1) {
						return;
					}
					
					pause = true;
					if (iaGames) {
						lauchAIGames();
					}
				}

				public void playerBonusUpdated(Player player) {
					// rien
				}
			});
			
			for (Player player : players) {
				if (player instanceof AI) {
					((AI) player).start();
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		pause = false;
		inGame = false;
		gs.fireRoundStarted();
	}
	
	public void movePlayer(final Player player, final Direction dir) {
		Contract.checkCondition(player != null && dir != null);
		
		if (!pause && player.isAlive() && player.getDestination() == null && grid.canMovePlayer(player, dir)) {
			if (gameClient != null) {
				gameClient.notifyServer(new PlayerMovedPacket(player.getName(), player.getPosition(), dir));
			}
			grid.movePlayer(player, dir);
		}
	}

	public void dropBomb(final Player player) {
		Contract.checkCondition(player != null);
		
		if (!pause && player.isAlive() && grid.canDropBomb(player)) {
			if (gameClient != null) {
				gameClient.notifyServer(new PlayerBombPacket(player.getName(), player.getPosition()));
			}
			grid.dropBomb(player);
		}
	}

	public void addGameListener(GameListener listener) {
		Contract.checkCondition(listener != null);
		gs.addGameListener(listener);
	}

	public void removeGameListener(GameListener listener) {
		Contract.checkCondition(listener != null);
		gs.removeGameListener(listener);
	}
	
	private void cleanGame() {
		for (Player player : players) {
			if (player instanceof AI) {
				player.kill();
			}
		}
		grid.removeGridListener(gridListener);
		players.clear();
		maxPlayers = 0;
		rounds = 0;
		totalRounds = 0;
		
		List<Grid> grids = availableGrids.get(4);
		grid = grids.get((int)(Math.random() * grids.size()));
	}

	
	// OUTILS

	private void initGameFolder() {
		ResourcesInstaller ri = new ResourcesInstaller();
		try {
			ri.exportResourcesToGameFolder("cfg", "cfg");
			ri.exportResourcesToGameFolder("lib", "bin");
			ri.exportResourcesToGameFolder("images/textures", "textures/default");
			ri.exportResourcesToGameFolder("maps", "maps");
			Settings.init();
			TexturesManager.init();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
	}

	private void initAvailableGrids() {
		try {
			availableGrids = new HashMap<Integer, List<Grid>>();
			for (int i = MIN_PLAYERS; i <= MAX_PLAYERS; i++) {
				availableGrids.put(i, new LinkedList<Grid>());
			}
			File folder = new File(ResourcesInstaller.GAME_PATH + "maps");
			for (File file : folder.listFiles()) {
				Grid g = new StdGrid(file);
				for (int i = MIN_PLAYERS; i <= g.getMaxPlayers(); i++) {
					availableGrids.get(i).add(g);
				}
			}
			for (Entry<Integer, List<Grid>> entry : availableGrids.entrySet()) {
				Collections.sort(entry.getValue(), new Comparator<Grid>() {
					public int compare(Grid g1, Grid g2) {
						if (g1.getMaxPlayers() > g2.getMaxPlayers()) {
							return 1;
						} else if (g1.getMaxPlayers() < g2.getMaxPlayers()) {
							return -1;
						}
						return 0;
					}
				});
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}