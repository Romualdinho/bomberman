package bomberman.model;

import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;

import bomberman.model.util.Contract;

public class TexturesManager {

	private static boolean initialized;
	private static Map<String,Image> images;
	
	public static void init() {
		images = new HashMap<String,Image>();
		
		File imagesFolder = new File(ResourcesInstaller.GAME_PATH + "textures/" + Settings.getStringValue("textures-pack"));
		for (File file : imagesFolder.listFiles()) {
			try {
				images.put(file.getName(), ImageIO.read(file));
			} catch (IOException e) {
				e.printStackTrace();
			}
	    }
		
		initialized = true;
	}
	
	public static Image getResource(String name) {
		Contract.checkCondition(initialized);
		
		return images.get(name);
	}
}