package bomberman.model.entity.wall;

import bomberman.model.entity.StdEntity;

public class StdWall extends StdEntity implements Wall {
	
	// CONSTRUCTEURS 

	private static final long serialVersionUID = 1L;

	public StdWall(Boolean isDestructible) {
		super(getImageName(isDestructible), true, isDestructible);	
	}
	
	// OUTILS
	
	/**
	 * @post <pre>
	 *     isDestructible == true => DESTRUCTIBLE_WALL_IMAGE
	 *     isDestructible == false => INDESTRUCTIBLE_WALL_IMAGE </pre>
	 */
	private static String getImageName(Boolean isDestructible) {
		if (isDestructible == true) {
			return DESTRUCTIBLE_WALL_IMAGE;
		}
		return INDESTRUCTIBLE_WALL_IMAGE;
	}
}
