package bomberman.model.entity.wall;

import bomberman.model.entity.Entity;

/**
 * Une interface spécifiant la notion de mur.
 *     
 * @cons <pre>
 * $DESC$
 *     Un constructeur de mur destructible si <code>isDestructible</code> vaut 
 *     true et non destructible si <code>isDestructible</code> vaut false.
 * $ARGS$
 *     Boolean isDestructible
 * $POST$
 *     isCollisible() == true
 *     isDestroyable() == isDestructible
 *     isDestructible == true => getImage().equals(DESTRUCTIBLE_WALL_IMAGE)
 *     isDestructible == false => getImage().equals(INDESTRUCTIBLE_WALL_IMAGE) </pre>
 *
 */
public interface Wall extends Entity {
	
	// CONSTANTES 
	
	final String DESTRUCTIBLE_WALL_IMAGE = "wooden_box.png";
	final String INDESTRUCTIBLE_WALL_IMAGE = "wall.png";
}
