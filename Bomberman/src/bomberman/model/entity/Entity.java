package bomberman.model.entity;

/**
 * Une interface qui sp�cifie la notion d'entit�.
 * 
 * @cons <pre>
 * $DESC$
 *     Un constructeur d'entit�.
 * $ARGS$
 *     ImageIcon i, boolean c, boolean d
 * $PRE$
 *     i != null
 * $POST$
 *     getImage().equals(i)
 *     isCollisible() == c
 *     isDestroyable() == d </pre>
 *
 */

public interface Entity {

	// REQUETES
	
	/**
	 * Retourne vrai si cette entit� est collisable.
	 */
	boolean isCollisible();
	
	/**
	 * Retourne vrai si cette entit� est destructible.
	 */
	boolean isDestroyable();
	
	/**
	 * Retourne le chemin de l'image associ�e � cette entit�.
	 */
	String pathImage();
}
