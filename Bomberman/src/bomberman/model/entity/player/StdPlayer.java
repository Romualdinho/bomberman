package bomberman.model.entity.player;

import bomberman.model.entity.StdEntity;
import bomberman.model.entity.bomb.StdStockBomb;
import bomberman.model.entity.bomb.StockBomb;
import bomberman.model.util.Contract;
import bomberman.model.util.Coordinate;

public class StdPlayer extends StdEntity implements Player {

	// ATTRIBUTS
	
	private static final long serialVersionUID = 1L;
	
	private String name;
	private boolean alive;
	private StockBomb bombs;
	private int speed;
	private int score;
	private Coordinate position;
	private Coordinate temporaryPosition;
	private double movementProgression;
	private transient Thread movementThread;
	private Coordinate destination;
	
	// CONSTRUCTEURS
	
	public StdPlayer(String n) {
		super(PLAYER_IMAGE, false, true);
		Contract.checkCondition(n != null && NAME_RECOGNIZER.matcher(n).matches());
		name = n;
		alive = true;
		bombs = new StdStockBomb();
		speed = DEFAULT_SPEED;
		score = 0;
		position = null;
		movementProgression = 0;
		destination = null;
		temporaryPosition= null;
	}
	
	// REQUETES
	
	public Coordinate getTemporaryPosition() {
		return temporaryPosition;
	}
	
	public String getName() {
		return name;
	}
	
	public boolean isAlive() {
		return alive;
	}
	
	public StockBomb getBombs() {
		return bombs;
	}
	
	public int getSpeed(){
		return speed;
	}
	
	public int getScore() {
		return score;
	}
	
	public Coordinate getPosition() {
		return position;
	}
	
	public double getMovementProgression() {
		return movementProgression;
	}
	
	public Coordinate getDestination() {
		return destination;
	}
	
	public boolean equals(Player p) {
		return p.getName().equals(getName());
	}
	// COMMANDES
	
	public void kill() {
		alive = false;
	}
	
	public void revive() {
		alive = true;
	}
	
	public void setPosition(Coordinate c) {
		if (movementThread != null && movementThread.isAlive()) {
			movementProgression = 1;
			try {
				movementThread.join();
			} catch (InterruptedException e) {
				// rien
			}
		}
		position = c;
		temporaryPosition = c;
	}
	
	public void setName(String n) {
		name = n;
	}
	
	public void setScore(int s) {
		score = s;
	}
	
	public void move(Coordinate c) {
		movementProgression = 0;
		temporaryPosition = position;
		destination = c;
		movementThread = new Thread(new MovementAnimation());
		movementThread.start();
		try {
			movementThread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		position = c;
		finishMove();
	}
	
	public void increaseScore() {
		score += 1;
	}
	
	public void increaseBombsRange() {
		bombs.increaseRange();
	}
	
	public void decreaseBombsTime() {
		bombs.decreaseTime();
	}
	
	public void increaseBombsNb() {
		bombs.addBomb();
	}
	
	private void finishMove() {
		new Thread(new Runnable() {
			public void run() {			
				movementThread = new Thread(new MovementAnimation2());
				movementThread.start();
				try {
					movementThread.join();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				destination = null;
				temporaryPosition = position;
			}
		}).start();
	}
	
	private class MovementAnimation implements Runnable {
		public void run() {
			while (movementProgression < 0.5) {
				try {
					Thread.sleep(5);
					movementProgression += 0.025;
				} catch (InterruptedException e) {
					// RIEN
				}		
			}
		}
	}
	
	private class MovementAnimation2 implements Runnable {
		public void run() {
			while (movementProgression < 1) {
				try {
					Thread.sleep(5);
					movementProgression += 0.025;
				} catch (InterruptedException e) {
					// RIEN
				}		
			}
		}
	}
}