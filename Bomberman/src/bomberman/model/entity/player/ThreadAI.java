package bomberman.model.entity.player;
 
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
 
import bomberman.model.GameModel;
import bomberman.model.entity.Entity;
import bomberman.model.entity.bomb.Bomb;
import bomberman.model.entity.bonus.Bonus;
import bomberman.model.util.Coordinate;
import bomberman.model.util.Direction;
 
public class ThreadAI extends Thread implements Runnable {
 
    // ATTRIBUTS
 
    private AI ai;
    private GameModel model;
 
    // CONSTRUCTEURS
 
    public ThreadAI(AI ai) {
        this.ai = ai;
        this.model = ai.getModel();
    }
 
    // RUN
 
    /* run()
     * (non-Javadoc)
     * @see java.lang.Thread#run()
     */
    public void run() {
    	while (ai.isAlive()) {
    		try {
    			Thread.sleep(60);
 
    			// Pose de la bombe
    			if (poseBomb()) {
    				model.dropBomb(ai);
    			}
 
    			// D�placement de l'IA
    			if (ai.isAlive() && (ai.getBombs().canDropBomb() || !isSafeCoord(ai.getPosition()))) {
    				List<Direction> tmpList = getWay(ai.getPosition());
    				if (tmpList != null) {
    					for (Direction d : tmpList) {
    						if (d != null && (ai.getBombs().canDropBomb() || !isSafeCoord(ai.getPosition()))) {
    							model.movePlayer(ai, d);
    						}
    						Thread.sleep(5);
    					}
    				}
    			}
    		} catch (InterruptedException e) {
    			// RIEN
    		}
    	}
    }
 
    // REQUETES
 
    /* getWay()
     * Retourne une liste de directions � prendre pour que l'IA �vite de mourir
     */
    private List<Direction> getWay(Coordinate coord) {
        List<Direction> diroin = new ArrayList<Direction>();
 
        Direction[] dir = getAleaDir();
        Coordinate c = null;
       
        if (dir == null) {
            return null;
        }
       
        for (Direction d : dir) {
            if (ai.isAlive()) {
                c = new Coordinate(coord.getX(), coord.getY());
 
                switch (d) {
                case UP:
                    c.setY(c.getY() - 1);
                    break;
                case DOWN:
                    c.setY(c.getY() + 1);
                    break;
                case LEFT:
                    c.setX(c.getX() - 1);
                    break;
                case RIGHT:
                    c.setX(c.getX() + 1);
                    break;
                default:
                    break;
                }
               
                // Une direction safe existe
                if (c.getX() >= 0 && c.getX() <= model.getGrid().getWidth() && c.getY() >= 0 && c.getY() <= model.getGrid().getHeight()) {
                    if ((model.getGrid().getEntities(c).isEmpty() || !model.getGrid().getEntities(c).isEmpty() && (model.getGrid().getEntities(c).get(0) instanceof Bonus || model.getGrid().getEntities(c).get(0) instanceof Player)) && isSafeCoord(c)) {
                        diroin.add(d);
                    }
                }
            }
        }
       
        // On trouve un autre chemin si la premi�re direction n'amm�ne pas sur une case safe
        if (c.getX() >= 0 && c.getX() <= model.getGrid().getWidth() && c.getY() >= 0 && c.getY() <= model.getGrid().getHeight()) {
            if ((model.getGrid().getEntities(c).isEmpty() || !model.getGrid().getEntities(c).isEmpty() && (model.getGrid().getEntities(c).get(0) instanceof Bonus || model.getGrid().getEntities(c).get(0) instanceof Player))) {
                diroin.addAll(getWay(c));
            }
        }
       
        return diroin;
    }
 
    // COMMANDES
 
    /* isSafeCoord()
     * D�termine si la coordonn�e pass�e en param�tre est safe ou pas
     */
    private synchronized boolean isSafeCoord(Coordinate c) {
        if (c == null) return false;
       
        for (int i = 0; i < model.getGrid().getWidth(); i++) {
        	Coordinate d = new Coordinate(i, c.getY());
            for (Entity e : model.getGrid().getEntities(d)) {
                if (e instanceof Bomb) {
                    int x = d.getX() - c.getX();
                    if (x >= ((Bomb) e).getRange() * -1
                            && x <= ((Bomb) e).getRange()) {
                        return false;
                    }
                }
            }
        }
 
        for (int i = 0; i < model.getGrid().getHeight(); i++) {
        	Coordinate d = new Coordinate(c.getX(), i);
            for (Entity e : new LinkedList<Entity>(model.getGrid().getEntities(d))) {
                if (e != null && e instanceof Bomb) {
                    int y = d.getY() - c.getY();
                    if (y >= ((Bomb) e).getRange() * -1
                            && y <= ((Bomb) e).getRange()) {
                        return false;
                    }
                }
            }
        }
 
        return true;
    }
 
    /* poseBomb()
     * Permet � l'IA de poser une bombe si un �l�ment destructible se trouve � proximit�
     */
    private boolean poseBomb() {
        Coordinate[] next = new Coordinate[4];
 
        if (ai.isAlive()) {
            next[0] = new Coordinate(ai.getPosition().getX(), ai.getPosition()
                    .getY() - 1);
            next[1] = new Coordinate(ai.getPosition().getX(), ai.getPosition()
                    .getY() + 1);
            next[2] = new Coordinate(ai.getPosition().getX() - 1, ai
                    .getPosition().getY());
            next[3] = new Coordinate(ai.getPosition().getX() + 1, ai
                    .getPosition().getY());
 
            for (Coordinate c : next) {
                if (model.getGrid().getGrid().containsKey(c)) {
                    if (!model.getGrid().getEntities(c).isEmpty()
                            && model.getGrid().getEntities(c).get(0) instanceof Bomb) {
                        return false;
                    }
                }
            }
 
            for (Coordinate c : next) {
                if (model.getGrid().getGrid().containsKey(c)) {
                	if (!model.getGrid().getEntities(c).isEmpty() && model.getGrid().getEntities(c).get(0).isDestroyable()) return true;
                }
            }
        }
       
        return false;
    }
 
    /* getAleaDir()
     * Permet de retourne un tableau des 4 directions al�atoirement, et selon l'attirance des IAs
     */
    private Direction[] getAleaDir() {
        Direction[] generate = new Direction[4];
 
        Direction axeX = null;
        Direction axeY = null;
 
        axeX = magnetPlayer("x");
        axeY = magnetPlayer("y");
 
        if (axeX == null || axeY == null)
            return null;
 
        int rand = (int) (Math.random() * 4);
 
        switch (rand) {
        case 0:
            generate[0] = axeX;
            generate[1] = axeY;
            generate[2] = (axeX.equals(Direction.LEFT)) ? Direction.RIGHT
                    : Direction.LEFT;
            generate[3] = (axeY.equals(Direction.UP)) ? Direction.DOWN
                    : Direction.UP;
            break;
        case 1:
            generate[1] = axeX;
            generate[0] = axeY;
            generate[2] = (axeX.equals(Direction.LEFT)) ? Direction.RIGHT
                    : Direction.LEFT;
            generate[3] = (axeY.equals(Direction.UP)) ? Direction.DOWN
                    : Direction.UP;
            break;
        case 2:
            generate[0] = axeX;
            generate[1] = axeY;
            generate[3] = (axeX.equals(Direction.LEFT)) ? Direction.RIGHT
                    : Direction.LEFT;
            generate[2] = (axeY.equals(Direction.UP)) ? Direction.DOWN
                    : Direction.UP;
            break;
        case 3:
            generate[1] = axeX;
            generate[0] = axeY;
            generate[3] = (axeX.equals(Direction.LEFT)) ? Direction.RIGHT
                    : Direction.LEFT;
            generate[2] = (axeY.equals(Direction.UP)) ? Direction.DOWN
                    : Direction.UP;
            break;
        default:
            break;
        }
        return generate;
    }
 
    /* magnetPlayer()
     * Permet aux joueurs de se rapprocher dans la carte, selon l'axe x et y
     */
    private Direction magnetPlayer(String axe) {
        if (ai.isAlive()) {
            for (int i = 0; i < model.getGrid().getWidth(); i++) {
                for (int j = 0; j < model.getGrid().getHeight(); j++) {
                    if (model.getGrid().getEntities(new Coordinate(i, j)) != null) {
                        for (Entity e : model.getGrid().getEntities(
                                new Coordinate(i, j))) {
                            if (e instanceof Player) {
                                if (axe.equals("x")) {
                                    if (i < ai.getPosition().getX()) {
                                        return Direction.LEFT;
                                    } else {
                                        return Direction.RIGHT;
                                    }
                                } else if (axe.equals("y")) {
                                    if (j < ai.getPosition().getY()) {
                                        return Direction.UP;
                                    } else {
                                        return Direction.DOWN;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return null;
    }
}