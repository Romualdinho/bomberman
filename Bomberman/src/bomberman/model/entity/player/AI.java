package bomberman.model.entity.player;

import bomberman.model.StdGameModel;

public class AI extends StdPlayer implements Player {
	
	// ATTRIBUTS
	
	private static final long serialVersionUID = 1L;
	
	private StdGameModel model;
	
	// CONSTRUCTEURS
	
	public AI (String s, StdGameModel m) {
		super(s);

		model = m;
	}
	
	// REQUETES
	
	public StdGameModel getModel() {
		return model;
	}
	
	public final void start() {
		Thread t = new ThreadAI(this);
		t.start();
	}
}
