package bomberman.model.entity.player;

import java.rmi.RemoteException;
import java.util.regex.Pattern;

import bomberman.model.entity.Entity;
import bomberman.model.entity.bomb.StockBomb;
import bomberman.model.util.Coordinate;

/**
 * Une interface qui sp�cifie la notion de joueur.
 * 
 * @inv <pre>
 *     getName() != null
 *     getBombs() != null 
 *     getSpeed() > 0
 *     getScore() >= 0 
 *     getMovementProgression() >= 0 </pre>
 *     
 * @cons <pre>
 * $DESC$
 *     Un constructeur de joueur.
 * $ARGS$
 *     String name
 * $PRE$
 *     n != null && NAME_RECOGNIZER.matcher(n).matches()
 * $POST$
 *     getImage().equals(PLAYER_IMAGE)
 *     isCollisible() == true
 *     isDestroyable() == true 
 *     getName().equals(name)
 *     isAlive() == true
 *     getBombs() != null
 *     getSpeed() == DEFAULT_SPEED
 *     getScore() == 0
 *     getPosition() == null
 *     getMovementProgression() == 0
 *     getDestination() == null </pre>
 */
public interface Player extends Entity {

    // CONSTANTES
    
	// Le pattern qui permet de tester la validit� d'un nom :
	//   {
	//     des blancs (0, 1 ou plusieurs)
	//     une cha�ne constitu�e d'au moins une lettre
	//     {
	//       un tiret
	//       une cha�ne constitu�e d'au moins une lettre ou un chiffre
	//     } au plus une fois
	//     des blancs (0, 1 ou plusieurs)
	//   } au moins une fois
	Pattern NAME_RECOGNIZER = Pattern.compile(
		"(\\s*[a-zA-Z0-9]+([-][a-zA-Z0-9]+)?\\s*)+"
	);
	
	final String PLAYER_IMAGE = "player.png";
	
	final int DEFAULT_SPEED = 10;
	
	// REQUETES
	
	/**
	 * Le nom du joueur.
	 * @throws RemoteException 
	 */
	String getName();
	
	/**
	 * Indique si le joueur est mort ou vivant.
	 */
	boolean isAlive();
	
	/**
	 * La liste des bombes du joueur.
	 */
	StockBomb getBombs();
	
	/**
	 * La vitesse du joueur.
	 */
	int getSpeed();
	
	/**
	 * Le score du joueur.
	 */
	int getScore();
	
	/**
	 * La position du joueur.
	 */
	Coordinate getPosition();
	
	/**
	 * La position temporaire
	 */
	Coordinate getTemporaryPosition();
	
	/**
	 * La progression du d�placement.
	 */
	double getMovementProgression();
	
	/**
	 * La coordonn�e vers laquelle le joueur se d�place.
	 */
	Coordinate getDestination();
	
	// COMMANDES

	/**
	 * @post <pre>
	 *     alive = false </pre>
	 */
	void kill();
	
	/**
	 * @post <pre>
	 *     alive = true </pre>
	 */
	void revive();
	
	/**
	 * @post <pre>
	 *     getPosition() == c </pre>
	 */
	void setPosition(Coordinate c);
	
	/**
	 * @post <pre>
	 *     getName() == n </pre>
	 */
	void setName(String n);
	
	/**
	 * @post <pre>
	 *     getScore() == s </pre>
	 */
	void setScore(int s);
	
	/**
	 * @post <pre>
	 *     getDestination() = null
	 *     getPosition() == c </pre>
	 */
	void move(Coordinate c);
	
	/**
	 * @post <pre>
	 *     score == old score + 1 </pre>
	 */
	void increaseScore() ;
	
	/**
	 * Augmente la port�e des bombes du stock.
	 * @post <pre>
	 *     forall i:[0..getBombs().getStockNb() - 1] : 
	 *         getBombs().toArray()[i].getRange() 
	 *             == old getBombs().toArray()[i].getRange() + 1 </pre> 
	 */
	void increaseBombsRange();
	
	/**
	 * Diminue le temps d'explosion des bombes du stock.
	 * @post <pre>
	 *     forall i:[0..getBombs().getStockNb() - 1] : 
	 *         getBombs().toArray()[i].getTime() > 1 =>
	 *        	   getBombs().toArray()[i].getTime() 
	 *            	   == old getBombs().toArray()[i].getTime() - 1 </pre> 
	 */
	void decreaseBombsTime();
	
	/**
	 * Ajoute une bombe au stock.
	 * @post <pre>
	 *     getBombs().getStockNb() == old getBombs().getStockNb() + 1 </pre> 
	 */
	void increaseBombsNb();
}