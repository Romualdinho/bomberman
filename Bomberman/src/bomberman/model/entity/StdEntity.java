package bomberman.model.entity;

import java.io.Serializable;

public class StdEntity implements Entity, Serializable {

	// ATTRIBUTS 
	
	private static final long serialVersionUID = 1L;
	
	protected boolean collisible;
	protected boolean destroyable;
	protected String image;
	
	// CONSTRUCTEUR
	
	public StdEntity(String i, boolean c, boolean d) {
		if (i == null) {
			throw new AssertionError("Erreur : image");
		}
		image = i;
		collisible = c;
		destroyable = d;
	}
	
	// REQUETES
	
	public boolean isCollisible() {
		return collisible;
	}
	
	public boolean isDestroyable() {
		return destroyable;
	}
	
	public String pathImage() {
		return image;
	}
	
	public boolean equals(Object o) {
		if (o == null || o.getClass() != getClass()) {
			return false;
		}
		Entity e = (Entity) o;
		return image.equals(e.pathImage()) && collisible == e.isCollisible() && destroyable == e.isDestroyable();
	}
}