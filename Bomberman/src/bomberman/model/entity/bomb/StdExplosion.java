package bomberman.model.entity.bomb;

import bomberman.model.entity.StdEntity;

public class StdExplosion extends StdEntity implements Explosion {
	
	// CONSTRUCTEURS

	private static final long serialVersionUID = 1L;

	public StdExplosion() {
		super(EXPLOSION_IMAGE, false, false);	
	}
}
