package bomberman.model.entity.bomb;

import bomberman.model.entity.Entity;

/**
 * Une interface spécifiant une explosion.
 *     
 * @cons <pre>
 * $DESC$
 *     Un constructeur d'explosion.
 * $POST$
 *     isCollisible() == false
 *     isDestroyable() == false
 *     getImage().equals(EXPLOSION_IMAGE) </pre>
 *
 */

public interface Explosion extends Entity {
	
	// CONSTANTES
	
	final String EXPLOSION_IMAGE = "explosion.png";
}
