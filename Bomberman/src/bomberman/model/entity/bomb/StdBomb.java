package bomberman.model.entity.bomb;


import bomberman.model.entity.StdEntity;
import bomberman.model.entity.player.Player;
import bomberman.model.util.Contract;
import bomberman.model.util.Coordinate;

public class StdBomb extends StdEntity implements Bomb {


	private static final long serialVersionUID = 1L;
	
	// ATTRIBUTS

	private int time;
	private int range;
	private Coordinate coord;
	private boolean hasExplode;
	private Player player;
	private double timeInstallation;
	private long remainingTime;
	
	
	// CONSTRUCTEURS
	
	public StdBomb() {
		super(BOMB_IMAGE, true, true);
		time = TIME;
		remainingTime = TIME * 1000;
		range = RANGE;
		coord = null;
		hasExplode = false;
	}
	
	public StdBomb(int t, int r, Coordinate c) {
		super(BOMB_IMAGE, true, true);
		Contract.checkCondition(t > 0 && r > 0);
		time = t;
		remainingTime = t * 1000;
		range = r;
		coord = c;
		hasExplode = false;
	}
	
	public StdBomb(Bomb b) {
		super(BOMB_IMAGE, true, true);
		Contract.checkCondition(b != null);
		time = b.getTime();
		remainingTime = b.getTime() * 1000;
		range = b.getRange();
		coord = b.getCoord();
		hasExplode = false;
	}

	// REQUETES
	
	public long remainingTime() {
		return remainingTime;
	}
	
	public int getTime() {
		return time;
	}
	
	public Player getPlayer() {
		return player;
	}
	
	public int getRange() {
		return range;
	}
	
	public Coordinate getCoord() {
		return coord;
	}
	
	public boolean hasExplode() {
		return hasExplode;
	}
	
	// COMMANDES
	
	public void modifyTime(int t) {
		Contract.checkCondition(t > 0);
		time = t;
	}
	
	public void modifyRange(int r) {
		Contract.checkCondition(r > 0);
		range = r;
	}	
	
	public void setCoord(Coordinate c) {
		coord = c;
	}
	
	public void explose() {
		hasExplode = true;
	}
	
	public void setPlayer(Player p) {
		player = p;
	}
	
	public void drop() {
		timeInstallation = System.currentTimeMillis();
	}
	
	public void stopExplosion() {
		remainingTime = (long) (timeInstallation - System.currentTimeMillis() + time * 1000 );
		timeInstallation = System.currentTimeMillis();
	}
}
