package bomberman.model.entity.bomb;

import java.io.Serializable;
import java.util.ArrayDeque;
import java.util.Deque;

public class StdStockBomb implements StockBomb, Serializable {

	// ATTRIBUTS
	
	private static final long serialVersionUID = 1L;
	
	private Deque<Bomb> stock;
	private StdBomb stdBomb;
	private Bomb lastDropBomb;

	// CONSTRUCTEURS

	public StdStockBomb() {
		stdBomb = new StdBomb();
		stock = new ArrayDeque<Bomb>();
		stock.add(new StdBomb());
	}

	// REQUETES

	public Deque<Bomb> getStock() {
		return stock;
	}
	
	public Bomb getStdBomb() {
		return stdBomb;	
	}
	
	public Bomb getLastDropBomb() {
		return lastDropBomb;
	}
	
	public int getStockNb() {
		return stock.size();
	}	
	
	public boolean canDropBomb() {
		return getStockNb() > 0;
	}

	// COMMANDES

	public void dropBomb() {
		if (canDropBomb()) {
			lastDropBomb = stock.removeFirst();	
		}
	}
	
	public void addBomb() {
		stock.addLast(new StdBomb(stdBomb));
	}
	
	public void increaseRange() {
		int range;
		range = stdBomb.getRange() + 1;
		stdBomb.modifyRange(range);
		int stockNb = getStockNb();
		stock.clear();
		for (int i = 0; i < stockNb ; ++i) {
			stock.add(new StdBomb(stdBomb));
		}
	}
	
	public void decreaseTime() {
		int time;
		time = stdBomb.getTime() - 1;
		if (time > 0) {
			stdBomb.modifyTime(time);
			int stockNb = getStockNb();
			stock.clear();
			for (int i = 0; i < stockNb ; ++i) {
				stock.add(new StdBomb(stdBomb));
			}
		}	
	}
	
	public void resetStockBomb() {
		stdBomb = new StdBomb();
		stock.clear();
		stock.add(new StdBomb());
	}
}
