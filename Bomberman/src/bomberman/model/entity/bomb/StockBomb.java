package bomberman.model.entity.bomb;

import java.util.Deque;

/**
 * Une interface qui sp�cifie les stocks de bombes.
 * 
 * @inv <pre>
 *     getStock() != null
 *     getStockNb() == getStock().size()
 *     getStockNb() >= 0
 *     getStockNb() > 0 => canDropBomb() </pre>
 *     
 * @cons <pre>
 * $DESC$
 *     Un constructeur de stock de bombes.
 * $POST$
 *     getStock() != null
 *     getStock().size() == 2 </pre>
 */
public interface StockBomb {

	// REQUETES
	
	/**
	 * Le stock de bombes.
	 */
	Deque<Bomb> getStock();
	
	/**
	 * Une bombe standard qui permet de conna�tre les caract�ristiques des bombes de ce stock.
	 */
	Bomb getStdBomb();
	
	/**
	 * La derni�re bombe de ce stock qui a �t� pos�e.
	 */
	Bomb getLastDropBomb();
	
	/**
	 * Le nombre de bombes de ce stock.
	 */
	int getStockNb();
	
	/**
	 * Retourne vrai si il reste au moins une bombe dans le stock.
	 */
	boolean canDropBomb();
	
	// COMMANDES
	
	/**
	 * @post <pre>
	 *     canDropBomb() => getLastDropBomb() == getStock().removeFirst() </pre>
	 */
	void dropBomb();
	
	/**
	 * Ajoute une bombe standard au stock.
	 * @post <pre>
	 *     getStock().getLast().equals(getStdBomb)
	 *     getStockNb() == old getStockNb() + 1 </pre>
	 */
	void addBomb();
	
	/**
	 * Augmente la port�e des bombes.
	 * @post <pre>
	 *     getStdBomb().getRange() == old getStdBomb().getRange() + 1
	 *     forall i:[0..getStockNb() - 1] : 
	 *         getStock().toArray()[i].getRange() 
	 *             == old getStock().toArray()[i].getRange() + 1 </pre> 
	 */
	void increaseRange();
	
	/**
	 * Diminue le temps d'explosion des bombes.
	 * @post <pre>
	 *     getStdBomb().getTime() > 1 =>
	 *     	   getStdBomb().getTime() == old getStdBomb().getTime() - 1
	 *         forall i:[0..getBombs().getStockNb() - 1] : 
	 *             getStock().toArray()[i].getTime() > 1 =>
	 *        	       getStock().toArray()[i].getTime() 
	 *            	       == old getStock().toArray()[i].getTime() - 1 </pre> 
	 */
	void decreaseTime();
	
	/**
	 * R�initialise le stock de bombes comme � la sortie du constructeur.
	 */
	void resetStockBomb();
}
