package bomberman.model.entity.bomb;

import bomberman.model.entity.Entity;
import bomberman.model.entity.player.Player;
import bomberman.model.util.Coordinate;

/**
 * Une interface qui sp�cifie la notion de bombe.
 * 
 * @inv <pre>
 *     getTime() > 0
 *     getRange() > 0 </pre>
 *     
 * @cons <pre>
 * $DESC$
 *     Un constructeur de bombe par d�faut.
 * $POST$
 *     getImage().equals(BOMB_IMAGE)
 *     isCollisible() == true
 *     isDestroyable() == true
 *     getTime() == TIME
 *     getRange() == RANGE
 *     hasExplode() == false
 *     getPlayer == null
 *     getCoord() == null </pre>
 *     
 * @cons <pre>
 * $DESC$
 *     Un constructeur de bombe dont le d�lai d'explosion est <code>t</code>,
 *     la port�e est <code>r</code> et les coordonn�es <code>c</code>.
 * $ARGS$
 *     int t, int r, Coordinate c
 * $PRE$
 *     t > 0 && r > 0
 * $POST$
 *     getImage().equals(BOMB_IMAGE)
 *     isCollisible() == true
 *     isDestroyable() == true
 *     getTime() == t
 *     getRange() == r
 *     hasExplode() == false
 *     getPlayer == null
 *     getCoord() == c </pre>
 *     
 * @cons <pre>
 * $DESC$
 *     Un constructeur de bombe cr�ant une nouvelle instance �quivalente � <code>b</code>.
 * $ARGS$
 *     Bomb b
 * $PRE$
 *     b != null
 * $POST$
 *     getImage().equals(BOMB_IMAGE)
 *     isCollisible() == true
 *     isDestroyable() == true
 *     getTime() == b.getTime()
 *     getRange() == b.getRange()
 *     hasExplode() == false
 *     getPlayer == null
 *     getCoord().equals(b.getCoord()) </pre>
 */
public interface Bomb extends Entity {

	// CONSTANTES
	
	final int TIME = 3;
	final int RANGE = 1;
	
	final String BOMB_IMAGE = "bomb.png";
	
	// REQUETES
	
	/**
	 * Le d�lai d'explosion restant de la bombe en millisecondes.
	 */
	long remainingTime();
	
	/**
	 * Le d�lai d'explosion de la bombe en secondes.
	 */
	int getTime();
	
	/**
	 * La port�e de la bombe.
	 */
	int getRange();
	
	/**
	 * Les coordonn�es de cette bombe.
	 */
	Coordinate getCoord();
	
	/**
	 * Indique si la bombe a explos�;
	 */
	boolean hasExplode();
	
	/**
	 * Le joueur qui a pos� cette bombe;
	 */
	Player getPlayer();
	
	// COMMANDES
	
	/**
	 * Permet de modifier le d�lai d'explosion.
	 * @pre <pre>
	 *     t > 0 </pre>
	 * @post <pre>
	 *     getTime() == t </pre>
	 */
	void modifyTime(int t);
	
	/**
	 * Permet de modifier la port�e.
	 * @pre <pre>
	 *     r > 0 </pre>
	 * @post <pre>
	 *     getRange() == r </pre>
	 */
	void modifyRange(int r); 
	
	/**
	 * Permet de modifier les coordonn�es.
	 * @post <pre>
	 *     getCoord() == c </pre>
	 */
	void setCoord(Coordinate c);
	
	/**
	 * Explose la bombe.
	 * @post <pre>
	 *     hasExplose() == true </pre>
	 */
	void explose();
	
	/**
	 * Modifie le joueur.
	 * @post <pre>
	 *     getPlaye()r = p </pre>
	 */
	void setPlayer(Player p);
	
	/**
	 * Lance le timer de l'explosion.
	 */
	void drop();
	
	/**
	 * Arrête le timer de l'explosion.
	 */
	void stopExplosion();
}