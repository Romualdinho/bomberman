package bomberman.model.entity;

public enum MapEntityEnum {
	PLAYER("Joueur", 'P'),
	INDESTRUCTIBLE_WALL("Mur non destructible", 'W'),
	DESTRUCTIBLE_WALL("Mur destructible", 'D'),
	GOLD_BOMB("MegaBonus", 'G'),
	RANGE_BOMB("Bonus port�e", 'R'),
	TIME_BOMB("Bonus temps", 'T'),
	ADD_BOMB("Bonus ajout", 'A'),
	EMPTY("Vide", '.');
	
	// ATTRIBUTS

	private String annotation;
	private char key;
	
	// CONSTRUCTEURS
	
	private MapEntityEnum(String s, char c) {
		annotation = s;
		key = c;
	}
	
	// REQUETES

	public String getAnnotation() {
		return annotation;
	}
	
	public char getKey() {
		return key;
	}
	
	public static MapEntityEnum getEntityByChar(char c) {
		for (MapEntityEnum ent : MapEntityEnum.values()) {
			if (ent.getKey() == c) {
				return ent;
			}
		}		
		return null;
	}
}
