package bomberman.model.entity.bonus;

import bomberman.model.entity.StdEntity;
import bomberman.model.entity.player.Player;
import bomberman.model.util.Contract;

public class BombRangeBonus extends StdEntity implements Bonus {

	// CONSTRUCTEURS

	private static final long serialVersionUID = 1L;

	/**
	 * @throws RemoteException 
	 * @post <pre>
	 *     getImage.equals(BOMB_RANGE_IMAGE) </pre>
	 */
	public BombRangeBonus() {
		super(BOMB_RANGE_IMAGE, false, false);
	}
	
	// COMMANDES
	
	/**
	 * @post <pre>
	 *     forall i:[0..p.getBombs().getStockNb() - 1] : 
	 *         p.getBombs().toArray()[i].getRange() 
	 *             == old p.getBombs().toArray()[i].getRange() + 1 </pre> 
	 */
	public void bonusAction(Player p) {
		Contract.checkCondition(p != null);
		p.increaseBombsRange();
	}
}
