package bomberman.model.entity.bonus;

import bomberman.model.entity.StdEntity;
import bomberman.model.entity.player.Player;
import bomberman.model.util.Contract;

public class BombGoldBonus extends StdEntity implements Bonus {
	
	// CONSTRUCTEUR

	private static final long serialVersionUID = 1L;

	/**
	 * @throws RemoteException 
	 * @post <pre>
	 *     getImage.equals(BOMB_GOLD_IMAGE) </pre>
	 */
	public BombGoldBonus() {
		super(BOMB_GOLD_IMAGE, false, false);
	}
		
	// COMMANDES
	
	/**
	 * @post <pre>
	 *     p.getBombs().getStockNb() == old p.getBombs().getStockNb() + 1 
	 *     forall i:[0..p.getBombs().getStockNb() - 1] : 
	 *         p.getBombs().toArray()[i].getTime() > 1 =>
	 *        	   p.getBombs().toArray()[i].getTime() 
	 *            	   == old p.getBombs().toArray()[i].getTime() - 1 
	 *         p.getBombs().toArray()[i].getRange() 
	 *             == old p.getBombs().toArray()[i].getRange() + 1 </pre> 
	 */
	public void bonusAction(Player p) {
		Contract.checkCondition(p != null);
		p.increaseBombsNb();
		p.increaseBombsRange();
		p.decreaseBombsTime();
	}
}
