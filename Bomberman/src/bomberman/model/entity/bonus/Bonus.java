package bomberman.model.entity.bonus;

import bomberman.model.entity.Entity;
import bomberman.model.entity.player.Player;

/**
 * Une interface qui sp�cifie la notion de bonus.
 * 
 * @inv <pre> </pre>
 *     
 * @cons <pre>
 * $DESC$
 *     Un constructeur de bonus.
 * $POST$
 *     isCollisible() == false
 *     isDestroyable() == false </pre>
 */
public interface Bonus extends Entity {

	// CONSTANTES
	
	final String BOMB_RANGE_IMAGE = "range_bomb.png";
	final String BOMB_TIME_IMAGE = "time_bomb.png";
	final String BOMB_NB_IMAGE = "add_bomb.png";
	final String BOMB_GOLD_IMAGE = "gold_bomb.png";
	
	// COMMANDES
	
	/**
	 * Ex�cute l'action de ce bonus sur le joueur <code>p</code>.
	 * @pre <pre>
	 *     p != null </pre>
	 */
	void bonusAction(Player p);
}
