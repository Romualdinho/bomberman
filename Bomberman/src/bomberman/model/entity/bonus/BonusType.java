package bomberman.model.entity.bonus;

public enum BonusType {

	BOMB_GOLD_BONUS(BombGoldBonus.class),
	BOMB_NB_BONUS(BombNbBonus.class),
	BOMB_RANGE_BONUS(BombRangeBonus.class),
	BOMB_TIME_BONUS(BombTimeBonus.class);
	
	// REQUETES
	
	private Class<? extends Bonus> bonusClass;
	
	// CONSTRUCTEURS
	
	BonusType(Class<? extends Bonus> bonusClass) {
		this.bonusClass = bonusClass;
	}
	
	// REQUETES
	
	public Class<? extends Bonus> getBonusClass() {
		return bonusClass;
	}
}
