package bomberman.model.entity.bonus;

import bomberman.model.entity.StdEntity;
import bomberman.model.entity.player.Player;
import bomberman.model.util.Contract;

public class BombNbBonus extends StdEntity implements Bonus {

	// CONSTRUCTEURS

	private static final long serialVersionUID = 1L;

	/**
	 * @throws RemoteException 
	 * @post <pre>
	 *     getImage.equals(BOMB_NB_IMAGE) </pre>
	 */
	public BombNbBonus() {
		super(BOMB_NB_IMAGE, false, false);
	}
		
	// COMMANDES
	
	/**
	 * @post <pre>
	 *     p.getBombs().getStockNb() == old p.getBombs().getStockNb() + 1 </pre> 
	 */
	public void bonusAction(Player p) {
		Contract.checkCondition(p != null);
		p.increaseBombsNb();
	}
}
