package bomberman.model;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import bomberman.model.util.Contract;

public class Settings {
	
	private static Map<String, String> settings;
	private static boolean initialized;
	
	public static void init() {
		initialized = true;
		
		settings = new HashMap<String, String>();
		
		File configFile = new File(ResourcesInstaller.GAME_PATH + "cfg/config.cfg");
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(configFile));
			String currentLine;
			while ((currentLine = br.readLine()) != null) {
				String[] split = currentLine.trim().split(":");
				settings.put(split[0], split[1]);
			}
			br.close();
		} catch (IOException ie) {
			//rien
		}
	}
	
	public static void setValue(String name, String value) {
		settings.put(name, value);
		save();
	}
	
	private static void save() {
		File configFile = new File(ResourcesInstaller.GAME_PATH + "cfg/config.cfg");
		BufferedWriter bw = null;
		try {
			bw = new BufferedWriter(new FileWriter(configFile));
			for (Entry<String, String> entry : settings.entrySet()) {
				bw.write(entry.getKey() + ":" + entry.getValue() + "\n");
			}
			bw.close();
		} catch (IOException ie) {
			//rien
		}
	}
	
	public static String getStringValue(String name) {
		Contract.checkCondition(initialized);
		return settings.get(name);
	}
	
	public static Boolean getBooleanValue(String name) {
		Contract.checkCondition(initialized);
		return Boolean.valueOf(settings.get(name));
	}
	
	public static Integer getIntegerValue(String name) {
		Contract.checkCondition(initialized);
		return Integer.valueOf(settings.get(name));
	}
}